import { Component, Injectable } from '@angular/core';
import { AuthService } from './_service';
import { User } from './_entity';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  principal: User

  constructor(private authService: AuthService) {
    this.principal = new User
  }

  async ngOnInit() {
    await this.retrieveLogged()
  }

  async retrieveLogged() {
    await this.authService.retrieveLoggedUser()
      .pipe(first())
      .subscribe(res => {
        if(res.errors && res.errors.length > 0) {
          this.principal = undefined
        } else {
          this.principal = res.result
        }
      }, err => {

      })
  }
}
