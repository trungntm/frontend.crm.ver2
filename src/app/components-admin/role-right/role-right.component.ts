import { Component, OnInit } from '@angular/core';
import * as toastr from "../../../assets/global/plugins/bootstrap-toastr/toastr.min.js"
import { RoleService, PrivilegeService } from '../../_service';
import { MatDialogService } from '../../_service/common';
import { ActivatedRoute } from '@angular/router';
import * as $ from 'jquery';
import { Role, Privilege } from '../../_entity';
import { first } from 'rxjs/operators';
import { PaginatorConstant, PREFIX } from '../../_constant';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-role-right',
  templateUrl: './role-right.component.html',
  styleUrls: [
    '../../../assets/admin/layout/css/themes/darkblue.css',
    '../../../assets/common/css/custom.css',
    './role-right.component.css']
})
export class RoleRightComponent implements OnInit {

  roles: Role []
  privileges: Privilege []
  privilegesOfRole: Boolean [][]
  constructor(private dialogService: MatDialogService, private route: ActivatedRoute,
    private roleService: RoleService, private privilegeService: PrivilegeService, private toastrService: ToastrService) {
  }

  ngOnInit() {
    this.retrieveRoles()
  }

  async retrieveRoles() {
    await this.roleService.retrieveAllRolePaging(PaginatorConstant.keyword, PaginatorConstant.page, 100)
      .subscribe(async res => {
        if(res.errors && res.errors.length > 0) {
        } else {
          this.roles = res.result

          this.roles.forEach(role => {
            const roleNameLength = role.roleName.length
            role.roleName = role.roleName.substr(PREFIX.ROLE_LENGTH, roleNameLength - PREFIX.ROLE_LENGTH)
          })
          await this.retrievePrivileges()
        }
    }, err => {

    })
  }

  async retrievePrivileges() {
    await this.privilegeService.retrieveAllPrivilegesPaging(PaginatorConstant.keyword, PaginatorConstant.page, 100)
    .subscribe(async res => {
      if (res.errors && res.errors.lenth > 0) {

      } else {
        this.privileges = res.result
        await this.checkPrivilegesOfRoles()
      }
    })
  }

  async checkPrivilegesOfRoles() {
    const roleLength = this.roles.length
    const privilegeLength = this.privileges.length

    this.privilegesOfRole = new Array(roleLength).fill(false).map(() => new Array(privilegeLength))
    if(this.roles && this.privileges) {
      this.roles.forEach((role, roleIndex) => {
        this.privileges.forEach((privilege, privilegeIndex) => {
          if(role.privileges) {
            role.privileges.filter(x => x.id === privilege.id)
            .map(() => this.privilegesOfRole[roleIndex ][privilegeIndex] = true)
          }
          
        });
      });
    }
  }

  async onToggleSetPrivilege(e,role,privilege) {
    await this.roleService.updatePrivilegeForRole(role.id, privilege.id)
      .pipe(first())
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {
          res.errors.forEach(error => {
            this.toastrService.error('Cập nhật quyền thất bại!', 'Thông báo')
          });
        } else {
          this.toastrService.success('Cập nhật quyền thành công!', 'Thông báo')
        }
      }, err => {
        this.toastrService.error('Cập nhật quyền thất bại! Lỗi hệ thống.', 'Thông báo')
      })
  }

}
