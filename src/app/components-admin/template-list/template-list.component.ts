import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import * as $ from 'jquery'
import { Template } from '../../_entity'
import { BlockUI, NgBlockUI } from 'ng-block-ui'
import { MatDialogService } from 'src/app/_service/common'
import { TemplateService } from 'src/app/_service'
import { TextConstant, Status, PaginatorConstant, ResponseMessage, NumberConstant } from 'src/app/_constant';
import { first } from 'rxjs/operators';
import * as toastr from "../../../assets/global/plugins/bootstrap-toastr/toastr.min.js"
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-template-list',
  templateUrl: './template-list.component.html',
  styleUrls: [
    '../../../assets/admin/layout/css/themes/darkblue.css',
    '../../../assets/common/css/custom.css',
    './template-list.component.css']
})
export class TemplateListComponent implements OnInit {

  @BlockUI() blockUI: NgBlockUI
  templates: Template[]
  gridData: GridDataResult
  pageSize 
  pageIndex
  skip
  errors = []
  userAfterCreate
  searchFilter
  kendoLoading = false
  constructor(private dialogService: MatDialogService, private toastrService: ToastrService,
    private templateService: TemplateService) {
      this.searchFilter = TextConstant.EMPTY
      this.pageSize = NumberConstant.PAGE_SIZE_DEFAULT
      this.pageIndex = NumberConstant.PAGE_INDEX_DEFAULT
      this.skip = 0
     }

  async ngOnInit() {
    await this.retrieveTemplatesPaging()
  }

  public ngAfterViewInit() {
    this.resizeGrid();
  }

  private resizeGrid() {
    var grid = $("#grid");
    var offsetbottom =
      parseInt($(window).height()) - parseInt(grid.offset().top);
    var gridElement = grid,
      dataArea = gridElement.find(".k-grid-content"),
      heightHeader = $(".k-grid-header", grid).outerHeight() || 0,
      heightToolbar = $(".k-grid-toolbar", grid).outerHeight() || 0,
      heightFooter = $(".k-pager-wrap", grid).outerHeight() || 0;
    dataArea.height(
      offsetbottom - heightHeader - heightToolbar - heightFooter - 25
    );
  }

  pageChange({ skip, take }: PageChangeEvent): void {
    this.pageIndex = skip/take;
    this.pageSize = take
    this.skip = skip

    this.retrieveTemplatesPaging(this.searchFilter, Status.ACTIVE, this.pageIndex, this.pageSize)
  }

  async retrieveTemplatesPaging(keyword = this.searchFilter, status = Status.ACTIVE, page = this.pageIndex, size = this.pageSize) {
    this.kendoLoading = true

    return await this.templateService.retrieveAllTemplatePaging(keyword, status, page, size)
      .subscribe(res => {
        console.log(res)
        if (res.errors && res.errors.length > 0) {
          this.gridData = <GridDataResult> {
            total: 0,
            data: []
          }
        } else {
          this.gridData = <GridDataResult> {
            total: res.totalItems,
            data: res.result
          }
        }
         this.kendoLoading = false
      }, err => {
        this.kendoLoading = false
      })
  }

  async deleteTemplate(id) {
    this.searchFilter = TextConstant.EMPTY
    const dataDialog = {
      title: 'Xác nhận xóa',
      message: 'Bạn có chắc chắn xóa dữ liệu?'
    }

    return await this.dialogService.openConfirmDialog(dataDialog)
      .afterClosed()
      .subscribe(async res => {
        if (res) {
          this.blockUI.start(TextConstant.LOADING)

          await this.templateService.deleteTemplate(id)
            .pipe(first())
            .subscribe(async res => {
              if (res.body.errors && res.body.errors.length > 0) {
                this.toastrService.error('Xóa dữ liệu không thành công. Lỗi người dùng!', 'Thông báo')
              } else {
                this.toastrService.success('Xóa dữ liệu thành công!', 'Thông báo')
                await this.retrieveTemplatesPaging()                
              }

              this.blockUI.stop()
            }, err => {
              this.blockUI.stop()
              this.toastrService.error('Xóa dữ liệu không thành công. Lỗi hệ thống!', 'Thông báo')              
            })
        }
      })
  }

  async onFilter() {
    await this.retrieveTemplatesPaging(this.searchFilter.trim(), Status.ACTIVE, this.pageIndex, this.pageSize)
  }

  async onCancelFilter() {
    this.searchFilter = TextConstant.EMPTY
    await this.retrieveTemplatesPaging()

  }
}
