import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { StudentService } from 'src/app/_service';
import { Course, Student } from '../../_entity';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { TextConstant, Status } from 'src/app/_constant';
import { first } from 'rxjs/operators';
import { GridDataResult } from '@progress/kendo-angular-grid';
import * as $ from 'jquery';


@Component({
  selector: 'app-courses-of-student',
  templateUrl: './courses-of-student.component.html',
  styleUrls: ['./courses-of-student.component.css']
})
export class CoursesOfStudentComponent implements OnInit {

  @BlockUI() blockUI: NgBlockUI
  gridData: GridDataResult
  courses: Course[]
  studentId
  student: Student
  statusDefined
  textFilter
  statusFilter
  kendoLoading = false
  constructor(private activatedRoute: ActivatedRoute, 
    private toastrService: ToastrService,
    private studentService: StudentService) {
      this.statusDefined = [{
        label: 'Hoạt động', value: true
      }, {
        label: 'Ngưng hoạt động', value: false
      }]
      this.textFilter = TextConstant.EMPTY
      this.statusFilter = Status.ACTIVE
      this.student = new Student()
     }

  ngOnInit() {
    this.activatedRoute.params.subscribe(async params => {
      if (params.id) {
        this.studentId = params.id
        await this.retrieveStudentById(this.studentId)
        await this.retrieveCoursesOfStudent(this.studentId)
      }
    })
  }

  public ngAfterViewInit() {
    this.resizeGrid();
  }

  private resizeGrid() {
    var grid = $("#grid");
    var offsetbottom =
      parseInt($(window).height()) - parseInt(grid.offset().top);
    var gridElement = grid,
      dataArea = gridElement.find(".k-grid-content"),
      heightHeader = $(".k-grid-header", grid).outerHeight() || 0,
      heightToolbar = $(".k-grid-toolbar", grid).outerHeight() || 0,
      heightFooter = $(".k-pager-wrap", grid).outerHeight() || 0;
    dataArea.height(
      offsetbottom - heightHeader - heightToolbar - heightFooter - 25
    );
  }

  async retrieveStudentById(studentId) {
    this.blockUI.start(TextConstant.LOADING)
    await this.studentService.getById(studentId)
      .pipe(first())
      .subscribe(res => {
        console.log(res)
        if (res.errors && res.errors.length > 0) {

        } else {
          this.student = res.result
        }
        this.blockUI.stop()
      }, err =>  this.blockUI.stop())
  }

  async retrieveCoursesOfStudent(studentId, keyword = this.textFilter, status = this.statusFilter) {
    this.kendoLoading = true
    await this.studentService.getCoursesOfStudent(studentId, keyword, status)
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {
          this.gridData = <GridDataResult> {
            total: 0,
            data: []
          }
        } else {
          this.gridData = <GridDataResult> {
            total: 0,
            data: res.result
          }
        }
        this.kendoLoading = false
      })
  }

  async onFilter() {
    await this.retrieveCoursesOfStudent(this.studentId)
  }

  async onCancelFilter() {
    this.textFilter = TextConstant.EMPTY
    this.statusFilter = Status.ACTIVE
    await this.retrieveCoursesOfStudent(this.studentId)
  }

}
