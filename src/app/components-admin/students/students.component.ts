import { Component, OnInit } from '@angular/core';
import {IPDropdown, Student} from 'src/app/_entity';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { StudentService } from 'src/app/_service';
import { MatDialogService } from 'src/app/_service/common';
import { DialogService } from 'primeng/api';
import { NumberConstant, TextConstant, Status } from 'src/app/_constant';
import * as $ from 'jquery';
import { first } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import {getStateEmployee, getStatus} from '../../_helper';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: [
    '../../../assets/admin/layout/css/themes/darkblue.css',
    '../../../assets/common/css/custom.css',
    './students.component.css']
})
export class StudentsComponent implements OnInit {

  @BlockUI() blockUI: NgBlockUI
  
  students: Student[]
  gridData: GridDataResult
  pageSize 
  pageIndex
  skip
  errors = []
  textFilter
  statusFilter = true;
  statusDefined:IPDropdown[] = [];
  kendoLoading=false
  constructor(private studentService: StudentService, 
    private dialogService: MatDialogService, 
    private primeDialogService: DialogService,
    private toastrService: ToastrService) {
    this.statusDefined = [...getStatus()];

    this.pageSize = NumberConstant.PAGE_SIZE_DEFAULT
    this.pageIndex = NumberConstant.PAGE_INDEX_DEFAULT
    this.skip = 0;
    this.textFilter = TextConstant.EMPTY
    this.statusFilter = Status.ACTIVE
   }

  async ngOnInit() {

    await this.getStudentsByPage()
  }

  public ngAfterViewInit() {
    this.resizeGrid();
  }

  private resizeGrid() {
    var grid = $("#grid");
    var offsetbottom =
      parseInt($(window).height()) - parseInt(grid.offset().top);
    var gridElement = grid,
      dataArea = gridElement.find(".k-grid-content"),
      heightHeader = $(".k-grid-header", grid).outerHeight() || 0,
      heightToolbar = $(".k-grid-toolbar", grid).outerHeight() || 0,
      heightFooter = $(".k-pager-wrap", grid).outerHeight() || 0;
    dataArea.height(
      offsetbottom - heightHeader - heightToolbar - heightFooter - 25
    );
  }

  async getStudentsByPage(keyword=this.textFilter, status=this.statusFilter, page=this.pageIndex, size=this.pageSize) {
    this.kendoLoading = true
    await this.studentService.getStudentsByPage(keyword, status, page, size)
      .subscribe(res => {
        this.gridData = res;
        this.kendoLoading = false
      }, err => {

        this.kendoLoading = false
      })
  }

  pageChange({ skip, take }: PageChangeEvent): void {
    this.pageIndex = skip/take;
    this.pageSize = take;
    this.skip = skip;

    this.getStudentsByPage(this.textFilter,this.statusFilter, this.pageIndex, this.pageSize)
  }

  async onCancelFilter() {
    this.textFilter = TextConstant.EMPTY;
    this.statusFilter = Status.ACTIVE;
    await this.getStudentsByPage(this.textFilter.trim(), this.statusFilter, this.pageIndex, this.pageSize)
  }

  async onFilter() {
    await this.getStudentsByPage(this.textFilter.trim(), this.statusFilter, this.pageIndex, this.pageSize)
  }

  async deleteStudent(studentId) {
    const dataDialog = {
      title: 'Xác nhận xóa',
      message: 'Bạn có chắc chắn xóa học viên?'
    };
    this.dialogService.openConfirmDialog(dataDialog)
      .afterClosed()
      .subscribe(async res => {
        if (res) {
          await this.studentService.deleteStudent(studentId)
          .pipe(first())
          .subscribe(async res => {
            if (res.errors && res.errors.length > 0) {
              this.toastrService.error('Xóa học viên không thành công. Lỗi người dùng!', 'Thông báo')
            } else {
              this.toastrService.success('Xóa học viên thành công!', 'Thông báo')
              await this.getStudentsByPage()
            }
    
            this.blockUI.stop()
          }, err => {
            this.toastrService.error('Xóa học viên không thành công. Lỗi hệ thống!', 'Thông báo')
            this.blockUI.stop()
          })
        }
      })
    
  }
}
