import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import * as toastr from "../../../assets/global/plugins/bootstrap-toastr/toastr.min.js"
import { User, Role } from '../../_entity';
import { ActivatedRoute, Router } from '@angular/router';
import { RoleService, UsersService } from '../../_service';
import { first } from 'rxjs/operators';
import { TextConstant } from 'src/app/_constant/index.js';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-user-right',
  templateUrl: './user-right.component.html',
  styleUrls: [
    './user-right.component.css']
})
export class UserRightComponent implements OnInit {

  user: User;
  rolesOfUser: Role[];
  roles: Role[];
  id: any;
  errors: any;
  isSetRole

  constructor(private route: ActivatedRoute, private toastrService: ToastrService,
    private userService: UsersService, private roleService: RoleService) {
      this.user = new User()
      this.errors = []
     }

  ngOnInit() {
    this.route.params
      .subscribe( params => this.id = params.id )
    
    if (this.id) {
      this.retrieveUserById(this.id);
    }
  }

  async retrieveUserById(id) {
    await this.userService.retrieveUserById(id)
      .pipe(first())
      .subscribe(async res => {
        if(res.errors.length > 0) {
          res.errors.forEach(error => {
            this.errors.push(error)
          });
        } else {
          this.user = res.result;
          this.rolesOfUser = this.user.roles;
        }
        await this.getAllRoles(TextConstant.EMPTY, 0, 100);
      }, err => {
        this.errors.push(err)
      });
  }

  async getAllRoles(name: string, page: number, size: number) {
    await this.roleService.retrieveAllRolePaging(name, page, size)
      .subscribe(async res => {
        this.roles = res.result
        await this.checkRoleIncludeRoleOfUser()
      }, err => {
        console.log(err)
      });
  }

  checkRoleIncludeRoleOfUser() {
    if(this.rolesOfUser) {
      for (const role of this.rolesOfUser) {
        this.roles.filter(x => x.roleName === role.roleName)
          .map(x => x.isOfUser = true)
      }
    }
  }

  async toggleSetRole(e,item) {
    this.isSetRole = e.checked
    const rid = item.id
    await this.updateRoleForUser(rid);

  }

  async updateRoleForUser(roleId) {
    return await this.userService.updateRoleForUser(this.id, roleId)
      .pipe(first())
      .subscribe(res => {
        if(res.errors && res.errors.length > 0) {
          res.errors.forEach(error => {
            this.toastrService.error(`Cập nhật quyền người dùng không thành công. Lỗi người dùng! ${error.message}` , 'Thông báo')
          });
        } else {
          this.roles.forEach(role => {
            if (role.id === roleId) {
              if (this.isSetRole) {
                localStorage.setItem(role.roleName, role.roleName)
              } else {
                localStorage.removeItem(role.roleName)
              }
            }
          })
            
          this.toastrService.success('Cập nhật quyền người dùng thành công!', 'Thông báo')
        }
      }, err => {
        this.toastrService.error('Cập nhật quyền người dùng không thành công. Lỗi hệ thống!', 'Thông báo')
      })
  }

}
