import { Component, OnInit } from '@angular/core';
import { Template } from '../../_entity';
import * as $ from 'jquery';
import { MatDialogService } from '../../_service/common';
import { ActivatedRoute } from '@angular/router';
import { TemplateService } from '../../_service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { first } from 'rxjs/operators';
import { TextConstant } from '../../_constant';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-template-detail',
  templateUrl: './template-detail.component.html',
  styleUrls: [
    '../../../assets/admin/layout/css/themes/darkblue.css',
    './template-detail.component.css']
})
export class TemplateDetailComponent implements OnInit {

  id
  @BlockUI() blockUI: NgBlockUI
  public Editor = ClassicEditor
  template: Template

  constructor(private dialogService: MatDialogService, private route: ActivatedRoute, private templateService: TemplateService, private toastrService: ToastrService) {
    this.template = new Template()
  }

  ngOnInit() {
    this.route.params
      .subscribe( params => this.id = params.id )
    if (this.id) {
      this.retrieveTemplateById(this.id)
    }
  }

  async onSubmit() {
    if (!this.id) {
      await this.createTemplate()
    } else {
      await this.updateTemplate()
    }
  }

  async retrieveTemplateById(id) {
    this.blockUI.start(TextConstant.LOADING)

    return await this.templateService.retrieveActivateTemplateById(id)
      .pipe(first())
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {

        } else {
          this.template = res.result
        }
        this.blockUI.stop()
      }, err => {
        this.blockUI.stop()
      })
  }

  async createTemplate() {
    const dataDialog = {
      title: 'Thông báo',
      message: 'Bạn có chắc chắn muốn thêm mới template?'
    }

    this.dialogService.openConfirmDialog(dataDialog)
      .afterClosed()
      .subscribe(async res => {
        if (res) {
          this.blockUI.start(TextConstant.LOADING)
          return await this.templateService.createTemplate(this.template)
            .pipe(first())
            .subscribe(res => {
              if (res.errors && res.errors.length > 0) {
                this.toastrService.error('Thêm mới template không thành công. Lỗi người dùng!', 'Thông báo')
              } else {
                this.toastrService.success('Thêm mới template thành công!', 'thông báo')
              }

              this.blockUI.stop()
            }, err => {
              this.toastrService.error('Thêm mới template không thành công. Lỗi hệ thống!', 'Thông báo')
              this.blockUI.stop()
            })
        }
      })
  }

  async updateTemplate() {
    const dataDialog = {
      title: 'Thông báo',
      message: 'Bạn có chắc chắn muốn cập nhật template?'
    }

    this.dialogService.openConfirmDialog(dataDialog)
      .afterClosed()
      .subscribe(async res => {
        if (res) {
          this.blockUI.start(TextConstant.LOADING)
          return await this.templateService.updateTemplate(this.id, this.template)
            .pipe(first())
            .subscribe(res => {
              if (res.errors && res.errors.length > 0) {
                this.toastrService.error('Cập nhật template không thành công. Lỗi người dùng!', 'Thông báo')
              } else {
                this.toastrService.success('Cập nhật template thành công!', 'Thông báo')
              }

              this.blockUI.stop()
            }, err => {
              this.toastrService.error('Cập nhật template không thành công. Lỗi hệ thống!', 'Thông báo')
              this.blockUI.stop()
            })
        }
      })
   }

}
