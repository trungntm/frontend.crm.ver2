import { Component, OnInit } from '@angular/core';
import {IFilterRequest, IPDropdown} from '../../_entity';
import {GridDataResult, PageChangeEvent} from '@progress/kendo-angular-grid';
import {NumberConstant, TextConstant} from '../../_constant';
import {DialogService} from 'primeng/api';
import {ContractTypeService} from '../../_service';
import {ContractTypeDetailComponent} from './contract-type-detail/contract-type-detail.component';
import * as $ from "jquery";
import {getStatus} from '../../_helper';
import {BlockUI, NgBlockUI} from 'ng-block-ui';

@Component({
  selector: 'app-contract-types',
  templateUrl: './contract-types.component.html',
  styleUrls: ['./contract-types.component.css']
})
export class ContractTypesComponent implements OnInit {

  @BlockUI() blockUI: NgBlockUI;
  protected searchText: string;
  protected pStatus: IPDropdown[] = [];
  protected statusFilter: boolean;
  protected kendoLoading = false;
  protected gridData: GridDataResult;
  protected pageIndex: number = NumberConstant.PAGE_INDEX_DEFAULT;
  protected pageSize: number = NumberConstant.PAGE_SIZE_DEFAULT;
  protected skip: number = 0;
  paramsReq: IFilterRequest;
  constructor(private primeDialogService: DialogService, private contractTypeService: ContractTypeService) {
    this.searchText = TextConstant.EMPTY;
    this.statusFilter = true;
    this.paramsReq = { keyword: this.searchText, status: this.statusFilter, page: this.pageIndex, size: this.pageSize };

  }

  ngOnInit() {
    this.pStatus = [...getStatus()];
    this.retrieveByParams(this.paramsReq);
  }

  ngAfterViewInit() {
    this.resizeGrid();
  }

  retrieveByParams(params: IFilterRequest) {
    this.kendoLoading = true;
    this.contractTypeService.retrieveByParams(params)
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {
          this.gridData = <GridDataResult> {
            total: 0,
            data: []
          }
        } else {
          this.gridData = <GridDataResult> {
            total: res.totalItems,
            data: res.result
          }
        }
        this.kendoLoading = false;
      })
  }

  pageChange({ skip, take }: PageChangeEvent): void {
    this.pageIndex = skip / take;
    this.pageSize = take;
    this.skip = skip;
    this.paramsReq = { keyword: this.searchText, status: this.statusFilter, page: this.pageIndex, size: this.pageSize };

    this.retrieveByParams(this.paramsReq);
  }

  private resizeGrid() {
    var grid = $("#grid");
    var offsetbottom =
      parseInt($(window).height()) - parseInt(grid.offset().top);
    var gridElement = grid,
      dataArea = gridElement.find(".k-grid-content"),
      heightHeader = $(".k-grid-header", grid).outerHeight() || 0,
      heightToolbar = $(".k-grid-toolbar", grid).outerHeight() || 0,
      heightFooter = $(".k-pager-wrap", grid).outerHeight() || 0;
    dataArea.height(
      offsetbottom - heightHeader - heightToolbar - heightFooter - 25
    );
  }

  showPopupAdd() {
    const ref = this.primeDialogService.open(ContractTypeDetailComponent, {
      header: 'Thông tin loại hợp đồng',
      width: '50%'
    });

    ref.onClose.subscribe(res => {
      if (res && res === true) {
        this.retrieveByParams(this.paramsReq);
      }
    });
  }

  showPopupEdit(id) {
    const ref = this.primeDialogService.open(ContractTypeDetailComponent, {
      header: 'Thông tin loại hợp đồng',
      width: '50%',
      data: {
        id: id
      }
    });

    ref.onClose.subscribe(res => {
      if (res && res === true) {
        this.retrieveByParams(this.paramsReq);
      }
    });
  }

  onFilter() {
    this.paramsReq = { keyword: this.searchText, status: this.statusFilter, page: this.pageIndex, size: this.pageSize };

    this.retrieveByParams(this.paramsReq);
  }

  onCancelFilter() {
    this.searchText = TextConstant.EMPTY;
    this.statusFilter = true;

    this.paramsReq = { keyword: this.searchText, status: this.statusFilter, page: this.pageIndex, size: this.pageSize };

    this.retrieveByParams(this.paramsReq);
  }
}
