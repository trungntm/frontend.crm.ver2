import { Component, OnInit } from '@angular/core';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {DynamicDialogConfig, DynamicDialogRef} from 'primeng/api';
import {ToastrService} from 'ngx-toastr';
import {ContractTypeService} from '../../../_service';
import {ContractType, IPDropdown} from '../../../_entity';
import {getStatus} from '../../../_helper';
import {TextConstant} from '../../../_constant';

@Component({
  selector: 'app-contract-type-detail',
  templateUrl: './contract-type-detail.component.html',
  styleUrls: ['./contract-type-detail.component.css']
})
export class ContractTypeDetailComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  public Editor = ClassicEditor;
  protected id: number;
  protected contractType: ContractType;
  protected pStatus: IPDropdown[] = [];

  constructor(public ref: DynamicDialogRef,
              public config: DynamicDialogConfig,
              private toastrService: ToastrService,
              private contractTypeService: ContractTypeService) {
    this.contractType = new ContractType();
  }

  ngOnInit() {
    this.pStatus = [...getStatus()];
    if (this.config.data && this.config.data.id) {
      this.id = this.config.data.id;
      this.retrieveById(this.id);
    }
  }

  retrieveById(id: number) {
    this.contractTypeService.retrieveById(id)
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {

        } else {
          this.contractType = res.result;
        }
      })
  }

  closePopup() {
    this.ref.close();
  }

  onSubmit() {
    this.id ? this.update(this.contractType) : this.create(this.contractType);
  }

  create(contractTypeDto: ContractType) {
    this.blockUI.start(TextConstant.LOADING);
    this.contractTypeService.create(contractTypeDto)
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {
          res.errors.forEach(err => {
            this.toastrService.error(err.message);
          });
        } else {
          this.toastrService.success("Thêm mới dữ liệu loại hợp đồng thành công!");
          this.ref.close(true);
        }
        this.blockUI.stop();
      }, err => {
        this.toastrService.error(err);
        this.blockUI.stop();
      })
  }

  update(contractTypeDto: ContractType) {
    this.blockUI.start(TextConstant.LOADING)
    this.contractTypeService.update(contractTypeDto)
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {
          res.errors.forEach(err => {
            this.toastrService.error(err.message);
          });
        } else {
          this.toastrService.success("Cập nhật dữ liệu loại hợp đồng thành công!");
          this.ref.close(true);
        }
        this.blockUI.stop();
      }, err => {
        this.toastrService.error(err);
        this.blockUI.stop();
      })
  }

}
