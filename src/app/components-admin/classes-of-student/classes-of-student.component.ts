import { Component, OnInit } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { GridDataResult } from '@progress/kendo-angular-grid';
import { Course, Student, Class, Invoice } from 'src/app/_entity';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { StudentService, InvoiceService } from 'src/app/_service';
import { TextConstant, Status } from 'src/app/_constant';
import { first } from 'rxjs/operators';
import * as $ from 'jquery'
import { DialogService } from 'primeng/api';
import { PaymentTuitionFeesComponent } from '../payment-tuition-fees/payment-tuition-fees.component';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-classes-of-student',
  templateUrl: './classes-of-student.component.html',
  styleUrls: ['./classes-of-student.component.css']
})
export class ClassesOfStudentComponent implements OnInit {

  @BlockUI() blockUI: NgBlockUI
  gridData: GridDataResult
  gridDataInvoices: GridDataResult[] = []
  classes: Class[]
  studentId
  student: Student
  statusDefined
  textFilter
  statusFilter
  kendoLoading = false
  tuitionFeeValue = 0
  invoices: Invoice[] = []
  constructor(private activatedRoute: ActivatedRoute, 
    private toastrService: ToastrService,
    private studentService: StudentService,
    private primeDialogService: DialogService,
    private invoiceService: InvoiceService) {
      this.statusDefined = [{
        label: 'Hoạt động', value: true
      }, {
        label: 'Ngưng hoạt động', value: false
      }]
      this.textFilter = TextConstant.EMPTY
      this.statusFilter = Status.ACTIVE
      this.student = new Student()
     }

  ngOnInit() {
    this.activatedRoute.params.subscribe(async params => {
      if (params.id) {
        this.studentId = params.id
        await this.retrieveStudentById(this.studentId)
        await this.retrieveClassesOfStudent(this.studentId)
      }
    })
  }

  public ngAfterViewInit() {
    this.resizeGrid();
  }

  private resizeGrid() {
    var grid = $("#grid");
    var offsetbottom =
      parseInt($(window).height()) - parseInt(grid.offset().top);
    var gridElement = grid,
      dataArea = gridElement.find(".k-grid-content"),
      heightHeader = $(".k-grid-header", grid).outerHeight() || 0,
      heightToolbar = $(".k-grid-toolbar", grid).outerHeight() || 0,
      heightFooter = $(".k-pager-wrap", grid).outerHeight() || 0;
    dataArea.height(
      offsetbottom - heightHeader - heightToolbar - heightFooter - 25
    );
  }

  async retrieveStudentById(studentId) {
    this.blockUI.start(TextConstant.LOADING)
    await this.studentService.getById(studentId)
      .pipe(first())
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {

        } else {
          this.student = res.result
        }
        this.blockUI.stop()
      }, err =>  this.blockUI.stop())
  }

  async retrieveClassesOfStudent(studentId, keyword = this.textFilter, status = this.statusFilter) {
    this.kendoLoading = true
    await this.studentService.getClassesOfStudent(studentId, keyword, status)
      .subscribe(res => {
        this.gridDataInvoices = []
        if (res.errors && res.errors.length > 0) {
          this.gridData = <GridDataResult> {
            total: 0,
            data: []
          }
        } else {
          this.gridData = <GridDataResult> {
            total: 0,
            data: res.result
          }
          this.classes = res.result
          this.classes.forEach(async aClass => {
            this.getInvoices(this.studentId, aClass)
          })

        }
        this.kendoLoading = false
      })
  }

  async getInvoices(studentId, aClass: Class) {
    this.kendoLoading = true
    await this.invoiceService.retrieveInvoiceByStudentAndClass(studentId, aClass.id)
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {
          this.gridDataInvoices.push(<GridDataResult> {
            total: 0,
            data: []
          })
        } else {
          this.gridDataInvoices.push(<GridDataResult> {
            total: 0,
            data: res.result
          })

          const array = res.result
          array.forEach((invoice: Invoice) => {
            aClass.tuitionValue -= invoice.paymentValue
          });
        }

        this.kendoLoading = false
    })
    
  }

  getTuitionValuePayment(e) {
    const aClass = this.classes.find(aClass => aClass.id === e.classId)
    aClass.tuitionValue -= e.totalTuititionFeePayment
  }

  async onFilter() {
    await this.retrieveClassesOfStudent(this.studentId)
  }

  async onCancelFilter() {
    this.textFilter = TextConstant.EMPTY
    this.statusFilter = Status.ACTIVE
    await this.retrieveClassesOfStudent(this.studentId)
  }

  paymentCost(classId) {
    const ref = this.primeDialogService.open(PaymentTuitionFeesComponent, {
      data: {
        studentId: this.studentId,
        classId: classId
      },
      width: '50%',
      header: 'Thanh toán học phí'
    })

    ref.onClose.subscribe(data => {
      isNullOrUndefined(data) ? undefined: this.retrieveClassesOfStudent(this.studentId)
    })
  }

}
