import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery'
import { EmployeeTypeService } from 'src/app/_service';
import { MatDialogService } from 'src/app/_service/common';
import { EmployeeType } from 'src/app/_entity';
import { Status, TextConstant, PaginatorConstant, NumberConstant } from 'src/app/_constant';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { DialogService } from 'primeng/api';
import { ToastrService } from 'ngx-toastr';
import { first } from 'rxjs/operators';
import { EmployeeTypeDetailComponent } from '../employee-type-detail/employee-type-detail.component';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-employee-types',
  templateUrl: './employee-types.component.html',
  styleUrls: [
    './employee-types.component.css']
})
export class EmployeeTypesComponent implements OnInit {

  @BlockUI() blockUI: NgBlockUI;
  empTypes: EmployeeType[]
  errors = []
  empTypeAfterCreate
  gridData: GridDataResult
  pageSize 
  pageIndex
  skip
  searchText: string
  kendoLoading = false

  constructor(private matDialogService: MatDialogService, private empTypeService: EmployeeTypeService, private primeDialogSrv: DialogService, private toastrService: ToastrService) {
    this.pageSize = NumberConstant.PAGE_SIZE_DEFAULT
    this.pageIndex = NumberConstant.PAGE_INDEX_DEFAULT
    this.skip = 0
    this.searchText = TextConstant.EMPTY
   }

  async ngOnInit() {
    await this.getEmployeeTypesByPage()
  }

  public ngAfterViewInit() {
    this.resizeGrid();
  }

  private resizeGrid() {
    var grid = $("#grid");
    var offsetbottom =
      parseInt($(window).height()) - parseInt(grid.offset().top);
    var gridElement = grid,
      dataArea = gridElement.find(".k-grid-content"),
      heightHeader = $(".k-grid-header", grid).outerHeight() || 0,
      heightToolbar = $(".k-grid-toolbar", grid).outerHeight() || 0,
      heightFooter = $(".k-pager-wrap", grid).outerHeight() || 0;
    dataArea.height(
      offsetbottom - heightHeader - heightToolbar - heightFooter - 25
    );
  }

  async getEmployeeTypesByPage(keyword = TextConstant.EMPTY, status = Status.ACTIVE, pageIndex = this.pageIndex, pageSize = this.pageSize) {
    this.kendoLoading = true
    await this.empTypeService.getEmployeeTypesByPage(keyword, status, pageIndex, pageSize)
      .subscribe(res => {
        this.gridData = res
        this.kendoLoading = false
      })
  }

  pageChange({ skip, take }: PageChangeEvent): void {
    this.pageIndex = skip/take;
    this.pageSize = take
    this.skip = skip

    this.getEmployeeTypesByPage(this.searchText.trim(), Status.ACTIVE, this.pageIndex, this.pageSize)
  }

  async getDataAfterCreate(e) {
    isNullOrUndefined(e)? undefined: await this.getEmployeeTypesByPage()
  }

  
  deleteEmpType(empTypeId) {
    const dataDialog = {
      title: 'Xoá dữ liệu',
      message: 'Bạn chắc chắn xóa dữ liệu?'
    }
    this.matDialogService.openConfirmDialog(dataDialog)
      .afterClosed()
      .subscribe(async res => {
        if(res) {
          this.blockUI.start(TextConstant.LOADING)
          await this.empTypeService.deleteEmployeeType(empTypeId)
            .pipe(first())
            .subscribe(async res => {
              if (res.errors && res.errors.length > 0) {
                this.toastrService.error('Xóa dữ liệu thất bại. Lỗi người dùng!', 'Thông báo')
              } else {
                this.toastrService.success("Xóa dữ liệu thành công!", "Thông báo")
                await this.getEmployeeTypesByPage()
              }
              this.blockUI.stop()
            }, err => {
              this.toastrService.error('Xóa dữ liệu thất bại. Lỗi hệ thống!', 'Thông báo')
              this.blockUI.stop()
            })
        }
      })
  }

  showPopupEditEmpType(empTypeId) {
    const ref = this.primeDialogSrv.open(EmployeeTypeDetailComponent, {
      data: {
        id: empTypeId
      },
      header: 'Thông tin loại nhân viên',
      width: '50%'
    })

    ref.onClose.subscribe (async res => {
      isNullOrUndefined(res)? undefined: await this.getEmployeeTypesByPage()
    })
  }

  async onFilter() {
    await this.getEmployeeTypesByPage(this.searchText.trim(), Status.ACTIVE, this.pageIndex, this.pageSize)  
  }

  async onCancelFilter() {
    this.searchText = TextConstant.EMPTY
    await this.getEmployeeTypesByPage()
  }

}
