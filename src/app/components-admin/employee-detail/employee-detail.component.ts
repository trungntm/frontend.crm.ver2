import { Component, OnInit } from "@angular/core";
import { EmployeeService } from "../../_service";
import { MatDialogService } from "../../_service/common";
import { ActivatedRoute } from "@angular/router";
import {Employee, Image, IPDropdown} from '../../_entity';
import { first } from "rxjs/operators";
import { TextConstant } from "../../_constant";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { ToastrService } from 'ngx-toastr';
import {getStateEmployee} from '../../_helper';
import { STATE_EMPLOYEE} from '../../_common/common';
import {TakeBreak} from '../../_entity/take-break';
import { isWithinRange } from 'date-fns';

@Component({
  selector: "app-employee-detail",
  templateUrl: "./employee-detail.component.html",
  styleUrls: [
    "../../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css",
    "./employee-detail.component.css"
  ]
})
export class EmployeeDetailComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  id;
  employee: Employee;
  imageSelected;
  errors = [];
  states: IPDropdown[] = [];
  takeBreak: TakeBreak;
  isReader: boolean = false;
  protected currentDate: Date = new Date();

  constructor(
    private dialogService: MatDialogService,
    private route: ActivatedRoute,
    private employeeService: EmployeeService,
    private toastrService: ToastrService
  ) {
    this.employee = new Employee();
    this.takeBreak = new TakeBreak();
    this.states = [...getStateEmployee()];
  }

  ngOnInit() {
    this.route.params.subscribe(async params => {
      this.id = params.id;
      await this.retrieveEmployeeById(this.id);
    });
  }

  async retrieveEmployeeById(id) {
    await this.employeeService
      .retrieveEmployeeById(id)
      .pipe(first())
      .subscribe(
        res => {
          if (res.errors && res.errors.length > 0) {
            res.errors.forEach(error => {
              this.toastrService.error(error.message);
            });
          } else {
            this.employee = res.result;
            this.employee.birthDay = new Date(this.employee.birthDay);

            if (this.employee.takeBreaks && this.employee.takeBreaks.length > 0) {
              this.employee.takeBreaks.forEach(tb => {
                if (isWithinRange(this.currentDate, tb.fromTime, tb.toTime) == false) {
                  this.employee.takeBreaks.splice(this.employee.takeBreaks.indexOf(tb), 1);
                }
              });

              if (this.employee.takeBreaks.length > 0) {
                this.takeBreak = this.employee.takeBreaks[0];
                this.takeBreak.toTime = new Date(this.takeBreak.toTime);
                this.takeBreak.fromTime = new Date(this.takeBreak.fromTime);
                this.isReader = true;
              }
            }
          }
        },
        err => {
          this.toastrService.error(err);
        }
      );
  }

  async onFileSelected(e) {
    if (e.target.files && e.target.files[0]) {
      this.imageSelected = e.target.files[0];
      const reader = new FileReader();
      reader.onload = (event: any) => {
        if (!this.employee.image) {
          this.employee.image = new Image()
        }
        this.employee.image.imgPath = event.target.result;
        console.log(this.employee.image.imgPath)
      };
      reader.readAsDataURL(e.target.files[0]);
    }
  }

  async onChangeImage() {
    const formData = new FormData();
    formData.append("image", this.imageSelected, this.imageSelected.name);

    await this.employeeService
      .changeImage(this.id, formData)
      .pipe(first())
      .subscribe(
        res => {
          if (res.errors && res.errors.length > 0) {
            this.toastrService.error('Cập nhật hình ảnh không thành công. Lỗi người dùng!', 'Thông báo')
          } else {
            this.toastrService.success('Cập nhật hình ảnh thành công!', 'Thông báo')
          }
        },
        err => {
          this.toastrService.error('Cập nhật hình ảnh không thành công. Lỗi hệ thống!', 'Thông báo')
        }
      );
  }

  onChangeState() {
    this.takeBreak = new TakeBreak();
    this.isReader = this.employee.state !== STATE_EMPLOYEE.ACTIVATE && this.employee.state !== STATE_EMPLOYEE.DEACTIVATE;
  }

  async updateEmployee(id, employeeDto: Employee) {
    this.blockUI.start(TextConstant.LOADING);

    if (employeeDto.state != STATE_EMPLOYEE.ACTIVATE && employeeDto.state != STATE_EMPLOYEE.DEACTIVATE) {
      if (!employeeDto.takeBreaks) {
        employeeDto.takeBreaks = [];
      }
      employeeDto.takeBreaks.push(this.takeBreak);
    }
    return await this.employeeService.updateEmployee(id, employeeDto).subscribe(
      res => {
        if (res.errors && res.errors.length > 0) {
          this.toastrService.error('Cập nhật thông tin nhân viên không thành công. Lỗi người dùng!', 'Thông báo');
        } else {
          this.toastrService.success('Cập nhật thông tin nhân viên thành công!', 'Thông báo');
        }
        this.blockUI.stop();

      },
      err => {
        this.toastrService.error('Cập nhật thông tin nhân viên không thành công. Lỗi hệ thống!', 'Thông báo');
        this.blockUI.stop();
      }
    );
  }

  async onSubmit() {
    if (this.id) {
      await this.updateEmployee(this.id, this.employee);
    }
  }
}
