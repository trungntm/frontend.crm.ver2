import { Component, OnInit } from "@angular/core";
import { EmployeeType } from "../../_entity";
import { EmployeeTypeService } from "../../_service";
import { MatDialogService } from "../../_service/common";
import * as $ from "jquery";
import { ActivatedRoute } from "@angular/router";
import { first } from "rxjs/operators";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { TextConstant, ResponseMessage } from "../../_constant";
import * as toastr from "../../../assets/global/plugins/bootstrap-toastr/toastr.min.js";
import { DynamicDialogRef, DynamicDialogConfig } from "primeng/api";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-employee-type-detail",
  templateUrl: "./employee-type-detail.component.html",
  styleUrls: ["./employee-type-detail.component.css"]
})
export class EmployeeTypeDetailComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  empType: EmployeeType;
  errors = [];
  id;
  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private toastrService: ToastrService,
    private empTypeService: EmployeeTypeService
  ) {
    this.empType = new EmployeeType();
  }

  async ngOnInit() {
    if (this.config.data && this.config.data.id) {
      this.id = this.config.data.id;
      await this.onLoadData(this.id);
    }
  }

  async onLoadData(id) {
    return await this.empTypeService
      .retrieveEmployeeTypeById(id)
      .pipe(first())
      .subscribe(
        res => {
          if (res.errors && res.errors.length > 0) {
            res.errors.forEach(error => {
              this.errors.push(error);
            });
          } else {
            this.empType = res.result;
          }
        },
        err => {}
      );
  }

  async onSubmit() {
    if (!this.id) {
      await this.createEmployeeType(this.empType);
    } else {
      await this.updateEmployeeType(this.id, this.empType);
    }
  }

  async createEmployeeType(empTypeDto) {
    await this.empTypeService
      .createEmployeeType(empTypeDto)
      .pipe(first())
      .subscribe(
        res => {
          if (res.errors && res.errors.length > 0) {
            this.toastrService.error(
              "Thêm mới dữ liệu thất bại. Lỗi người dùng!",
              "Thông báo"
            );
          } else {
            this.toastrService.success(
              "Thêm mới dữ liệu thành công!",
              "Thông báo"
            );
            this.ref.close(res);
            this.blockUI.stop();
          }
        },
        err => {
          this.toastrService.error(
            "Thêm mới dữ liệu thất bại. Lỗi hệ thống!",
            "Thông báo"
          );
          this.blockUI.stop();
        }
      );
  }

  async updateEmployeeType(id, employeeDto) {
    return await this.empTypeService
      .updateEmployeeType(id, employeeDto)
      .pipe(first())
      .subscribe(
        res => {
          if (res.errors && res.errors.length) {
            this.toastrService.error(
              "Cập nhật mới dữ liệu thất bại. Lỗi hệ thống!",
              "Thông báo"
            );
          } else {
            this.toastrService.success(
              "Cập nhật dữ liệu thành công!",
              "Thông báo"
            );
            this.ref.close(res);
          }
          this.blockUI.stop();
        },
        err => {
          this.toastrService.error("Cập nhật dữ liệu thất bại!", "Thông báo");
          this.blockUI.stop();
        }
      );
  }

  closePopup() {
    this.ref.close();
  }
}
