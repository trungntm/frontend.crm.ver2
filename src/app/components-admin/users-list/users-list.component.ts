import { Component, OnInit } from '@angular/core'
import * as $ from 'jquery'
import { NgBlockUI, BlockUI } from 'ng-block-ui'
import * as toastr from '../../../assets/global/plugins/bootstrap-toastr/toastr.min.js'
import { 
  User, EmployeeType 
} from '../../_entity'
import { 
  UsersService,
  EmployeeTypeService 
} from '../../_service'
import { PaginatorConstant, Status, TextConstant, ResponseMessage, NumberConstant } from '../../_constant'
import { MatDialogService } from '../../_service/common'
import { DialogService } from 'primeng/api';
import { UserDetailComponent } from '../user-detail/user-detail.component';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { isUndefined } from 'util';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: [
    '../../../assets/admin/layout/css/themes/darkblue.css',
    '../../../assets/common/css/custom.css',
    './users-list.component.css']
})
export class UsersListComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI
  
  users: User[]
  empTypes: EmployeeType[]
  gridData: GridDataResult
  pageSize 
  pageIndex
  skip
  errors = []
  userAfterCreate
  kendoLoading = false

  constructor(private dialogService: MatDialogService, private primeDialogService: DialogService,
    private userService: UsersService, private empTypeService: EmployeeTypeService) {
      this.pageSize = NumberConstant.PAGE_SIZE_DEFAULT
      this.pageIndex = NumberConstant.PAGE_INDEX_DEFAULT
      this.skip = 0
     }

  async ngOnInit() {
    await this.onPageLoad()
    await this.getUsersByPage()
  }

  public ngAfterViewInit() {
    this.resizeGrid();
  }

  private resizeGrid() {
    var grid = $("#grid");
    var offsetbottom =
      parseInt($(window).height()) - parseInt(grid.offset().top);
    var gridElement = grid,
      dataArea = gridElement.find(".k-grid-content"),
      heightHeader = $(".k-grid-header", grid).outerHeight() || 0,
      heightToolbar = $(".k-grid-toolbar", grid).outerHeight() || 0,
      heightFooter = $(".k-pager-wrap", grid).outerHeight() || 0;
    dataArea.height(
      offsetbottom - heightHeader - heightToolbar - heightFooter - 25
    );
  }

  async onPageLoad() {
    await this.empTypeService.getAllEmployeeTypes()
      .subscribe(res => {
        if (res.errors && res.errors.length > 0){
          
        } else {
          this.empTypes = res.result
          
        }
      })
  }

  async getUsersByPage(keyword = TextConstant.EMPTY, status = Status.ACTIVE, pageIndex = this.pageIndex, pageSize = this.pageSize) {
    this.kendoLoading = true
    await this.userService.getUsersByPage(keyword, status, pageIndex, pageSize)
      .subscribe(res => {
        this.gridData = res
        this.kendoLoading = false
      })
  }

  pageChange({ skip, take }: PageChangeEvent): void {
    this.pageIndex = skip/take;
    this.pageSize = take
    this.skip = skip

    this.getUsersByPage(TextConstant.EMPTY, Status.ACTIVE, this.pageIndex, this.pageSize)
  }
  async getDataAfterCreate(e) {
    this.userAfterCreate = e

    isUndefined(this.userAfterCreate)? undefined: await this.getUsersByPage()
  }

  async showEditUserPopup(userId) {
    const ref = this.primeDialogService.open(UserDetailComponent, {
      data: {
        id: userId
      },
      header: 'Thông tin người dùng',
      width: '70%'
    })
    
    ref.onClose.subscribe(async (res) => {
      await this.getUsersByPage()
      })
  }

  async onDeleteRecord(id) {
    const dialogData = {
      id: 1,
      title: 'Thông báo',
      message: 'Xóa tài khoản?'
    }

    this.dialogService.openConfirmDialog(dialogData)
      .afterClosed()
      .subscribe(async res => {
        if (res) {
          this.blockUI.start(TextConstant.LOADING)
          await this.userService.onDeleteUser(id)
            .subscribe(async res => {
              if (res.errors && res.errors.length > 0) {
                toastr.error(ResponseMessage.DELETE_ERROR_USER)
              } else {
                toastr.success(ResponseMessage.DELETE_SUCCESS)
                await this.getUsersByPage()
              }
            }, err => {
              toastr.error(ResponseMessage.DELETE_ERROR_SYSTEM)
              
            })
          this.blockUI.stop()
        }
      })
  }

  async onFilter() {
    const $keyword = $('#txtFilter').val().trim()
    const $empType = $('#sel-empType option:selected').val()
    await this.getUsersByPage($keyword, Status.ACTIVE, this.pageIndex, this.pageSize)
    }

  async onCancelFilter() {
    this.getUsersByPage()
  }
}
