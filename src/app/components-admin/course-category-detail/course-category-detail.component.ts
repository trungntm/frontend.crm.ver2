import { Component, OnInit } from "@angular/core";
import { CourseCategory } from "../../_entity";
import * as $ from "jquery";
import * as toastr from "../../../assets/global/plugins/bootstrap-toastr/toastr.min.js";
import { MatDialogService } from "../../_service/common";
import { ActivatedRoute, Router } from "@angular/router";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { first } from "rxjs/operators";
import { TextConstant, ResponseMessage } from "../../_constant";
import { CourseCategoryService } from "../../_service";
import { DynamicDialogRef, DynamicDialogConfig } from "primeng/api";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-course-category-detail",
  templateUrl: "./course-category-detail.component.html",
  styleUrls: ["./course-category-detail.component.css"]
})
export class CourseCategoryDetailComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  id;
  courseCategory: CourseCategory;

  constructor(
    private courseCatService: CourseCategoryService,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private toastrService: ToastrService
  ) {
    this.courseCategory = new CourseCategory();
  }

  ngOnInit() {
    if (this.config.data && this.config.data.id) {
      this.id = this.config.data.id;
      this.retrieveCourseCategoryById(this.id)
    }
  }

  async retrieveCourseCategoryById(courseCategoryId) {
    await this.courseCatService.retrieveCourseCategoryById(courseCategoryId)
      .pipe(first())
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {

        } else {
          this.courseCategory = res.result
        }
      })
  }

  async onSubmit() {
    if (this.id) {
      await this.updateCourseCategory(this.courseCategory);
    } else {
      await this.createCourseCategory(this.courseCategory);
    }
  }

  closePopup() {
    this.ref.close(undefined);
  }

  async updateCourseCategory(courseCatDto) {
    return this.courseCatService
      .updateCourseCategory(courseCatDto)
      .pipe(first())
      .subscribe(
        res => {
          if (res.errors && res.errors.length > 0) {
            this.toastrService.error('Cập nhật dữ liệu thất bại. Lỗi người dùng!', 'Thông báo')
          } else {
            this.toastrService.success('Cập nhật dữ liệu thành công!', 'Thông báo')
            this.ref.close(res)
          }
        },
        err => {
          this.toastrService.error('Cập nhật dữ liệu thất bại. Lỗi hệ thống!')
        }
      );
  }

  async createCourseCategory(courseCatDto) {
    return this.courseCatService
      .createCourseCategory(courseCatDto)
      .pipe(first())
      .subscribe(
        res => {
          if (res.errors && res.errors.length > 0) {
            this.toastrService.error('Thêm dữ liệu thất bại. Lỗi người dùng!', 'Thông báo')
          } else {
            this.toastrService.success('Thêm dữ liệu thành công!', 'Thông báo')
            this.ref.close(res)
          }
        },
        err => {
          this.toastrService.error('Thêm dữ liệu thất bại. Lỗi hệ thống!')
        }
      );
  }
}
