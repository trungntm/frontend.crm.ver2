import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { RoleService } from '../../_service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Role } from '../../_entity';
import { PaginatorConstant, PREFIX, TextConstant, NumberConstant } from '../../_constant';
import { DialogService } from 'primeng/api';
import { RoleDetailComponent } from '../role-detail/role-detail.component';
import { MatDialogService } from 'src/app/_service/common';
import { first } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { RoleUserConfigComponent } from '../role-user-config/role-user-config.component';

@Component({
  selector: 'app-roles-list',
  templateUrl: './roles-list.component.html',
  styleUrls: [
    './roles-list.component.css']
})
export class RolesListComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI
  roles: Role[]
  roleAfterCreate
  gridData: GridDataResult
  pageSize 
  pageIndex
  skip
  searchText: string
  kendoLoading=false
  constructor(private roleService: RoleService, private matDialogService: MatDialogService, private primeDialogService: DialogService, private toastrService: ToastrService) {
    this.pageSize = NumberConstant.PAGE_SIZE_DEFAULT
    this.pageIndex = NumberConstant.PAGE_INDEX_DEFAULT
    this.skip = 0
   }

  async ngOnInit() {
    // await this.onLoadData(TextConstant.EMPTY, PaginatorConstant.page, PaginatorConstant.size)
    await this.getRolesByPage()
  }

  public ngAfterViewInit() {
    this.resizeGrid();
  }

  private resizeGrid() {
    var grid = $("#grid");
    var offsetbottom =
      parseInt($(window).height()) - parseInt(grid.offset().top);
    var gridElement = grid,
      dataArea = gridElement.find(".k-grid-content"),
      heightHeader = $(".k-grid-header", grid).outerHeight() || 0,
      heightToolbar = $(".k-grid-toolbar", grid).outerHeight() || 0,
      heightFooter = $(".k-pager-wrap", grid).outerHeight() || 0;
    dataArea.height(
      offsetbottom - heightHeader - heightToolbar - heightFooter - 25
    );
  }

  async getRolesByPage(keyword = TextConstant.EMPTY, pageIndex = this.pageIndex, pageSize = this.pageSize) {
    this.kendoLoading = true
    await this.roleService.getRolesByPage(keyword, pageIndex, pageSize)
      .subscribe(res => {
        this.gridData = res
        this.kendoLoading = false
      })
  }

  pageChange({ skip, take }: PageChangeEvent): void {
    this.pageIndex = skip/take;
    this.pageSize = take
    this.skip = skip

    this.getRolesByPage(TextConstant.EMPTY, this.pageIndex, this.pageSize)
  }

  async getDataAfterCreate(e) {
    this.roleAfterCreate = e

    if (this.roleAfterCreate.id) {
      await this.getRolesByPage()
    }
  }

  deleteRole(roleId) {
    const dataDialog = {
      title: 'Xoá dữ liệu',
      message: 'Bạn chắc chắn xóa dữ liệu?'
    }
    this.matDialogService.openConfirmDialog(dataDialog)
      .afterClosed()
      .subscribe(res => {
        if(res) {
          this.blockUI.start(TextConstant.LOADING)
          this.roleService.deleteRole(roleId)
            .pipe(first())
            .subscribe(async res => {
              if (res.errors && res.errors.length > 0) {
                this.toastrService.error('Xóa dữ liệu thất bại. Lỗi người dùng!', 'Thông báo')
              } else {
                this.toastrService.success("Xóa dữ liệu thành công!", "Thông báo")
                await this.getRolesByPage()
              }
              this.blockUI.stop()
            }, err => {
              this.toastrService.error('Xóa dữ liệu thất bại. Lỗi hệ thống!', 'Thông báo')
              this.blockUI.stop()
            })
        }
      })
  }

  showEditRolePopup(roleId) {
    const ref = this.primeDialogService.open(RoleDetailComponent, {
      data: {
        id: roleId
      },
      header: 'Thông tin quyền người dùng',
      width: '50%'
    })

    ref.onClose.subscribe (async res => {
      await this.getRolesByPage()
    })
  }

  async onFilter() {
    await this.getRolesByPage(this.searchText.trim(), this.pageIndex, this.pageSize)
  }

  async onCancelFilter() {
    this.searchText = TextConstant.EMPTY
    await this.getRolesByPage()
  }

  showDetailRole(roleId) {
    this.primeDialogService.open(RoleUserConfigComponent, {
      data: {
        roleId: roleId
      },
      header: 'Chi tiết nhóm quyền',
      width: '70%'
    })
  }
}
