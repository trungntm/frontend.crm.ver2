import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { InvoiceService } from 'src/app/_service';
import { Invoice } from 'src/app/_entity';
import { GridDataResult } from '@progress/kendo-angular-grid';
import {PrintService} from '../../_service/common';

@Component({
  selector: 'app-invoices-student-class',
  templateUrl: './invoices-student-class.component.html',
  styleUrls: ['./invoices-student-class.component.css']
})
export class InvoicesStudentClassComponent implements OnInit {

  @Input() studentId
  @Input() classId
  gridData: GridDataResult
  kendoLoading = false
  @Output() emitTuitionValue = new EventEmitter<any>()
  constructor(private invoiceService: InvoiceService, private printService: PrintService) { }

  ngOnInit() {
    this.getInvoices(this.studentId, this.classId)
  }

  async getInvoices(studentId, classId) {
    this.kendoLoading = true
    await this.invoiceService.retrieveInvoiceByStudentAndClass(studentId, classId)
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {
          this.gridData = <GridDataResult> {
            total: 0,
            data: []
          }
        } else {
          this.gridData = <GridDataResult> {
            total: 0,
            data: res.result
          }
        }
        this.kendoLoading = false
      })
  }

  onPrintInvoice() {
    // this.invoiceService.printInvoice()
    //   .subscribe(res => {
    //     console.log(res)
    //   })
    this.printService.printDocument('invoice', ['1']);
  }
}
