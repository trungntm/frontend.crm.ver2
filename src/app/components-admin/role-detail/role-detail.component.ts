import { Component, OnInit } from "@angular/core";
import * as $ from "jquery";
import * as toastr from "../../../assets/global/plugins/bootstrap-toastr/toastr.min.js";
import { RoleService } from "../../_service";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { first } from "rxjs/operators";
import { Role } from "../../_entity";
import { PREFIX, TextConstant } from "../../_constant";
import { DynamicDialogRef, DynamicDialogConfig } from "primeng/api";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-role-detail",
  templateUrl: "./role-detail.component.html",
  styleUrls: ["./role-detail.component.css"]
})
export class RoleDetailComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  id;
  role: Role;
  constructor(
    private roleService: RoleService,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private toastrService: ToastrService
  ) {
    this.role = new Role();
  }

  async ngOnInit() {
    if (this.config.data && this.config.data.id) {
      this.id = this.config.data.id;
      this.onLoadData(this.id);
    }
  }

  async onLoadData(id) {
    this.blockUI.start(TextConstant.LOADING);

    await this.roleService
      .retrieveRoleById(id)
      .pipe(first())
      .subscribe(
        res => {
          if (res.errors && res.errors.length > 0) {
          } else {
            this.role = res.result;
            const roleNameLength = this.role.roleName.length;
            this.role.roleName = this.role.roleName.substr(
              PREFIX.ROLE_LENGTH,
              roleNameLength - PREFIX.ROLE_LENGTH
            );
          }
          this.blockUI.stop();
        },
        err => {
        }
      );
  }

  onSubmit() {
    this.blockUI.start(TextConstant.LOADING);
    if (!this.id) {
      this.roleService
        .insertRole(this.role)
        .pipe(first())
        .subscribe(
          res => {
            if (res.errors && res.errors.length > 0) {
              this.toastrService.error(
                "Thêm mới dữ liệu thất bại. Lỗi người dùng!",
                "Thông báo"
              );
            } else {
              this.toastrService.success(
                "Thêm mới dữ liệu thành công!",
                "Thông báo"
              );
              this.ref.close(res);
            }
            $("input").val("");
            this.blockUI.stop();
          },
          err => {
            this.toastrService.error(
              "Thêm mới dữ liệu thất bại. Lỗi hệ thống!",
              "Thông báo"
            );
            this.blockUI.stop();
          }
        );
    } else {
      this.roleService
        .updateRole(this.role)
        .pipe(first())
        .subscribe(
          res => {
            if (res.errors && res.errors.length) {
              this.toastrService.error(
                "Cập nhật mới dữ liệu thất bại. Lỗi hệ thống!",
                "Thông báo"
              );
            } else {
              this.toastrService.success(
                "Cập nhật dữ liệu thành công!",
                "Thông báo"
              );
              this.ref.close(res);
            }
            this.blockUI.stop();
          },
          err => {
            this.toastrService.error("Cập nhật dữ liệu thất bại!", "Thông báo");
            this.blockUI.stop();
          }
        );
    }
  }

  closePopup() {
    this.ref.close();
  }
}
