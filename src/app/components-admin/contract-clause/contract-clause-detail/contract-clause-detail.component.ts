import { Component, OnInit } from '@angular/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {DynamicDialogConfig, DynamicDialogRef} from 'primeng/api';
import {ToastrService} from 'ngx-toastr';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {first} from 'rxjs/operators';
import {TextConstant} from '../../../_constant';
import {ContractTypeService, ContractClauseService} from '../../../_service';
import {ContractType, IPDropdown, ContractClause} from '../../../_entity';

@Component({
  selector: 'app-contract-clause-detail',
  templateUrl: './contract-clause-detail.component.html',
  styleUrls: ['./contract-clause-detail.component.css']
})
export class ContractClauseDetailComponent implements OnInit {

  @BlockUI() blockUI: NgBlockUI;
  public Editor = ClassicEditor;
  protected id: number = 0;
  contractClause: ContractClause;
  contractTypesDisplay: IPDropdown[] = [];
  constructor(public ref: DynamicDialogRef,
              public config: DynamicDialogConfig,
              private contractClauseService: ContractClauseService,
              private contractTypeService: ContractTypeService,
              private toastrService: ToastrService) {
    this.contractClause = new ContractClause();
  }

  ngOnInit() {
    this.contractTypeService.retrieveAll()
      .subscribe(res => {
        if (res.result && res.result.length > 0) {
          res.result.forEach((data: ContractType) => {
            this.contractTypesDisplay.push({
              label: data.name,
              value: data.id
            })
          })
        }
      });
    if (this.config.data && this.config.data.id) {
      this.id = this.config.data.id;
      this.retrieveById(this.id);
    }
  }

  retrieveById(id: number) {
    this.contractClauseService.retrieveById(id)
      .pipe(first())
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {

        } else {
          this.contractClause = res.result;
        }
      })
  }

  closePopup() {
    this.ref.close();
  }

  onSubmit() {
    this.id ? this.update(this.contractClause): this.create(this.contractClause);
  }

  create(contractClauseDto: ContractClause) {
    this.blockUI.start(TextConstant.LOADING);
    this.contractClauseService.create(contractClauseDto)
      .pipe(first())
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {
          res.errors.forEach(err => {
            this.toastrService.error(err.message);
          })
        } else {
          this.toastrService.success('Thêm dữ liệu điều khoản hợp đồng thành công!');
          this.ref.close(true);
        }
        this.blockUI.stop();
      }, err => {
        this.toastrService.error(err);
        this.blockUI.stop();
      })
  }

  update(contractClauseDto: ContractClause) {
    this.blockUI.start(TextConstant.LOADING);
    this.contractClauseService.update(contractClauseDto)
      .pipe(first())
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {
          res.errors.forEach(err => {
            this.toastrService.error(err.message);
          })
        } else {
          this.toastrService.success('Cập nhật dữ liệu điều khoản hợp đồng thành công!');
          this.ref.close(true);
        }
        this.blockUI.stop();
      }, err => {
        this.toastrService.error(err);
        this.blockUI.stop();
      })
  }


}
