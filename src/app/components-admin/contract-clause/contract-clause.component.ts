import { Component, OnInit } from '@angular/core';
import {NumberConstant, Status, TextConstant} from '../../_constant';
import {ContractType, IFilterRequest, IPDropdown} from '../../_entity';
import { getStatus} from '../../_helper';
import {DialogService} from 'primeng/api';
import {ContractClauseDetailComponent} from './contract-clause-detail/contract-clause-detail.component';
import {ContractClauseService} from '../../_service/contract-clause.service';
import {GridDataResult, PageChangeEvent} from '@progress/kendo-angular-grid';
import * as $ from "jquery";
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {ContractTypeService} from '../../_service';

@Component({
  selector: 'app-contract-clause',
  templateUrl: './contract-clause.component.html',
  styleUrls: ['./contract-clause.component.css']
})
export class ContractClauseComponent implements OnInit {

  @BlockUI() blockUI: NgBlockUI;
  protected searchText: string;
  protected pStatus: IPDropdown[] = [];
  protected statusFilter: boolean;
  protected kendoLoading = false;
  protected contractTypeFilter: number = 0;
  protected gridData: GridDataResult;
  protected pageIndex: number = NumberConstant.PAGE_INDEX_DEFAULT;
  protected pageSize: number = NumberConstant.PAGE_SIZE_DEFAULT;
  protected skip: number = 0;
  paramsReq: IFilterRequest;
  protected contractTypesDisplay: IPDropdown[] = [];

  constructor(private primeDialogService: DialogService, private contractClauseService: ContractClauseService, private contractTypeService: ContractTypeService) {
    this.searchText = TextConstant.EMPTY;
    this.statusFilter = true;
    this.paramsReq = { keyword: this.searchText, status: this.statusFilter, contractTypeId: this.contractTypeFilter, page: this.pageIndex, size: this.pageSize };
  }

  ngOnInit() {
    this.pStatus = [...getStatus()];
    this.retrieveByParams(this.paramsReq);
    this.contractTypeService.retrieveAll()
      .subscribe(res => {
        if (res.result && res.result.length > 0) {
          res.result.forEach((data: ContractType) => {
            this.contractTypesDisplay.push({
              label: data.name,
              value: data.id
            })
          })
        }
      })
  }

  ngAfterViewInit() {
    this.resizeGrid();
  }

  retrieveByParams(params: IFilterRequest = this.paramsReq) {
    this.kendoLoading = true;
    this.contractClauseService.retrieveByParams(params)
      .subscribe(res=> {
        if (res.errors && res.errors.length > 0) {
          this.gridData = <GridDataResult> {
            total: 0,
            data: []
          }
        } else {
          this.gridData = <GridDataResult> {
            total: res.totalItems,
            data: res.result
          }
        }
        this.kendoLoading = false;
      })
  }

  pageChange({ skip, take }: PageChangeEvent): void {
    this.pageIndex = skip / take;
    this.pageSize = take;
    this.skip = skip;
    this.paramsReq = { keyword: this.searchText, status: this.statusFilter, contractTypeId: this.contractTypeFilter, page: this.pageIndex, size: this.pageSize };
    this.retrieveByParams(this.paramsReq);
  }

  private resizeGrid() {
    var grid = $("#grid");
    var offsetbottom =
      parseInt($(window).height()) - parseInt(grid.offset().top);
    var gridElement = grid,
      dataArea = gridElement.find(".k-grid-content"),
      heightHeader = $(".k-grid-header", grid).outerHeight() || 0,
      heightToolbar = $(".k-grid-toolbar", grid).outerHeight() || 0,
      heightFooter = $(".k-pager-wrap", grid).outerHeight() || 0;
    dataArea.height(
      offsetbottom - heightHeader - heightToolbar - heightFooter - 25
    );
  }

  showPopupAdd() {
    const ref = this.primeDialogService.open(ContractClauseDetailComponent, {
      header: 'Thông tin điều khoản',
      width: '70%'
    });

    ref.onClose.subscribe(res => {
      if (res && res === true) {
        this.retrieveByParams(this.paramsReq);
      }
    });
  }

  showPopupEdit(id) {
    const ref = this.primeDialogService.open(ContractClauseDetailComponent, {
      header: 'Thông tin điều khoản',
      width: '70%',
      data: {
        id: id
      }
    });

    ref.onClose.subscribe(res => {
      if (res && res === true) {
        this.retrieveByParams(this.paramsReq);
      }
    });
  }

  onFilter() {
    this.paramsReq = { keyword: this.searchText, status: this.statusFilter, contractTypeId: this.contractTypeFilter, page: this.pageIndex, size: this.pageSize };
    this.retrieveByParams(this.paramsReq);
  }

  onCancelFilter() {
    this.searchText = TextConstant.EMPTY;
    this.statusFilter = true;
    this.contractTypeFilter = 0;
    this.paramsReq = { keyword: this.searchText, status: this.statusFilter, contractTypeId: this.contractTypeFilter, page: this.pageIndex, size: this.pageSize };
    this.retrieveByParams(this.paramsReq);
  }
}
