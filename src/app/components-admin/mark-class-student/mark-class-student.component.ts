import { Component, OnInit, Input } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { MarkServiceService } from 'src/app/_service';
import { NumberConstant, TextConstant, Status } from 'src/app/_constant';
import { ToastrService } from 'ngx-toastr';
import { first } from 'rxjs/operators';
import * as $ from 'jquery';
import {IDataConfirmDelete, Mark} from '../../_entity';
import {MatDialogService} from '../../_service/common';

@Component({
  selector: 'app-mark-class-student',
  templateUrl: './mark-class-student.component.html',
  styleUrls: ['./mark-class-student.component.css']
})
export class MarkClassStudentComponent implements OnInit {

  @BlockUI() blockUI: NgBlockUI
  errors = []
  gridData: GridDataResult
  marks: Mark[] = []
  pageSize 
  pageIndex
  skip
  searchFilter: string
  statusFilter
  statusDefined
  selectedExcel
  kendoLoading = false
  @Input() classId
  constructor(private markService: MarkServiceService, private toastrService: ToastrService, private matDialogService: MatDialogService) {
    this.statusDefined = [{
      label: 'Hoạt động', value: true
    }, {
      label: 'Ngưng hoạt động', value: false
    }]
    
    this.pageSize = NumberConstant.PAGE_SIZE_DEFAULT
    this.pageIndex = NumberConstant.PAGE_INDEX_DEFAULT
    this.skip = 0
    this.searchFilter = TextConstant.EMPTY
    this.statusFilter = Status.ACTIVE
   }

  ngOnInit() {
    this.retrieveMarksByClass(this.classId)
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    this.resizeGrid()
  }

  async retrieveMarksByClass(classId = this.classId, keyword = TextConstant.EMPTY, status = Status.ACTIVE, page = this.pageIndex, size = this.pageSize) {
    this.kendoLoading = true
    await this.markService.retrieveMarksByClass(classId, keyword, status, page, size)
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {
          this.marks = []
          this.gridData = <GridDataResult> {
            data: [],
            total: 0
          }
        } else {
          this.marks = res.result
          this.gridData = <GridDataResult> {
            data: res.result,
            total: res.totalItems
          }
        }
        this.kendoLoading = false
      })
  }

  pageChange({ skip, take }: PageChangeEvent): void {
    this.pageIndex = skip / take;
    this.pageSize = take;
    this.skip = skip;

    this.retrieveMarksByClass()
  }

  async onFilter() {
    await this.retrieveMarksByClass(this.classId, this.searchFilter.trim(), this.statusFilter, this.pageIndex, this.pageSize)
  }

  async onCancelFilter() {
    this.searchFilter = TextConstant.EMPTY
    this.statusFilter = Status.ACTIVE
    await this.retrieveMarksByClass(this.classId, this.searchFilter.trim(), this.statusFilter, this.pageIndex, this.pageSize)
  }

  async onChangeExcel(e) {
    if (e.target.files && e.target.files[0]) {
      this.selectedExcel = e.target.files[0];

      const formData = new FormData();
      formData.append('excel', this.selectedExcel, this.selectedExcel.name);
      await this.onImportExcel(formData);
    }
  }

  async onImportExcel(formData) {
    this.blockUI.start(TextConstant.LOADING);
    this.markService
      .importExcel(this.classId, formData)
      .pipe(first())
      .subscribe(
        async res => {
          if (res.body.errors && res.body.errors.length > 0) {
            this.toastrService.error(
              "Import danh sách học viên thất bại. Lỗi người dùng!",
              "Thông báo"
            );
          } else {
            this.toastrService.success(
              "Import danh sách học viên thành công!",
              "Thông báo"
            );
            await this.retrieveMarksByClass()
          }
          this.blockUI.stop();
        },
        err => {
          this.toastrService.error(
            "Import danh sách học viên thất bại. Lỗi hệ thống!",
            "Thông báo"
          );
          this.blockUI.stop();
        }
      );
  }

  private resizeGrid() {
    var grid = $("#grid1");
    var offsetbottom =
      parseInt($(window).height()) - parseInt(grid.offset().top);
    var gridElement = grid,
      dataArea = gridElement.find(".k-grid-content"),
      heightHeader = $(".k-grid-header", grid).outerHeight() || 0,
      heightToolbar = $(".k-grid-toolbar", grid).outerHeight() || 0,
      heightFooter = $(".k-pager-wrap", grid).outerHeight() || 0;
    dataArea.height(
      offsetbottom - heightHeader - heightToolbar - heightFooter - 50
    );
  }

  updateMark() {
    const dialogData: IDataConfirmDelete  = {
      title: 'Cập nhật điểm học viên',
      message: 'Bạn chắc chắn cập nhật điểm học viên?'
    }

    this.matDialogService.openConfirmDialog(dialogData)
      .afterClosed()
      .subscribe(res => {
        if (res) {
          this.blockUI.start(TextConstant.LOADING);
          this.markService.updateMarks(this.marks)
            .subscribe(res => {
              if (res.errors && res.errors.length > 0) {
                res.errors.forEach(err => {
                  this.toastrService.error(err.message);
                })
              } else {
                this.toastrService.success('Cập nhật điểm lớp học thành công!');
              }
              this.blockUI.stop();
            }, err => {
              this.toastrService.error(err);
              this.blockUI.stop();
            })
        }
      })
  }
}
