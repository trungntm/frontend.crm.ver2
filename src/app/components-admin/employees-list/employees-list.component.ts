import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { EmployeeService, EmployeeTypeService } from '../../_service';
import {BaseResponse, Employee, IDataConfirmDelete, IFilterRequest, IPDropdown} from '../../_entity';
import { PaginatorConstant, Status, TextConstant, NumberConstant } from 'src/app/_constant';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { MatDialogService } from 'src/app/_service/common';
import { DialogService } from 'primeng/api';
import {ToastrService} from 'ngx-toastr';
import {getStateEmployee} from '../../_helper';
import {STATE_EMPLOYEE} from '../../_common/common';

@Component({
  selector: 'app-employees-list',
  templateUrl: './employees-list.component.html',
  styleUrls: [
    './employees-list.component.css']
})
export class EmployeesListComponent implements OnInit {

  @BlockUI() blockUI: NgBlockUI
  errors = []
  gridData: GridDataResult
  pageSize 
  pageIndex
  skip
  searchText: string
  statusFilter
  employeeTypes
  statusDefined
  protected states: IPDropdown[] = [];
  protected stateFilter:number = STATE_EMPLOYEE.ACTIVATE;
  kendoLoading = false
  constructor(private employeeService: EmployeeService,
              private empTypeService: EmployeeTypeService,
              private matDialogSrv: MatDialogService,
              private primeDialogSrv: DialogService,
              private toastrService: ToastrService) {

    this.states = [...getStateEmployee()];
    this.pageSize = NumberConstant.PAGE_SIZE_DEFAULT;
    this.pageIndex = NumberConstant.PAGE_INDEX_DEFAULT;
    this.skip = 0;
    this.searchText = TextConstant.EMPTY;
    this.statusFilter = Status.ACTIVE;
    this.employeeTypes = [];
   }

  async ngOnInit() {
    await this.getAllEmployeeTypes();
    await this.getEmployeesByPage();
  }

  public ngAfterViewInit() {
    this.resizeGrid();
  }

  private resizeGrid() {
    var grid = $("#grid");
    var offsetbottom =
      parseInt($(window).height()) - parseInt(grid.offset().top);
    var gridElement = grid,
      dataArea = gridElement.find(".k-grid-content"),
      heightHeader = $(".k-grid-header", grid).outerHeight() || 0,
      heightToolbar = $(".k-grid-toolbar", grid).outerHeight() || 0,
      heightFooter = $(".k-pager-wrap", grid).outerHeight() || 0;
    dataArea.height(
      offsetbottom - heightHeader - heightToolbar - heightFooter - 25
    );
  }

  async getEmployeesByPage(keyword = this.searchText, state = this.stateFilter, pageIndex = this.pageIndex, pageSize = this.pageSize) {
    this.kendoLoading = true
    const params: IFilterRequest = { keyword, state, page: pageIndex, size: pageSize };
    await this.employeeService.getEmployeesByPage(params)
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {
          this.gridData = <GridDataResult> {
            data: [],
            total: 0
          }
        } else {
          this.gridData = <GridDataResult> {
            data: res.result,
            total: res.totalItems
          }
        }
        this.kendoLoading = false
      })
  }

  pageChange({ skip, take }: PageChangeEvent): void {
    this.pageIndex = skip/take;
    this.pageSize = take;
    this.skip = skip;

    this.getEmployeesByPage()
  }

  async onFilter() {
    await this.getEmployeesByPage()
  }

  async onCancelFilter() {
    this.searchText = TextConstant.EMPTY;
    this.statusFilter = Status.ACTIVE;
    this.stateFilter = STATE_EMPLOYEE.ACTIVATE;
    await this.getEmployeesByPage();
  }

  async onDeleteRecord(id) {
    const dataConfirm:IDataConfirmDelete = {
      title: 'Xác nhận',
      message: 'Xác nhận xóa dữ liệu?'
    };

    this.matDialogSrv.openConfirmDialog(dataConfirm)
      .afterClosed()
      .subscribe(res => {
        if (res) {
          this.blockUI.start(TextConstant.LOADING);
          this.employeeService.deleteEmployee(id)
            .subscribe(res => {
              if (res.errors && res.errors.length > 0) {
                res.errors.forEach(error => {
                  this.toastrService.error(error.message);
                })
              } else {
                this.getEmployeesByPage();
                this.toastrService.success("Xóa dữ liệu thành công!");
              }
              this.blockUI.stop();
            })
        }
      })
  }

  async getAllEmployeeTypes(keyword = TextConstant.EMPTY, status = Status.ACTIVE, page = 0, size = 100000) {
      this.empTypeService.retrieveEmployeeTypesPaging(keyword, status, page, size)
        .subscribe(res => {
          if (res.errors && res.errors.length > 0) {

          } else {
            res.result.map(x => {
              this.employeeTypes.push(((label, value) => ({label: x.typeName, value: x.id}))(x))
            })
          }
        })
  }
}
