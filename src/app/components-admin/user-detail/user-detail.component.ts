import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import * as $ from 'jquery'
import { NgBlockUI, BlockUI } from 'ng-block-ui'
import { User, EmployeeType } from '../../_entity'
import { UsersService, EmployeeTypeService } from '../../_service'
import { MatDialogService } from '../../_service/common'
import { first } from 'rxjs/operators'
import { TextConstant, ResponseMessage } from '../../_constant'
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/api';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: [
    './user-detail.component.css'],
})
export class UserDetailComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  user: User
  employeeTypes: EmployeeType[]
  id
  constructor(private dialogService: MatDialogService, private route: ActivatedRoute, private userService: UsersService, private employeeTypeService: EmployeeTypeService,
    public ref: DynamicDialogRef, public config: DynamicDialogConfig, private toastrService: ToastrService) {
    this.user = new User()
   }

  async ngOnInit() {
    await this.onLoadContent()
    if(this.config.data && this.config.data.id) {
      this.id = this.config.data.id
      await this.onLoadData(this.id)
    }
  }

  async onLoadData(id) {
    this.blockUI.start(TextConstant.LOADING)

    await this.userService.retrieveUserById(id)
      .pipe(first())
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {
        } else {
          this.user = res.result
        }
        this.blockUI.stop()

      }, err => {
        this.blockUI.stop()
      })
  }

  async onLoadContent() {
    await this.employeeTypeService.getAllEmployeeTypes()
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {

        } else {
          this.employeeTypes = res.result
        }
      }, err => {

      })
  }

  closePopup() {
    this.ref.close(undefined)
  }

  async onSubmit() {
    if (!this.id) {
      await this.createUser(this.user)
    } else {
      await this.updateUser(this.user)
    }
  }

  async createUser(userDto) {
    this.blockUI.start(TextConstant.LOADING)
    await this.userService.createUser(userDto)
      .pipe(first())
      .subscribe(res => {
        if(res.errors && res.errors.length > 0) {
          this.toastrService.error("Thêm dữ liệu thất bại. Lỗi người dùng!", "Thông báo")
        } else {
          this.toastrService.success("Thêm dữ liệu thành công!", "Thông báo")
          this.ref.close(res)
        }
        this.blockUI.stop()
      }, err => {
        this.toastrService.error("Thêm dữ liệu thất bại. Lỗi hệ thống!", "Thông báo")
        this.blockUI.stop()
      })
  }

  async updateUser(userDto) {
    this.blockUI.start(TextConstant.LOADING)
    await this.userService.updateUser(userDto)
      .pipe(first())
      .subscribe(res => {
        if(res.errors && res.errors.length > 0) {
          this.toastrService.error("Cập nhật dữ liệu thất bại. Lỗi người dùng!", "Thông báo")
        } else {
          this.toastrService.success("Cập nhật dữ liệu thành công!", "Thông báo")
          this.ref.close(res)
        }
        this.blockUI.stop()
      }, err => {
        this.toastrService.error("Cập nhật dữ liệu thất bại. Lỗi hệ thống!", "Thông báo")
        this.blockUI.stop()
      })
  }
}
