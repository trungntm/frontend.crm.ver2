import { Component, OnInit } from '@angular/core';
import {ProfileService} from '../../_service';
import {IFilterRequest, User} from '../../_entity';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  protected principal: User;
  protected isEditPassword = false;
  protected isEditFullName = false;

  constructor(private profileService: ProfileService, private toastrService: ToastrService) {
    this.principal = new User();
  }

  ngOnInit() {
    this.retrieveCurrent();
  }

  protected retrieveCurrent() {
    this.profileService.retrieveCurrent()
      .subscribe(res => {
        this.principal = res.result;
      })
  }

  protected onUpdatePassword() {
    if (this.principal.password != this.principal.confirmPassword) {
      this.toastrService.error('Mật khẩu xác nhận không trùng khớp');
      return;
    }

    this.profileService.updatePassword({ uid: this.principal.id }, this.principal.password)
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {
          res.errors.forEach(error => {
            this.toastrService.error(error.message);
          })
        } else {
          this.toastrService.success('Đổi mật khẩu thành công!')
        }
      }, err => {
        this.toastrService.error(err);
      });
  }

  // protected onUpdateProfile() {
  //   this.profileService.updateProfile(this.principal)
  //     .subscribe(res => {
  //       if (res.errors && res.errors.length > 0) {
  //         res.errors.forEach(err => {
  //           this.toastrService.error(err.message);
  //         })
  //       } else {
  //         this.toastrService.success('Cập nhật thông tin thành công!');
  //       }
  //     })
  // }
}
