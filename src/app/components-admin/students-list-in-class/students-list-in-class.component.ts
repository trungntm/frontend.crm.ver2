import { Component, OnInit, Input } from '@angular/core';
import { StudentService, ClassService, EmployeeService, EmployeeTypeService } from "src/app/_service";
import { ToastrService } from "ngx-toastr";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { TextConstant, NumberConstant, Status } from "src/app/_constant";
import { first, map } from "rxjs/operators";
import { Class, Employee } from "src/app/_entity";
import { GridDataResult } from "@progress/kendo-angular-grid";
import * as $ from 'jquery';
import { MatDialogService } from 'src/app/_service/common';

@Component({
  selector: 'app-students-list-in-class',
  templateUrl: './students-list-in-class.component.html',
  styleUrls: ['./students-list-in-class.component.css']
})
export class StudentsListInClassComponent implements OnInit {

  @BlockUI() blockUI: NgBlockUI;
  @Input() classId;
  class: Class;
  gridData: GridDataResult;
  pageSize;
  pageIndex;
  skip;
  errors = [];
  searchText;
  statusFilter;
  statusDefined;
  lecture: Employee
  kendoLoading = false
  constructor(
    private matDialogService: MatDialogService,
    private classService: ClassService,
    private toastrService: ToastrService
  ) {
    this.class = new Class();
    this.statusDefined = [
      {
        label: "Hoạt động",
        value: true
      },
      {
        label: "Ngưng hoạt động",
        value: false
      }
    ];

    this.pageSize = NumberConstant.PAGE_SIZE_DEFAULT;
    this.pageIndex = NumberConstant.PAGE_INDEX_DEFAULT;
    this.skip = 0;
    this.searchText = TextConstant.EMPTY;
    this.statusFilter = Status.ACTIVE;

   }

  ngOnInit() {
    this.getClassById(this.classId)
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    this.resizeGrid()
  }

  async getClassById(classId) {
    this.kendoLoading = true
    await this.classService
      .getClassById(classId)
      .pipe(first())
      .subscribe(
        res => {
          if (res.errors && res.errors.length > 0) {
          } else {
            this.class = res.result;
            this.gridData = <GridDataResult>{
              data: this.class.students,
              total: this.class.students.length
            };
          }
          this.kendoLoading = false
        },
        err => {
          this.kendoLoading = false

        }
      );
  }


  getDataAfterCreate(e) {
    this.gridData = e.result.students;
  }

  private resizeGrid() {
    var grid = $("#grid");
    console.log(grid)
    var offsetbottom =
      parseInt($(window).height()) - parseInt(grid.offset().top);
    var gridElement = grid,
      dataArea = gridElement.find(".k-grid-content"),
      heightHeader = $(".k-grid-header", grid).outerHeight() || 0,
      heightToolbar = $(".k-grid-toolbar", grid).outerHeight() || 0,
      heightFooter = $(".k-pager-wrap", grid).outerHeight() || 0;
    dataArea.height(
      offsetbottom - heightHeader - heightToolbar - heightFooter - 50
    );
  }

  recieveFormDataToImportExcel(e) {
    this.blockUI.start(TextConstant.LOADING);
    this.classService
      .importExcel(this.classId, e)
      .pipe(first())
      .subscribe(
        res => {
          console.log(res);
          if (res.body.errors && res.body.errors.length > 0) {
            this.toastrService.error(
              "Import danh sách học viên thất bại. Lỗi người dùng!",
              "Thông báo"
            );
          } else {
            this.toastrService.success(
              "Import danh sách học viên thành công!",
              "Thông báo"
            );
            this.gridData = <GridDataResult>{
              data: res.body.result.students,
              total: res.body.result.students.length
            };
          }
          this.blockUI.stop();
        },
        err => {
          this.toastrService.error(
            "Import danh sách học viên thất bại. Lỗi hệ thống!",
            "Thông báo"
          );
          this.blockUI.stop();
        }
      );
  }

  async deleteStudentInClass(stundentId ) {
    const dataDialog = {
      title: 'Xóa dữ liệu',
      message: 'Bạn có chắc chắn xóa học viên ra khỏi lớp học?'
    }

    this.matDialogService.openConfirmDialog(dataDialog)
      .afterClosed()
      .subscribe(async res => {
        if (res) {
          this.blockUI.start(TextConstant.LOADING)
          await this.classService.deleteStudentFromClass(this.classId, stundentId)
            .subscribe(async res => {
              if (res.body.errors && res.body.errors.length > 0) {
                this.toastrService.error('Xóa học viên khỏi lớp học không thành công. Lỗi người dùng!', 'Thông báo')
              } else {
                this.toastrService.success('Xóa học viên khỏi lớp học thành công!', 'Thông báo')
                await this.getClassById(this.classId)
              }
      
              this.blockUI.stop()
            }, err => {
              this.toastrService.error('Xóa học viên khỏi lớp học không thành công. Lỗi hệ thống!', 'Thông báo')
              this.blockUI.stop()
            })
        }
      })
  }

  async onFilter() {
    await this.getClassById(this.classId)
  }
}
