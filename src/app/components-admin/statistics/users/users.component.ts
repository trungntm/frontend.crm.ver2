import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StatisticService } from 'src/app/_service';
import { months, ChartType } from 'src/app/_constant/chart-constant';
import * as $ from 'jquery';
import { Chart } from "chart.js";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: [
    './users.component.css']
})
export class UsersComponent implements OnInit {
  chartUsers: any[];
  valuesActive: number[];
  monthsActive: number[];
  valuesActiveDeleted: number[];
  monthsActiveDeleted: number[];
  monthsInYear: string[];
  chartData: any[];
  chartDataDeleted: number[];
  currentYear: number

  constructor(
    private route: ActivatedRoute,
    private statisticService: StatisticService
  ) {
    this.monthsInYear = months.monthsInYear;
    // this.chartData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 7];
    this.chartData = new Array(12).fill(0)
    // this.chartData1 = [[1,2,3,4,5,6,7,8,9,10,11,12],[12,11,10,9,8,7,6,5,4,3,2,1]]
    this.chartDataDeleted = new Array(12).fill(0)
    this.chartUsers = new Array()    
    this.currentYear = new Date().getFullYear()
  }

  async ngOnInit() {

    await this.onInitStatisticUsers(this.currentYear)
  }

  
  async onInitStatisticUsers(year) {
    await this.statisticService
      .getStatisticUsersCreated(year)
      .subscribe(async res => {
        this.monthsActive = res.result.verticals;
        this.valuesActive = res.result.horizontals;

        this.monthsActive.forEach((month, monthIndex) => {
          this.valuesActive.forEach((value, valueIndex) => {
            if (monthIndex == valueIndex) {
              this.chartData[month - 1] = value;
            }
          });
        });
        this.chartData = this.chartData.slice(0, 12);

        await this.statisticService.getStatisticUsersDeleted(year).subscribe(
          async res => {
            if (res.errors && res.errors.length > 0) {

            } else {
              this.monthsActiveDeleted = res.result.verticals;
              this.valuesActiveDeleted = res.result.horizontals;

              if (res.result.verticals.length > 0 && res.result.horizontals.length > 0) {
                this.monthsActiveDeleted.forEach((month, monthIndex) => {
                  this.valuesActiveDeleted.forEach((value, valueIndex) => {
                    if (monthIndex == valueIndex) {
                      this.chartDataDeleted[month - 1] = value;
                    }
                  });
                });
              }
              this.chartDataDeleted = this.chartDataDeleted.slice(0, 12);

              this.chartUsers = new Chart("canvas", {
                type: ChartType.LINE,
                data: {
                  labels: this.monthsInYear,

                  datasets: [
                    {
                      label: "Tài khoản được tạo",
                      data: this.chartData,
                      borderColor: "#3cba9f",
                      fill: false
                    },
                    {
                      label: "Tài khoản bị xóa",
                      data: this.chartDataDeleted,
                      borderColor: "#FF5E00",
                      fill: false
                    }
                  ]
                },
                options: {
                  legend: {
                    display: true
                  },
                  tooltips: {
                    callbacks: {
                      title: (tooltipItems, data) => {
                        return `Tháng ${tooltipItems[0].xLabel}`;
                      },
                      label: function(tooltipItems, data) {
                        return `${tooltipItems.yLabel} account(s)`;
                      }
                    }
                  },
                  scales: {
                    xAxes: [
                      {
                        display: true
                      }
                    ],
                    yAxes: [
                      {
                        display: true
                      }
                    ]
                  }
                }
              });
            }
          },
          err => {}
        );
      });
  }

}
