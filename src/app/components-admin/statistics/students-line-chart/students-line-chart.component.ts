import { Component, OnInit } from '@angular/core';
import { StudentService, StatisticService } from 'src/app/_service';
import { TextConstant, Status, NumberConstant } from 'src/app/_constant';
import { Student } from 'src/app/_entity';

@Component({
  selector: 'app-students-line-chart',
  templateUrl: './students-line-chart.component.html',
  styleUrls: ['./students-line-chart.component.css']
})
export class StudentsLineChartComponent implements OnInit {

  // handler with ngx-charts
  dataChart: any[]
  dataActiveStudents = []
  dataDeactiveStudents = []

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = true;
  showLegend = true;
  legendPosition = 'below'
  legendTitle = 'Mô tả'
  showXAxisLabel = true;
  xAxisLabel = 'Thời gian';
  showYAxisLabel = true;
  yAxisLabel = 'Giá trị';
  timeline = false;
  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  constructor(private statisticService: StatisticService) { }

  async ngOnInit() {
    await this.countActiveStudents()
    await this.countDeactiveStudents()
    await this.iniDataChart()
  }

  async countActiveStudents() {
    return new Promise((resolve, reject) => {
      this.statisticService.countStudentsByTenYear(Status.ACTIVE)
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {
          reject()
        } else {
          res.result.map(data => {
            this.dataActiveStudents.push(
              (({xValue, yValue}) => ({
                name: yValue,
                value: xValue
              }))(data)
            )
          })
          console.log(this.dataActiveStudents)
          resolve()
        }
      })
    })
    
  }

  async countDeactiveStudents() {
    return new Promise((resolve, reject) => {
      this.statisticService.countStudentsByTenYear(Status.DEACTIVE)
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {
          reject()
        } else {
          res.result.map(data => {
            this.dataDeactiveStudents.push(
              (({xValue, yValue}) => ({
                name: yValue,
                value: xValue
              }))(data)
            )
          })
          console.log(this.dataActiveStudents)
          resolve()
        }
      })
    })
    
  }

  iniDataChart() {
    console.log('init chart')
    this.dataChart = [
      {
        name: 'Active',
        series: this.dataActiveStudents
      },
      {
        name: 'Deactive',
        series: this.dataDeactiveStudents
      }
    ]
  }
}
