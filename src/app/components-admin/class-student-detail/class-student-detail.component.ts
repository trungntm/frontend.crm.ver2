import { Component, OnInit } from "@angular/core";
import { Student } from "src/app/_entity";
import { StudentService, ClassService } from "src/app/_service";
import { TextConstant, Status, NumberConstant } from "src/app/_constant";
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/api';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

@Component({
  selector: "app-class-student-detail",
  templateUrl: "./class-student-detail.component.html",
  styleUrls: ["./class-student-detail.component.css"]
})
export class ClassStudentDetailComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;

  allStudents: Student[];
  selectedStudents: Student[];
  classId

  constructor(
    private studentService: StudentService,
    private classService: ClassService,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private toastrService: ToastrService
  ) {}

  async ngOnInit() {
    if (this.config.data && this.config.data.classId) {
      this.classId = this.config.data.classId
    }

    await this.getAllStudents();
    this.selectedStudents = [];
  }

  async getAllStudents() {
    await this.studentService
      .getStudentsByPage(
        TextConstant.EMPTY,
        Status.ACTIVE,
        NumberConstant.PAGE_INDEX_DEFAULT,
        1000000000
      )
      .subscribe(res => {
        console.log(res);
        if (res.errors && res.errors.length > 0) {
        } else {
          this.allStudents = res.data;
        }
      });
  }

  closePopup() {
    this.ref.close(undefined)
  }

  async onSubmit() {
    this.blockUI.start(TextConstant.LOADING)
    await this.classService.insertStudentsInToClass(this.classId, this.selectedStudents)
      .subscribe(res => {
        console.log(res)
        if (res.body.errors && res.body.errors.length > 0) {
          this.toastrService.error('Thêm học viên vào lớp học thất bại. Lỗi người dùng!', 'Thông báo')
        } else {
          this.toastrService.success('Thêm học viên vào lớp học thành công!', 'Thông báo')
          this.ref.close(res.body)
        }
        this.blockUI.stop()
      }, err => {
        this.toastrService.error('Thêm học viên vào lớp học thất bại. Lỗi hệ thống!', 'Thông báo')
        this.blockUI.stop()
      })
  }
}
