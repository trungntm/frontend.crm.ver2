import { Component, OnInit } from '@angular/core';
import { User, Role } from 'src/app/_entity';
import { RoleService, UsersService } from 'src/app/_service';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/api';
import { ToastrService } from 'ngx-toastr';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { TextConstant, NumberConstant } from 'src/app/_constant';
import { first } from 'rxjs/operators';
import { GridDataResult } from '@progress/kendo-angular-grid';

@Component({
  selector: 'app-role-user-config',
  templateUrl: './role-user-config.component.html',
  styleUrls: ['./role-user-config.component.css']
})
export class RoleUserConfigComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  users: User[]
  role: Role
  gridDataUser: GridDataResult
  roleId
  searchText
  pageSize 
  pageIndex
  skip
  constructor(
    private roleService: RoleService,
    private userService: UsersService,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private toastrService: ToastrService
  ) {
    this.role = new Role()
    this.pageSize = NumberConstant.PAGE_SIZE_DEFAULT
    this.pageIndex = NumberConstant.PAGE_INDEX_DEFAULT
    this.skip = 0
   }

  ngOnInit() {
    if (this.config.data && this.config.data.roleId) {
      this.roleId = this.config.data.roleId
      this.getById(this.roleId)
    } 
  }

  async getById(roleId) {
    this.blockUI.start(TextConstant.LOADING)
    await this.roleService.retrieveRoleById(roleId)
      .pipe(first())
      .subscribe(async res => {
        if (res.errors && res.errors.length > 0) {

        } else {
          this.role = res.result
          await this.getUserByRole(this.roleId, TextConstant.EMPTY, this.pageIndex, this.pageSize)
        }
        this.blockUI.stop()
      })
  }

  async getUserByRole(roleId, keyword = TextConstant.EMPTY, page = this.pageIndex, size = this.pageSize) {
    await this.userService.retrieveUsersByRole(roleId, keyword, page, size)
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {

        } else {
          this.gridDataUser = <GridDataResult> {
            data: res.result,
            total: res.result.length
          }
        }
      })
  }

}
