import { Component, OnInit } from '@angular/core';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {ContractType, IFilterRequest, IPDropdown} from '../../_entity';
import {GridDataResult, PageChangeEvent} from '@progress/kendo-angular-grid';
import {NumberConstant, TextConstant} from '../../_constant';
import * as $ from "jquery";
import {Router} from '@angular/router';
import {ContractEmployeeService, ContractTypeService} from '../../_service';
import {getStatus} from '../../_helper';

@Component({
  selector: 'app-employee-contract',
  templateUrl: './employee-contract.component.html',
  styleUrls: ['./employee-contract.component.css']
})
export class EmployeeContractComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  protected searchText: string;
  protected pStatus: IPDropdown[] = [];
  protected contractTypesDisplay: IPDropdown[] = [];
  protected typeFilter: number  = 0;
  protected statusFilter: boolean;
  protected kendoLoading = false;
  protected gridData: GridDataResult;
  protected pageIndex: number = NumberConstant.PAGE_INDEX_DEFAULT;
  protected pageSize: number = NumberConstant.PAGE_SIZE_DEFAULT;
  protected skip: number = 0;
  protected paramsReq: IFilterRequest;
  constructor(private router:Router, private contractEmployeeService: ContractEmployeeService, private contractTypeService: ContractTypeService) {
    this.searchText = TextConstant.EMPTY;
    this.statusFilter = true;
    this.paramsReq = { keyword: TextConstant.EMPTY, status: true, contractType: this.typeFilter, page: this.pageIndex, size: this.pageSize };
  }

  ngOnInit() {
    this.pStatus = [...getStatus()];
    this.contractTypeService.retrieveAll()
      .subscribe(res => {
        if (res.result && res.result.length > 0) {
          res.result.forEach((data: ContractType) => {
            this.contractTypesDisplay.push({
              label: data.name,
              value: data.id
            })
          })
        }
      });
    this.retrieveByParams(this.paramsReq);

  }

  ngAfterViewInit() {
    this.resizeGrid();
  }

  protected retrieveByParams(params: IFilterRequest) {
    this.kendoLoading = true;
    this.contractEmployeeService.retrieveByParams(params)
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {
          this.gridData = <GridDataResult> {
            data: [],
            total: 0
          }
        } else {
          this.gridData = <GridDataResult> {
            data: res.result,
            total: res.totalItems
          }
        }
        this.kendoLoading = false;
      }, err => {
        this.kendoLoading = false;
        this.gridData = <GridDataResult> {
          data: [],
          total: 0
        }
      })
  }

  pageChange({ skip, take }: PageChangeEvent): void {
    this.pageIndex = skip / take;
    this.pageSize = take;
    this.skip = skip;
    this.paramsReq = { keyword: this.searchText, status: this.statusFilter, contractType: this.typeFilter, page: this.pageIndex, size: this.pageSize };


    this.retrieveByParams(this.paramsReq);
  }

  onFilter() {
    this.paramsReq = { keyword: this.searchText, status: this.statusFilter, contractType: this.typeFilter, page: this.pageIndex, size: this.pageSize };
    this.retrieveByParams(this.paramsReq);
  }

  onCancelFilter() {
    this.searchText = TextConstant.EMPTY;
    this.statusFilter = true;
    this.typeFilter = 0;
    this.paramsReq = { keyword: this.searchText, status: this.statusFilter, contractType: this.typeFilter, page: this.pageIndex, size: this.pageSize };
    this.retrieveByParams(this.paramsReq);
  }

  private resizeGrid() {
    var grid = $("#grid");
    var offsetbottom =
      parseInt($(window).height()) - parseInt(grid.offset().top);
    var gridElement = grid,
      dataArea = gridElement.find(".k-grid-content"),
      heightHeader = $(".k-grid-header", grid).outerHeight() || 0,
      heightToolbar = $(".k-grid-toolbar", grid).outerHeight() || 0,
      heightFooter = $(".k-pager-wrap", grid).outerHeight() || 0;
    dataArea.height(
      offsetbottom - heightHeader - heightToolbar - heightFooter - 25
    );
  }

  onExportContract(id: number) {
    this.contractEmployeeService.exportContractEmployee(id)
      .subscribe(res => {
        console.log(res);
      })
  }
}
