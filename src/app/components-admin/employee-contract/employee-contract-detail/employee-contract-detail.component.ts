import { Component, OnInit } from '@angular/core';
import {ContractClauseService, ContractEmployeeService, ContractTypeService, EmployeeService} from '../../../_service';
import {ContractClause, ContractEmployee, ContractType, Employee, IPDropdown} from '../../../_entity';
import {getStatus} from '../../../_helper';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {ContractEmployeeRule} from '../../../_entity/contract-employee-rule';
import {ToastrService} from 'ngx-toastr';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import {TextConstant} from '../../../_constant';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-employee-contract-detail',
  templateUrl: './employee-contract-detail.component.html',
  styleUrls: ['./employee-contract-detail.component.css']
})
export class EmployeeContractDetailComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  public Editor = ClassicEditor;
  contractTypesDisplay: IPDropdown[] = [];
  contractTypes: ContractType[] =[];

  statusDisplay: IPDropdown[] = [];

  employeesDisplay: IPDropdown[] = [];
  employees: Employee[] = [];
  contractEmployee: ContractEmployee;
  contractClauses: ContractClause[] = [];
  contractEmployeeRules: ContractEmployeeRule[] = [];
  protected disable = false;
  private id: number;
  constructor(private contractTypeService: ContractTypeService,
              private employeeService: EmployeeService,
              private contractClauseService: ContractClauseService,
              private contractEmployeeService: ContractEmployeeService,
              private toastrService: ToastrService,
              private activatedRoute: ActivatedRoute) {
    this.contractEmployee = new ContractEmployee();
  }

  ngOnInit() {
    this.contractTypeService.retrieveAll()
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {

        } else {
          this.contractTypes = [...res.result];
          res.result.forEach((data: ContractType) => {
            this.contractTypesDisplay.push({
              label: data.name,
              value: data.id
            })
          })
        }
      });

    this.statusDisplay = [...getStatus()];

    this.employeeService.retrieveAll()
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {

        } else {
          this.employees = [...res.result];
          res.result.forEach((data: Employee) => {
            this.employeesDisplay.push({
              label: data.name,
              value: data.id
            })
          })
        }
      });

    this.activatedRoute.params.subscribe(params => {
      if (params && params.id) {
        this.id = params.id;
        this.blockUI.start(TextConstant.LOADING);
        this.contractEmployeeService.retrieveById(this.id)
          .subscribe(res => {
            if (res.errors && res.errors.length > 0) {

            } else {
              this.contractEmployee = res.result;
              this.contractEmployee.startDate = new Date(this.contractEmployee.startDate);
              this.contractEmployee.endDate = new Date(this.contractEmployee.endDate);
              this.disable = true;
            }
            this.blockUI.stop();
          })
      }
    })
  }

  onChangeEmployee() {
    this.contractEmployee.employee = this.findEmployeeById(this.contractEmployee.employee.id);
    this.contractEmployee.employee.birthDay = new Date(this.contractEmployee.employee.birthDay);
  }

  onChangeContractType() {
    this.contractClauseService.retrieveByContractType(this.contractEmployee.contractType.id)
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {

        } else {
          this.contractClauses =[...res.result];
          this.contractEmployee.contractEmployeeRules = [...res.result];
        }
      })
  }

  findEmployeeById(id: number ): Employee {
    return this.employees.find(emp => emp.id === id);
  }

  findContractTypeById(id: number){
    return this.contractTypes.find(ct => ct.id === id);
  }

  onSubmit() {
    this.id ? this.onUpdateContractEmployee(this.contractEmployee): this.onCreateContractEmployee(this.contractEmployee);
  }

  onCreateContractEmployee(contractEmployeeDto: ContractEmployee) {
    this.blockUI.start(TextConstant.LOADING);
    this.contractEmployeeService.create(contractEmployeeDto)
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {
          res.errors.forEach(err => {
            this.toastrService.error(err.message);
          })
        } else {
          this.toastrService.success('Thêm mới hợp đồng thành công!');
        }
        this.blockUI.stop();
      }, err => {
        this.blockUI.stop();
        this.toastrService.error(err);
      })
  }

  onUpdateContractEmployee(contractEmployeeDto: ContractEmployee) {

  }

}
