import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/_service';
import { first } from 'rxjs/operators';
import { User } from 'src/app/_entity';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: [
    '../../../assets/admin/layout/css/themes/darkblue.css',
    './header.component.css',
  ]
})
export class HeaderComponent implements OnInit {

  principal: User;

  constructor(private authService: AuthService) {
    this.principal = new User()
  }

  async ngOnInit() {
    await this.retrieveLogged()
  }

  async retrieveLogged() {
    await this.authService.retrieveLoggedUser()
      .pipe(first())
      .subscribe(res => {
        if(res.errors && res.errors.length > 0) {
          this.principal = undefined
        } else {
          this.principal = res.result
        }
      }, err => {

      })
  }

}
