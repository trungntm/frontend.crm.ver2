import { Component, OnInit } from "@angular/core";
import * as $ from "jquery";
import {Class, Course, ClassLecture, IFilterRequest} from '../../_entity';
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { MatDialogService } from "../../_service/common";
import { ClassService, CourseService, EmployeeService } from "../../_service";
import { DynamicDialogRef, DynamicDialogConfig } from "primeng/api";
import { ToastrService } from "ngx-toastr";
import { first } from "rxjs/operators";
import { TextConstant, Status, NumberConstant } from "src/app/_constant";
import { GridDataResult, PageChangeEvent } from "@progress/kendo-angular-grid";
import {STATE_EMPLOYEE} from '../../_common/common';

@Component({
  selector: "app-class-detail",
  templateUrl: "./class-detail.component.html",
  styleUrls: ["./class-detail.component.css"]
})
export class ClassDetailComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  class: Class;
  courses: Course[];
  classLecture: ClassLecture;
  gridDataLecture: GridDataResult;
  id;
  pageSize;
  pageIndex;
  skip;
  searchText: string;
  currentLectureId;

  constructor(
    private classService: ClassService,
    private courseService: CourseService,
    private employeeService: EmployeeService,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private toastrService: ToastrService
  ) {
    this.class = new Class();
    this.classLecture = new ClassLecture();
    this.pageSize = NumberConstant.PAGE_SIZE_DEFAULT;
    this.pageIndex = NumberConstant.PAGE_INDEX_DEFAULT;
    this.skip = 0;
    this.searchText = TextConstant.EMPTY;
  }

  async ngOnInit() {
    await this.getAllCourse();
    if (this.config.data && this.config.data.id) {
      this.id = this.config.data.id;
      await this.retrieveById(this.id);
      await this.getClassLectureByClassId(this.id);
      await this.getAllEmployee();

    }
  }

  async retrieveById(classId) {
    this.blockUI.start(TextConstant.LOADING);
    this.classService
      .getClassById(classId)
      .pipe(first())
      .subscribe(
        res => {
          if (res.errors && res.errors.length) {
          } else {
            this.class = res.result;
            this.class.openDate = new Date(this.class.openDate);
            this.class.endDate = new Date(this.class.endDate);
          }

          this.blockUI.stop();
        },
        err => this.blockUI.stop()
      );
  }

  async getAllCourse() {
    return this.courseService
      .retrieveCourses(TextConstant.EMPTY, Status.ACTIVE, 0, 1000000)
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {
        } else {
          this.courses = res.result;
        }
      });
  }

  async getAllEmployee(
    keyword = this.searchText,
    state = STATE_EMPLOYEE.ACTIVATE,
    page = this.pageIndex,
    size = this.pageSize
  ) {
    const params:IFilterRequest = {keyword, state, page, size};
    return await this.employeeService
      .getEmployeesByPage(params)
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {
          this.gridDataLecture = <GridDataResult> {
            data: [],
            total: 0
          }
        } else {
          this.gridDataLecture = <GridDataResult> {
            data: res.result,
            total: res.totalItems
          }
        }
      });
  }

  pageChange({ skip, take }: PageChangeEvent): void {
    this.pageIndex = skip / take;
    this.pageSize = take;
    this.skip = skip;

    this.getAllEmployee();
  }

  async getClassLectureByClassId(classId) {
    await this.classService
      .getClassLectureByClasssId(classId)
      .pipe(first())
      .subscribe(async res => {
        if (res.errors && res.errors.length > 0) {
        } else {
          if (res.result.employee && res.result.employee.id) {
            this.currentLectureId = res.result.employee.id;
          }
        }
      });
  }

  async onToggleChooseLecture(e, lectureId) {
    if (e.checked === true) {
      await this.classService
        .setLectureForClass(this.id, lectureId)
        .pipe(first())
        .subscribe(
          res => {
            if (res.errors && res.errors.length > 0) {
              this.toastrService.error(
                "Thêm giảng viên cho lớp học thất bại. Lỗi người dùng!",
                "Thông báo"
              );
            } else {
              this.toastrService.success(
                "Thêm giảng viên cho lớp học thành công!",
                "Thông báo"
              );
            }
          },
          err => {
            this.toastrService.error(
              "Thêm giảng viên cho lớp học thất bại. Lỗi hệ thống!",
              "Thông báo"
            );
          }
        );
    }
  }

  async onSubmit() {
    if (this.id) {
      await this.updateClass(this.class);
    } else {
      await this.createClass(this.class);
    }
  }

  closePopup() {
    this.ref.close(undefined);
  }

  async createClass(classDto) {
    this.blockUI.start(TextConstant.LOADING);

    this.classService
      .createClass(classDto)
      .pipe(first())
      .subscribe(
        res => {
          if (res.body.errors && res.body.errors.length) {
            this.toastrService.error(
              "Thêm dữ liệu thất bại. Lỗi người dùng!",
              "Thông báo"
            );
          } else {
            this.toastrService.success("Thêm dữ liệu thành công!", "Thông báo");
            this.ref.close(res);
          }
          this.blockUI.stop();
        },
        err => {
          this.toastrService.error(
            "Thêm dữ liệu thất bại. Lỗi hệ thống!",
            "Thông báo"
          );
          this.blockUI.stop();
        }
      );
  }

  async updateClass(classDto) {
    this.blockUI.start(TextConstant.LOADING);

    this.classService
      .updateClass(classDto)
      .pipe(first())
      .subscribe(
        res => {
          console.log(res);
          if (res.body.errors && res.body.errors.length) {
            this.toastrService.error(
              "Cập nhật dữ liệu thất bại. Lỗi người dùng!",
              "Thông báo"
            );
          } else {
            this.toastrService.success(
              "Cập nhật dữ liệu thành công!",
              "Thông báo"
            );
            this.ref.close(res);
          }
          this.blockUI.stop();
        },
        err => {
          this.toastrService.error(
            "Cập nhật dữ liệu thất bại. Lỗi hệ thống!",
            "Thông báo"
          );
          this.blockUI.stop();
        }
      );
  }
}
