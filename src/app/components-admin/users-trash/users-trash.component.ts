import { Component, OnInit } from '@angular/core'
import * as $ from 'jquery'
import { NgBlockUI, BlockUI } from 'ng-block-ui'
import * as toastr from '../../../assets/global/plugins/bootstrap-toastr/toastr.min.js'
import { 
  User, EmployeeType 
} from '../../_entity'
import { 
  UsersService,
  EmployeeTypeService 
} from '../../_service'
import { PaginatorConstant, Status, TextConstant, ResponseMessage, HttpStatus } from '../../_constant'
import { MatDialogService } from '../../_service/common'
import { first } from 'rxjs/operators'

@Component({
  selector: 'app-users-trash',
  templateUrl: './users-trash.component.html',
  styleUrls: [
    './users-trash.component.css']
})
export class UsersTrashComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI
  
  users: User[]
  empTypes: EmployeeType[]
  page = 0
  current = 0
  pageNumber: number
  pageArray = []
  errors = []

  constructor(private dialogService: MatDialogService, 
    private userService: UsersService, private empTypeService: EmployeeTypeService) { }

    async ngOnInit() {
      await this.initView()
      await this.retrieveAllUserPaging(PaginatorConstant.keyword, Status.DEACTIVE, PaginatorConstant.page, PaginatorConstant.size)
    }
  
    async initView() {
      $('#menu-users').addClass('start active open')
      $('#li-users-trash').addClass('active')
  
      await this.onPageLoad()
    }
  
    async onPageLoad() {
      await this.empTypeService.getAllEmployeeTypes()
        .subscribe(res => {
          if (res.errors && res.errors.length > 0){
          } else {
            this.empTypes = res.result
          }
        })
    }
  
    async retrieveAllUserPaging(keyword, status, page, size) {
      this.blockUI.start(TextConstant.LOADING)
      await this.userService.retrieveAllUsersPaging(keyword, status, page, size)
        .subscribe(res => {
          if (res.errors && res.errors.length > 0) {
            res.errors.forEach(error => {
              this.errors.push(error)
            })

            this.errors.filter(x => x.status === HttpStatus.NOT_FOUND).map(x => console.log(x))
          } else {
            this.users = res.result
            this.pageNumber = res.pageNumber
            for (let index = 0; index < this.pageNumber; index++) {
              this.pageArray.push(index)
            }
            this.current = page
          }
          this.blockUI.stop()
  
        }, err => {
          this.blockUI.stop()
  
        })
    }
  
    async onReactiveUser(id) {
      const dialogData = {
        title: 'Thông báo',
        message: 'Khôi phục tài khoản?'
      }

      this.dialogService.openConfirmDialog(dialogData)
        .afterClosed()
        .subscribe(async res => {
          if (res) {
            this.blockUI.start(TextConstant.LOADING)
            await this.userService.reactiveUser(id)
              .pipe(first())
              .subscribe(async res => {
                if (res.errors && res.errors.length > 0) {
                  toastr.error(ResponseMessage.UPDATE_ERROR_USER)
                  this.blockUI.stop()
                } else {
                  toastr.success(ResponseMessage.UPDATE_SUCCESS)
                  await this.retrieveAllUserPaging(PaginatorConstant.keyword, Status.DEACTIVE, PaginatorConstant.page, PaginatorConstant.size)

                  this.blockUI.stop()
                }
              }, err => {
                toastr.error(ResponseMessage.UPDATE_ERROR_SYSTEM)
                this.blockUI.stop()
              })
          }
        })
    }

    async onDeleteRecord(id) {
      const dialogData = {
        id: 1,
        title: 'Thông báo',
        message: 'Xóa tài khoản?'
      }
  
      this.dialogService.openConfirmDialog(dialogData)
        .afterClosed()
        .subscribe(async res => {
          if (res) {
            this.blockUI.start(TextConstant.LOADING)
            await this.userService.onDeleteUser(id)
              .subscribe(async res => {
                if (res.errors && res.errors.length > 0) {
                  toastr.error(ResponseMessage.DELETE_ERROR_USER)
                } else {
                  toastr.success(ResponseMessage.DELETE_SUCCESS)
                  await this.retrieveAllUserPaging(PaginatorConstant.keyword, Status.DEACTIVE, PaginatorConstant.page, PaginatorConstant.size)
                }
              }, err => {
                toastr.error(ResponseMessage.DELETE_ERROR_SYSTEM)
                
              })
            this.blockUI.stop()
          }
        })
    }
  
    async onFilter() {
      const $keyword = $('#txtFilter').val().trim()
      const $empType = $('#sel-empType option:selected').val()
      this.pageArray = []
      await this.retrieveAllUserPaging($keyword, Status.DEACTIVE, PaginatorConstant.page, PaginatorConstant.size)
    }
  
    async onCancelFilter() {
      this.pageArray = []
      await this.retrieveAllUserPaging(TextConstant.EMPTY, Status.DEACTIVE, PaginatorConstant.page, PaginatorConstant.size)
  
    }
  
    onGotoPage(page: number) {
      const $keyword = $('#txtFilter').val().trim()
      this.pageArray = []
      this.retrieveAllUserPaging($keyword, Status.DEACTIVE, page, PaginatorConstant.size)
    }
  
  
    onPrePage() {
      const $keyword = $('#txtFilter').val().trim()
      if (this.current > 0) {
        this.current--
      }
      this.pageArray = []
  
      this.retrieveAllUserPaging($keyword, Status.DEACTIVE, this.current, PaginatorConstant.size)
    }
  
    onNextPage() {
      const $keyword = $('#txtFilter').val().trim()
      if (this.current < this.pageNumber - 1) {
        this.current++
      }
      this.pageArray = []
  
      this.retrieveAllUserPaging($keyword, Status.DEACTIVE, this.current, PaginatorConstant.size)
    }

}
