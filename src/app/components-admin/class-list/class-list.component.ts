import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import {Class, IFilterRequest, IPDropdown} from '../../_entity';
import { MatDialogService } from '../../_service/common';
import { ClassService } from '../../_service';
import { TextConstant, Status, NumberConstant } from 'src/app/_constant';
import { first } from 'rxjs/operators';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { DialogService } from 'primeng/api';
import { ToastrService } from 'ngx-toastr';
import { ClassDetailComponent } from '../class-detail/class-detail.component';
import { isNullOrUndefined } from 'util';
import {getStates} from '../../_helper';
import {STATE} from '../../_common/common';

@Component({
  selector: 'app-class-list',
  templateUrl: './class-list.component.html',
  styleUrls: [
    './class-list.component.css']
})
export class ClassListComponent implements OnInit {

  @BlockUI() blockUI: NgBlockUI
  classes: Class []
  gridData: GridDataResult
  pageSize 
  pageIndex
  skip
  courseAfterCreate
  searchText: string
  errors = []
  statusDefined;
  statusFilter;
  protected stateFilter: number = STATE.ALL;
  kendoLoading = false;
  pState: IPDropdown[] = [];

  constructor(private matDialogSrv: MatDialogService, private classService: ClassService, private primeDialogSrv: DialogService, private toastrSrv: ToastrService) {
    this.statusDefined = [
      {
        label: "Hoạt động",
        value: true
      },
      {
        label: "Ngưng hoạt động",
        value: false
      }
    ];

    this.pState = [...getStates()];

    this.pageSize = NumberConstant.PAGE_SIZE_DEFAULT;
    this.pageIndex = NumberConstant.PAGE_INDEX_DEFAULT;
    this.skip = 0;
    this.statusFilter = Status.ACTIVE;
    this.searchText = TextConstant.EMPTY;
   }

  async ngOnInit() {
    await this.getClassesByPage()
  }
  public ngAfterViewInit() {
    this.resizeGrid();
  }

  private resizeGrid() {
    var grid = $("#grid");
    var offsetbottom =
      parseInt($(window).height()) - parseInt(grid.offset().top);
    var gridElement = grid,
      dataArea = gridElement.find(".k-grid-content"),
      heightHeader = $(".k-grid-header", grid).outerHeight() || 0,
      heightToolbar = $(".k-grid-toolbar", grid).outerHeight() || 0,
      heightFooter = $(".k-pager-wrap", grid).outerHeight() || 0;
    dataArea.height(
      offsetbottom - heightHeader - heightToolbar - heightFooter - 25
    );
  }
  async getClassesByPage(keyword = TextConstant.EMPTY, status = Status.ACTIVE, state = this.stateFilter, pageIndex = this.pageIndex, pageSize = this.pageSize) {
    this.kendoLoading = true;
    const params:IFilterRequest = {keyword: keyword, status: status, state: state, page: pageIndex, size: pageSize};
    await this.classService.getClassesByPage(params)
      .subscribe(res => {
        this.gridData = res
        this.kendoLoading = false
      })
  }

  async getDataAfterCreate(e) {
    isNullOrUndefined(e)? undefined: await this.getClassesByPage()
  }

  showPopupEditClass(classId) {
    const ref = this.primeDialogSrv.open(ClassDetailComponent, {
      data: {
        id: classId
      },
      header: 'Thông tin lớp học',
      width: '70%'
    })

    ref.onClose.subscribe(async res => {
      isNullOrUndefined(res) ? undefined: await this.getClassesByPage()
    })
  }

  async deleteClass(id) {
    const dataDialog = {
      title: 'Xóa dữ liệu',
      message: 'Bạn chắc chắn muốn xóa dữ liệu?'
    };
    return this.matDialogSrv.openConfirmDialog(dataDialog)
      .afterClosed()
      .subscribe(async res => {
        if (res) {
          this.blockUI.start(TextConstant.LOADING)

          await this.classService.deleteClass(id)
            .pipe(first())
            .subscribe(async res => {
              if (res.errors && res.errors.length > 0) {
                this.toastrSrv.error('Xóa dữ liệu thất bại. Lỗi người dùng!', 'Thông báo')
              } else {
                this.toastrSrv.success('Xóa dữ liệu thành công!', 'Thông báo')
                await this.getClassesByPage()
              }
              this.blockUI.stop()
            }, err => {
              this.toastrSrv.error('Xóa dữ liệu thất bại. Lỗi hệ thống!', 'Thông báo')
              this.blockUI.stop()
            })
        }
      })
  }
  
  pageChange({ skip, take }: PageChangeEvent): void {
    this.pageIndex = skip/take;
    this.pageSize = take;
    this.skip = skip;

    this.getClassesByPage(this.searchText.trim(), this.statusFilter, this.stateFilter, this.pageIndex, this.pageSize)
  }

  async onFilter() {
    await this.getClassesByPage(this.searchText.trim(), this.statusFilter, this.stateFilter, this.pageIndex, this.pageSize)
  }

  async onCancelFilter() {
    this.searchText = TextConstant.EMPTY;
    this.statusFilter = Status.ACTIVE;
    this.stateFilter = STATE.ALL;
    await this.getClassesByPage();
  }
}
