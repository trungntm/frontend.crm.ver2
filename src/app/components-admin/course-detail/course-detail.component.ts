import { Component, OnInit } from "@angular/core";
import { Course, CourseCategory } from "../../_entity";
import { CourseService, CourseCategoryService } from "../../_service";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import * as ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import { first, reduce } from "rxjs/operators";
import { TextConstant, Status } from "src/app/_constant";
import { DynamicDialogRef, DynamicDialogConfig } from "primeng/api";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-course-detail",
  templateUrl: "./course-detail.component.html",
  styleUrls: [
    "../../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css",
    "./course-detail.component.css"
  ]
})
export class CourseDetailComponent implements OnInit {
  id;
  @BlockUI() blockUI: NgBlockUI;
  public Editor = ClassicEditor;
  course: Course;
  courseCategories: CourseCategory[];
  imageSelected;
  constructor(
    private courseService: CourseService,
    private courseCatService: CourseCategoryService,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private toastrService: ToastrService
  ) {
    this.course = new Course();
  }

  async ngOnInit() {
    await this.retrieveCourseCaterories();

    if (this.config.data && this.config.data.id) {
      this.id = this.config.data.id;
      await this.retrieveCourseById(this.id);
    }
  }

  async retrieveCourseById(id) {
    return await this.courseService
      .retrieveCourseById(id)
      .pipe(first())
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {
        } else {
          this.course = res.result;
        }
      });
  }

  async retrieveCourseCaterories() {
    return await this.courseCatService
      .retrieveCourseCategories(TextConstant.EMPTY, Status.ACTIVE)
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {
        } else {
          this.courseCategories = res.result;
        }
      });
  }

  async onSubmit() {
    if (this.id) {
      return await this.updateCourse();
    } else {
      return await this.createCourse();
    }
  }

  async updateCourse() {
    this.blockUI.start(TextConstant.LOADING);
    return await this.courseService
      .updateCourse(this.id, this.course)
      .pipe(first())
      .subscribe(
        res => {
          if (res.errors && res.errors.length > 0) {
            this.toastrService.error(
              "Cập nhật dữ liệu thất bại. Lỗi người dùng!",
              "Thông báo"
            );
          } else {
            this.toastrService.success(
              "Cập nhật dữ liệu thành công!",
              "Thông báo"
            );
            this.ref.close(res);
          }

          this.blockUI.stop();
        },
        err => {
          this.toastrService.error(
            "Cập nhật dữ liệu thất bại. Lỗi hệ thống!",
            "Thông báo"
          );
          this.blockUI.stop();
        }
      );
  }

  async createCourse() {
    this.blockUI.start(TextConstant.LOADING);
    return await this.courseService
      .createCourse(this.course)
      .pipe(first())
      .subscribe(
        res => {
          if (res.errors && res.errors.length > 0) {
            this.toastrService.error(
              "Thêm dữ liệu thất bại. Lỗi người dùng!",
              "Thông báo"
            );
          } else {
            this.toastrService.success("Thêm dữ liệu thành công!", "Thông báo");
            this.ref.close(res);
          }
          this.blockUI.stop();
        },
        err => {
          this.toastrService.error(
            "Thêm dữ liệu thất bại. Lỗi hệ thống!",
            "Thông báo"
          );
          this.blockUI.stop();
        }
      );
  }

  closePopup() {
    this.ref.close(undefined);
  }

  async onFileSelected(e) {
    if (e.target.files && e.target.files[0]) {
      this.imageSelected = e.target.files[0];
      const reader = new FileReader();
      reader.onload = (event: any) => {
        this.course.imgPath = event.target.result;
      };
      reader.readAsDataURL(e.target.files[0]);
    }

    // reader.onload = () => {
    //   this.formImage.get('avatar').setValue({
    //     filename: this.imageSelected.name,
    //     filetype: this.imageSelected.type,
    //     value: `data:${this.imageSelected.type};base64,${reader.result.split(',')[1]}`
    //   })
    // }
  }

  async onChangeImage() {
    const formData = new FormData();
    formData.append("image", this.imageSelected, this.imageSelected.name);

    this.blockUI.start(TextConstant.LOADING);
    await this.courseService
      .changeImage(this.id, formData)
      .pipe(first())
      .subscribe(
        res => {
          if (res.body.errors && res.body.errors.length > 0) {
            this.toastrService.error(
              "Cập nhật hình ảnh thất bại. Lỗi người dùng!",
              "Thông báo"
            );
          } else {
            this.toastrService.success(
              "Cập nhật hình ảnh thành công!",
              "Thông báo"
            );
          }
          this.blockUI.stop();
        },
        err => {
          this.toastrService.error(
            "Cập nhật dữ liệu thất bại. Lỗi hệ thống!",
            "Thông báo"
          );
          this.blockUI.stop();
        }
      );
  }
}
