import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import * as $ from 'jquery';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { CourseCategory } from '../../_entity';
import { PaginatorConstant, PREFIX, TextConstant, Status, NumberConstant } from '../../_constant';
import { CourseCategoryService } from '../../_service';
import { MatDialogService } from 'src/app/_service/common';
import { DialogService } from 'primeng/api';
import { CourseCategoryDetailComponent } from '../course-category-detail/course-category-detail.component';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';

@Component({
  selector: 'app-course-categories-list',
  templateUrl: './course-categories-list.component.html',
  styleUrls: [
    './course-categories-list.component.css']
})
export class CourseCategoriesListComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI
  courseCategories: CourseCategory []
  gridData: GridDataResult
  pageSize 
  pageIndex
  skip
  courseCatAfterCreate
  searchText: string
  kendoLoading = false
  constructor(private courseCatService: CourseCategoryService, private matDialogSrv: MatDialogService, private primeDialogSrv: DialogService) {
    this.pageSize = NumberConstant.PAGE_SIZE_DEFAULT
    this.pageIndex = NumberConstant.PAGE_INDEX_DEFAULT
    this.skip = 0
    this.searchText = TextConstant.EMPTY
   }

  async ngOnInit() {
    await this.getCourseCatByPage()
  }
  public ngAfterViewInit() {
    this.resizeGrid();
  }

  private resizeGrid() {
    var grid = $("#grid");
    var offsetbottom =
      parseInt($(window).height()) - parseInt(grid.offset().top);
    var gridElement = grid,
      dataArea = gridElement.find(".k-grid-content"),
      heightHeader = $(".k-grid-header", grid).outerHeight() || 0,
      heightToolbar = $(".k-grid-toolbar", grid).outerHeight() || 0,
      heightFooter = $(".k-pager-wrap", grid).outerHeight() || 0;
    dataArea.height(
      offsetbottom - heightHeader - heightToolbar - heightFooter - 25
    );
  }
  async getCourseCatByPage(keyword = TextConstant.EMPTY, status = Status.ACTIVE, pageIndex = this.pageIndex, pageSize = this.pageSize) {
    this.kendoLoading = true
    await this.courseCatService.getCourseCategoriesByPage(keyword, status, pageIndex, pageSize)
      .subscribe(res => {
        if( res.errors && res.errors.length > 0) {
          this.gridData = <GridDataResult> {
            total: 0,
            data: []
          }
        } else {
          this.gridData = <GridDataResult> {
            total: res.totalItems,
            data: res.result
          }
        }
        this.kendoLoading = false
      })
  }

  pageChange({ skip, take }: PageChangeEvent): void {
    this.pageIndex = skip/take;
    this.pageSize = take
    this.skip = skip

    this.getCourseCatByPage(this.searchText.trim(), Status.ACTIVE, this.pageIndex, this.pageSize)
  }

  
  showPopupEditCourseCat(courseCatId) {
    const ref = this.primeDialogSrv.open(CourseCategoryDetailComponent, {
      data: {
        id: courseCatId
      },
      header: 'Thông tin loại khóa học',
      width: '50%'
    })

    ref.onClose.subscribe(async res => {
      await this.getCourseCatByPage()
    })
  }

  async deleteCourseCat(courseCatId) {
    //TODO : Delete course category
  }

  async onFilter() {
    await this.getCourseCatByPage(this.searchText.trim(), Status.ACTIVE, this.pageIndex, this.pageSize)
  }

  async onCancelFilter() {
    this.searchText = TextConstant.EMPTY;
    await this.getCourseCatByPage()
  }
}
