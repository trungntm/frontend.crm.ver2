import { Component, OnInit, Input } from '@angular/core';
import { StudentService, CourseService, ClassService, InvoiceService } from 'src/app/_service';
import { ToastrService } from 'ngx-toastr';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/api';
import { Invoice, Student, Class } from 'src/app/_entity';
import { first } from 'rxjs/operators';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { TextConstant } from 'src/app/_constant';

@Component({
  selector: 'app-payment-tuition-fees',
  templateUrl: './payment-tuition-fees.component.html',
  styleUrls: ['./payment-tuition-fees.component.css']
})
export class PaymentTuitionFeesComponent implements OnInit {

  @BlockUI() blockUI: NgBlockUI
  studentId
  classId
  invoice: Invoice
  student: Student
  class: Class
  constructor(private studentService: StudentService,
    private classService: ClassService,
    private invoiceService: InvoiceService,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private toastrService: ToastrService) {
      this.student = new Student()
      this.class = new Class()
      this.invoice = new Invoice()
     }

  ngOnInit() {
    if (this.config.data) {
      if (this.config.data.studentId) {
        this.studentId = this.config.data.studentId
        this.retrieveStudentById(this.studentId)
      }
      
      if (this.config.data.classId) {
        this.classId = this.config.data.classId
        this.retrieveClassById(this.classId)
      }
    }
  }

  async retrieveStudentById(studentId) {
    await this.studentService.getById(studentId)
      .pipe(first())
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {

        } else {
          this.student = res.result
        }
      })
  }

  async retrieveClassById(classId) {
    await this.classService.getClassById(classId)
      .pipe(first())
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {

        } else {
          this.class = res.result
        }
      })
  }

  async onSubmit() {
    await this.createInvoice(this.invoice)
  }

  async createInvoice(invoiceDto) {
    invoiceDto.student = this.student
    invoiceDto.aclass = this.class
    this.blockUI.start(TextConstant.LOADING)
    await this.invoiceService.createInvoice(invoiceDto)
      .pipe(first())
      .subscribe(res => {
        if (res.body.errors && res.body.errors.length > 0) {
          this.toastrService.error('Thanh toán không thành công. Lỗi người dùng!', 'Thông báo')
        } else {
          this.toastrService.success('Thanh toán thành công!', 'Thông báo')
          this.ref.close(res.body)
        }
        this.blockUI.stop()
      }, err => {
        this.toastrService.error('Thanh toán không thành công. Lỗi hệ thống!', 'Thông báo')
        this.blockUI.stop()
      })

  }

  closePopup() {
    this.ref.close();
  }

}
