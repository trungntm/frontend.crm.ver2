import { Component, OnInit } from "@angular/core";
import * as $ from "jquery";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { Course } from "../../_entity";
import {
  TextConstant,
  Status,
  NumberConstant
} from "../../_constant";
import { CourseService } from "../../_service";
import { MatDialogService } from "src/app/_service/common";
import { DialogService, MenuItem } from "primeng/api";
import { CourseDetailComponent } from "../course-detail/course-detail.component";
import { GridDataResult, PageChangeEvent } from "@progress/kendo-angular-grid";
import { first } from "rxjs/operators";
import { ToastrService } from 'ngx-toastr';
import { isNullOrUndefined } from 'util';

@Component({
  selector: "app-courses-list",
  templateUrl: "./courses-list.component.html",
  styleUrls: ["./courses-list.component.css"]
})
export class CoursesListComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  courses: Course[];
  gridData: GridDataResult;
  pageSize;
  pageIndex;
  skip;
  courseAfterCreate;
  searchText: string;
  kendoLoading = false
  menuItem: MenuItem[]
  constructor(
    private courseService: CourseService,
    private matDialogSrv: MatDialogService,
    private primeDialogSrv: DialogService,
    private toastrService: ToastrService
  ) {
    this.menuItem = [
      {
        label: 'Thông tin',
        icon: 'fa fa-info'
      },
      {
        label: 'Học phí',
        icon: 'fa fa-money',
      }
    ]

    this.pageSize = NumberConstant.PAGE_SIZE_DEFAULT;
    this.pageIndex = NumberConstant.PAGE_INDEX_DEFAULT;
    this.skip = 0;
    this.searchText = TextConstant.EMPTY;
  }

  async ngOnInit() {
    await this.getCoursesByPage();
  }

  public ngAfterViewInit() {
    this.resizeGrid();
  }

  private resizeGrid() {
    var grid = $("#grid");
    var offsetbottom =
      parseInt($(window).height()) - parseInt(grid.offset().top);
    var gridElement = grid,
      dataArea = gridElement.find(".k-grid-content"),
      heightHeader = $(".k-grid-header", grid).outerHeight() || 0,
      heightToolbar = $(".k-grid-toolbar", grid).outerHeight() || 0,
      heightFooter = $(".k-pager-wrap", grid).outerHeight() || 0;
    dataArea.height(
      offsetbottom - heightHeader - heightToolbar - heightFooter - 25
    );
  }
  async getCoursesByPage(
    keyword = TextConstant.EMPTY,
    status = Status.ACTIVE,
    pageIndex = this.pageIndex,
    pageSize = this.pageSize
  ) {
    this.kendoLoading = true
    await this.courseService
      .getCoursesByPage(keyword, status, pageIndex, pageSize)
      .subscribe(res => {
        this.gridData = res;
        this.kendoLoading = false
      });
  }

  pageChange({ skip, take }: PageChangeEvent): void {
    this.pageIndex = skip / take;
    this.pageSize = take;
    this.skip = skip;

    this.getCoursesByPage(
      this.searchText.trim(),
      Status.ACTIVE,
      this.pageIndex,
      this.pageSize
    );
  }

  deleteCourse(courseId) {
    const dialogData = {
      title: "Xóa dữ liệu",
      message: "Bạn chắc chắn muốn xóa dữ liệu?"
    };

    this.matDialogSrv
      .openConfirmDialog(dialogData)
      .afterClosed()
      .subscribe(async res => {
        if (res) {
          this.blockUI.start(TextConstant.LOADING);
          this.courseService
            .deleteCourse(courseId)
            .pipe(first())
            .subscribe(async res => {
              if (res.errors && res.errors.length) {
                res.errors.forEach(error => {
                  this.toastrService.error(error.message, 'Thông báo')
                });
              } else {
                this.toastrService.success('Xóa dữ liệu thành công!', 'Thông báo')
                await this.getCoursesByPage()
              }

              this.blockUI.stop()
            }, err => {
              this.toastrService.error('Xóa dữ liệu thất bại. Lỗi hệ thống!', 'Thông báo')
              this.blockUI.stop()
            });
        }
      });
  }

  showPopupEditCourse(courseId) {
    const ref = this.primeDialogSrv.open(CourseDetailComponent, {
      data: {
        id: courseId
      },
      header: "Thông tin khóa học",
      width: "80%"
    });

    ref.onClose.subscribe(async res => {
      res ? await this.getCoursesByPage() :  undefined;
    });
  }

  async onFilter() {
    await this.getCoursesByPage(
      this.searchText.trim(),
      Status.ACTIVE,
      this.pageIndex,
      this.pageSize
    );
  }

  async onCancelFilter() {
    this.searchText = TextConstant.EMPTY;
    await this.getCoursesByPage();
  }

  async getDataAfterCreate(e) {
    e ? this.getCoursesByPage(): undefined;
  }

  recieveFormDataToImportExcel(e) {
    console.log(e)
  }
}
