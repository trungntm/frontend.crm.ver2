import { Component, OnInit } from "@angular/core";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { StatisticService } from "../../_service";
import { Status } from 'src/app/_constant';
import { DashboardService } from 'src/app/_service/dashboard.service';
import { IChartData } from 'src/app/components-common/line-chart/chart-data';
import { CalendarService } from 'src/app/_service/calendar.service';
import { IFilterRequest } from 'src/app/_entity';
import { CalendarEvent } from 'angular-calendar';
import { getColorByState } from 'src/app/_helper/color-helper';
import { addMonths, subMonths } from 'date-fns';

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: [
    "./dashboard.component.css",
  ]
})
export class DashboardComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;

  labelStudentsInCourseCategory
  valueStudentInCourseCategory
  pieDataStudentsInCourseCategory
  totalStudents: number = 0;
  totalLectures: number = 0;
  totalCourses: number = 0;
  chartStudents: IChartData[] = [];
  chartStudentsByCourseCategory:any [] = [];
  classesDisplayCalendar: CalendarEvent[] = [];
  currentDate: Date = new Date();
  constructor(
    private dashboardService: DashboardService,
    private statisticService: StatisticService,
    private calendarService: CalendarService
  ) {

    this.labelStudentsInCourseCategory = []
    this.valueStudentInCourseCategory = []
  }

  async ngOnInit() {
    const paramsReq: IFilterRequest = { month: this.currentDate.getMonth() + 1, year: this.currentDate.getFullYear() };
    await this.countData();
    await this.statisticStudentsTenYear()
    await this.statisticStudentsByCourseCategory();
    await this.retrieveClassesToCalendar(paramsReq);
  }

  async countData() {
    await this.dashboardService.countStudents()
      .subscribe((res:number) => {
        this.totalStudents = res;
      })
    
    await this.dashboardService.countLectures()
      .subscribe((res:number) => {
        this.totalLectures = res;
      })
    
    await this.dashboardService.countCourses()
      .subscribe((res: number) => {
        this.totalCourses = res;
      })
  }

  statisticStudentsTenYear() {
    this.statisticService.countStudentsByTenYear(Status.ACTIVE)
      .subscribe(res => {
        let chartDataStudents:IChartData[] = [];
        let studentsTenYear = [];

        res.result.map(item => {
          studentsTenYear.push({
            name: item.yValue,
            value: item.xValue
          });
        });
        chartDataStudents = [{name: "Số học viên", series: studentsTenYear}];
        this.chartStudents = [...chartDataStudents];
      })
  }
  
  statisticStudentsByCourseCategory() {
    this.dashboardService.countStudentsByCourseCategory()
      .subscribe(res => {
        let charData:IChartData[] = [];
        let chartDataTemp = [];

        res.map(item => {
          chartDataTemp.push({
            name: item.xValue,
            value: item.yValue
          })
        })

        charData = [{name: 'Số học viên', series: [...chartDataTemp]}]
        this.chartStudentsByCourseCategory = [...charData[0].series]
      })
  }

  protected retrieveClassesToCalendar(params: IFilterRequest) {
    return this.calendarService.retrieveOpenClassesToCalendar(params)
      .subscribe(res => {
        let eventsTemp:CalendarEvent[] = [];
        if (res.errors && res.errors.length > 0) {
          this.classesDisplayCalendar = [...this.classesDisplayCalendar, ...eventsTemp];
        } else {
          this.classesDisplayCalendar = [];
          res.result.forEach(element => {
            eventsTemp.push({
              title: element.title,
              start: new Date(element.startDate),
              end: new Date(element.endDate),
              color: getColorByState(element.state)
            })
          });
        }

        this.classesDisplayCalendar = [...this.classesDisplayCalendar, ...eventsTemp];
        let eventsTempEnd: CalendarEvent[] = []; 
        this.calendarService.retrieveEndClassesToCalendar(params)
          .subscribe(res => {
            if (res.errors && res.errors.length > 0) {
              this.classesDisplayCalendar = [...this.classesDisplayCalendar, ...eventsTempEnd];
            } else {
              res.result.forEach(item => {
                eventsTempEnd.push({
                  title: item.title,
                  start: new Date(item.startDate),
                  end: new Date(item.endDate),
                  color: getColorByState(2)
                });
              })
            }
            this.classesDisplayCalendar = [...this.classesDisplayCalendar, ...eventsTempEnd];
          })
      })
  }

  onReceiveClickNextPreviousCalendar(data) {
    if (data.value === true) {
      const params = { year: this.currentDate.getFullYear(), month: addMonths(data.viewDate, 1).getMonth() };
      this.retrieveClassesToCalendar(params);
    } else {
      const params = { year: this.currentDate.getFullYear(), month: subMonths(data.viewDate, 1).getMonth() + 2};
      this.retrieveClassesToCalendar(params);
    }
  }
}
