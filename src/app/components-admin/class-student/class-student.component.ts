import { Component, OnInit } from "@angular/core";
import { StudentService, ClassService, EmployeeService, EmployeeTypeService } from "src/app/_service";
import { ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { TextConstant, NumberConstant, Status } from "src/app/_constant";
import { first, map } from "rxjs/operators";
import { Class, Employee } from "src/app/_entity";
import { GridDataResult } from "@progress/kendo-angular-grid";
import * as $ from 'jquery';

@Component({
  selector: "app-class-student",
  templateUrl: "./class-student.component.html",
  styleUrls: ["./class-student.component.css"]
})
export class ClassStudentComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  classId;
  class: Class;
  gridData: GridDataResult;
  pageSize;
  pageIndex;
  skip;
  errors = [];
  textFilter;
  statusFilter;
  statusDefined;
  lecture: Employee
  kendoLoading = false
  // empTypes = []
  // empTypeId
  // employees = []

  constructor(
    private classService: ClassService,
    private studentService: StudentService,
    private employeeService: EmployeeService,
    private employeeTypeService: EmployeeTypeService,
    private activatedRoute: ActivatedRoute,
    private toastrService: ToastrService
  ) {
    this.class = new Class()
    this.lecture = new Employee()
  }

  async ngOnInit() {
    // await this.getAllEmployeeType()
    this.activatedRoute.params.subscribe(async params => {
      if (params.id) {
        this.classId = params.id;
        await this.getClassById(this.classId)
        await this.getClassLectureByClassId(this.classId)
      }
    });
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    this.resizeTabs()
  }

  async getClassById(classId) {
    this.kendoLoading = true
    await this.classService
      .getClassById(classId)
      .pipe(first())
      .subscribe(
        res => {
          if (res.errors && res.errors.length > 0) {
          } else {
            this.class = res.result;
          }
          this.kendoLoading = false
        },
        err => {
          this.kendoLoading = false

        }
      );
  }
  
  async getClassLectureByClassId(classId) {
    await this.classService.getClassLectureByClasssId(classId)
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {

        } else {
          this.lecture = res.result.employee
        }
      })
  }

  resizeTabs() {
    const tab = $('#p-tabview')
    var offsetbottom =
      parseInt($(window).height()) - parseInt(tab.offset().top);

    const tabElement = tab,
    dataArea = tabElement.find(".ui-tabview-panels"),
    heightHeader = $(".ui-tabview-nav", tab).outerHeight() || 0;

    dataArea.height(
      offsetbottom - heightHeader - 50
    );
  }
 
}
