import { Component, OnInit } from "@angular/core";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { Student } from "src/app/_entity";
import { StudentService } from "src/app/_service";
import { ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { TextConstant } from 'src/app/_constant';
import { first } from 'rxjs/operators';

@Component({
  selector: "app-student-detail",
  templateUrl: "./student-detail.component.html",
  styleUrls: ["./student-detail.component.css"]
})
export class StudentDetailComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  student: Student;
  id
  constructor(
    private studentService: StudentService,
    private route: ActivatedRoute,
    private toastrService: ToastrService
  ) {
    this.student = new Student();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params.id) {
        console.log(params.id)
        this.id = params.id
        this.retrieveStudentById(this.id)
      }
    })
  }

  async onSubmit() {
    console.log(this.id)

    if (this.id) {
      await this.updateStudent(this.student)
    } else {
      await this.createStudent(this.student)
    }
  }

  async retrieveStudentById(studentId) {
    this.blockUI.start(TextConstant.LOADING)
    await this.studentService.getById(studentId)
      .pipe(first())
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {

        } else {
          this.student = res.result
          this.student.birthDay = new Date(this.student.birthDay)
        }
        this.blockUI.stop()
      }, err => {
        this.blockUI.stop()
      })
  }

  async createStudent(studentDto) {
    this.blockUI.start(TextConstant.LOADING)
    await this.studentService.createStudent(studentDto)
      .pipe(first())
      .subscribe(res => {
        if (res.body.errors && res.body.errors.length > 0) {
          this.toastrService.error("Thêm học viên không thành công. Lỗi người dùng!", "Thông báo")
        } else {
          this.toastrService.success("Thêm học viên thành công!", "Thông báo")
        }
        this.blockUI.stop()
      }, err => {
        this.toastrService.error("Thêm học viên không thành công. Lỗi hệ thống!", "Thông báo")
        this.blockUI.stop()
      })
  }

  async updateStudent(studentDto) {
    this.blockUI.start(TextConstant.LOADING)
    await this.studentService.updateStudent(studentDto)
      .pipe(first())
      .subscribe(res => {
        if (res.body.errors && res.body.errors.length > 0) {
          this.toastrService.error("Sửa học viên không thành công. Lỗi người dùng!", "Thông báo")
        } else {
          this.toastrService.success("Sửa học viên thành công!", "Thông báo")
        }
        this.blockUI.stop()
      }, err => {
        this.toastrService.error("Sửa học viên không thành công. Lỗi hệ thống!", "Thông báo")
        this.blockUI.stop()
      })
  }
}
