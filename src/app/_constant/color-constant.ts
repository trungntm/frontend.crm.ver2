export const STATUS = {
  COLORS: {
    black: {
      primary: "#000",
      secondary: "#fff"
    },
    orange: {
      primary: "#FF7300",
      secondary: "#fff"
    },
    blue: {
      primary: "#1e90ff",
      secondary: "#fff"
    },
    red: {
      primary: "#F5222D",
      secondary: "#fff"
    },
    green: {
      primary: "#008000",
      secondary: "#fff"
    }
  }
};
