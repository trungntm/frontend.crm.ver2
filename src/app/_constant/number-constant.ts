export const NumberConstant = {
  PAGE_INDEX_DEFAULT: 0,
  PAGE_SIZE_DEFAULT: 20,
  LONG_MAX: 2147483647
};
