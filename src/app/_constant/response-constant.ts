export const ResponseConstant = {
  RESPONSE_SUCCESS: 'true',
  RESPONSE_FAIL: 'false'
}

export const ResponseMessage = {
  INSERT_SUCCESS: 'Thêm dữ liệu thành công!',
  INSERT_ERROR_SYSTEM: 'Thêm dữ liệu thất bại. Lỗi hệ thống!',
  INSERT_ERROR_USER: 'Thêm dữ liệu thất bại. Vui lòng kiểm tra lại!',

  UPDATE_SUCCESS: 'Cập nhật dữ liệu thành công!',
  UPDATE_ERROR_SYSTEM: 'Cập nhật dữ liệu thất bại. Lỗi hệ thống',
  UPDATE_ERROR_USER: 'Cập nhật dữ liệu thất bại. Vui lòng kiểm tra lại!',

  DELETE_SUCCESS: 'Xóa dữ liệu thành công!',
  DELETE_ERROR_SYSTEM: 'Xóa dữ liệu thất bại. Lỗi hệ thống!',
  DELETE_ERROR_USER: 'Xóa dữ liệu thất bại. Vui lòng kiểm tra lại!'
}

export const HttpStatus = {
  OK: 200,
  NOT_FOUND: 404,
  BAD_REQUEST: 400,
  ACCESS_DENIED: 403,
  UN_AUTHORIZED: 401,
  SERVER_ERROR: 500
}