export const months = {
  monthsInYear: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
}

export const ChartType = {
  LINE: 'line',         // line
  BAR: 'bar',           // hinh tru
  DOUGHNUT: 'doughnut', // circle empty,
  PIE: 'pie'            // circle full
}
