import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { TextConstant, PaginatorConstant } from '../_constant';
import { map } from 'rxjs/operators';
import { GridDataResult } from '@progress/kendo-angular-grid';

@Injectable({
  providedIn: 'root'
})
export class CourseCategoryService {

  context
  constructor(private http: HttpClient) {
    this.context = environment.base_url
   }

  retrieveCourseCategories(keyword, status, page = PaginatorConstant.page, size = PaginatorConstant.size): Observable<any> {
    if (!keyword) {
      keyword = TextConstant.EMPTY
    }
    
    return this.http.get(`${this.context}/api/v1.0/course-categories/list?name=${keyword}&status=${status}&page=${page}&size=${size}`)
  }

  getCourseCategoriesByPage(keyword, status, page, size): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/course-categories/list?keyword=${keyword}&status=${status}&page=${page}&size=${size}`)
  }

  createCourseCategory (courseCatDto): Observable<any> {
    return this.http.post(`${this.context}/api/v1.0/course-categories`, courseCatDto, { observe: `response` })
   }

  retrieveCourseCategoryById(courseCategoryId): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/course-categories/getById/${courseCategoryId}`)
  }

  updateCourseCategory(courseCatDto): Observable<any> {
    return this.http.put(`${this.context}/api/v1.0/course-categories/update`, courseCatDto, { observe: 'body' })
  }

}
