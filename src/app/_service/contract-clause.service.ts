import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {IFilterRequest} from '../_entity';
import {Observable} from 'rxjs';
import {ContractClause} from '../_entity/contract-clause';

@Injectable({
  providedIn: 'root'
})
export class ContractClauseService {
  private context: string;
  constructor(private http: HttpClient) {
    this.context = `${environment.base_url}/api/v1.0/contract-clauses`
  }

  public retrieveById(id: number): Observable<any> {
    return this.http.get(`${this.context}/getById/${id}`);
  }

  public retrieveByParams(params: IFilterRequest): Observable<any> {
    return this.http.get(`${this.context}/list`, { params });
  }

  public create(contractClauseDto: ContractClause): Observable<any> {
    return this.http.post(`${this.context}/create`, contractClauseDto, { observe: 'body' });
  }

  public update(contractClauseDto: ContractClause): Observable<any> {
    return this.http.put(`${this.context}/update`, contractClauseDto, { observe: 'body' });
  }

  public retrieveByContractType(id: number): Observable<any> {
    return this.http.get(`${this.context}/getByContractType/${id}`);
  }
}
