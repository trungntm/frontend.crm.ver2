import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class PrivilegeService {

  context
  constructor(private http: HttpClient) {
    this.context = environment.base_url
   }

  retrieveAllPrivilegesPaging(keyword, page, size): Observable<any> {
    if (!keyword) {
      keyword = ''
    }

    return this.http.get(`${this.context}/api/v1.0/privileges/list?keyword=${keyword}&page=${page}&size=${size}`)
  }
}
