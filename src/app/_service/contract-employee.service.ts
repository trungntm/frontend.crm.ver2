import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {ContractEmployee, IFilterRequest} from '../_entity';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContractEmployeeService {

  private context: string;
  constructor(private http: HttpClient) {
    this.context = `${environment.base_url}/api/v1.0/contract-employees`;
  }

  public retrieveByParams(params: IFilterRequest): Observable<any> {
    return this.http.get(`${this.context}/list`, { params });
  }

  public retrieveById(id: number): Observable<any> {
    return this.http.get(`${this.context}/getById/${id}`);
  }

  public create(contractEmployeeDto: ContractEmployee): Observable<any> {
    return this.http.post(`${this.context}/create`, contractEmployeeDto, { observe: 'body' })
  }

  public update(contractEmployeeDto: ContractEmployee): Observable<any> {
    return this.http.put(`${this.context}/update`, contractEmployeeDto, { observe: 'body' });
  }

  public exportContractEmployee(id: number): Observable<any> {
    return this.http.get(`${this.context}/exportWord/${id}`)
  }
}
