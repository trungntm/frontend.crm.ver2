import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { TextConstant } from '../_constant';
import {IFilterRequest} from '../_entity';

@Injectable({
  providedIn: 'root'
})
export class MarkServiceService {
  context
  constructor(private http: HttpClient) {
    this.context = environment.base_url
   }

  retrieveMarksByClass(classId, keyword, status, page, size): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/marks/getByClass?classId=${classId}&keyword=${keyword}&status=${status}&page=${page}&size=${size}`)
  }

  importExcel(classId, formData): Observable<any> {
    return this.http.post(`${this.context}/api/v1.0/marks/excel/${classId}`, formData, { observe: 'response' })
  }

  updateMarks(markDtos): Observable<any> {
    return this.http.put(`${this.context}/api/v1.0/marks/updateByClass`, markDtos, { observe: 'body' });
  }
}
