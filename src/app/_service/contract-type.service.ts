import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {IFilterRequest} from '../_entity';
import {Observable} from 'rxjs';
import {ContractType} from '../_entity/contract-type';

@Injectable({
  providedIn: 'root'
})
export class ContractTypeService {

  private context: string;
  constructor(private http: HttpClient) {
    this.context = `${environment.base_url}/api/v1.0/contract-types`;
  }

  public retrieveByParams(params: IFilterRequest): Observable<any> {
    return this.http.get(`${this.context}/list`, { params });
  }

  public retrieveById(id): Observable<any> {
    return this.http.get(`${this.context}/getById/${id}`);
  }

  public create(contractTypeDto: ContractType): Observable<any> {
    return this.http.post(`${this.context}/create`, contractTypeDto, { observe: 'body' });
  }

  public update(contractTypeDto: ContractType): Observable<any> {
    return this.http.put(`${this.context}/update`, contractTypeDto, { observe: 'body' });
  }

  public retrieveAll(): Observable<any> {
    return this.http.get(`${this.context}/getAll`);
  }

}
