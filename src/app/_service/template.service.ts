import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { TextConstant } from '../_constant';
import { map } from 'rxjs/operators';
import { GridDataResult } from '@progress/kendo-angular-grid';

@Injectable({
  providedIn: 'root'
})
export class TemplateService {
  context
  constructor(private http: HttpClient) {
    this.context = environment.base_url
   }

  retrieveAllTemplatePaging(keyword, status, page, size): Observable<any> {
    if (!keyword) {
      keyword = TextConstant.EMPTY
    }
    return this.http.get(`${this.context}/api/v1.0/templates/list?keyword=${keyword}&status=${status}&page=${page}&size=${size}`)
  }

  retrieveTemplateByIdAndStatus(id, status): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/templates/${id}/${status}`)
  }

  retrieveActivateTemplateById(id): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/templates/${id}`)
  }

  createTemplate(templateDto): Observable<any> {
    return this.http.post(`${this.context}/api/v1.0/templates`, templateDto, { observe: `response` })
  }

  updateTemplate(id, templateDto): Observable<any> {
    return this.http.put(`${this.context}/api/v1.0/templates/${id}`, templateDto, { observe: `response` })
  }
  
  deleteTemplate(id): Observable<any> {
    return this.http.put(`${this.context}/api/v1.0/templates/delete/${id}`, { observe: `response` })
  }
}
