import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { IFilterRequest } from '../_entity';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CalendarService {

  private context: string;

  constructor(private http: HttpClient) {
    this.context = environment.base_url;
   }

  retrieveOpenClassesToCalendar(params: IFilterRequest): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/classes/getOpenClassesToCalendar`, { params })
  }

  retrieveEndClassesToCalendar(params: IFilterRequest): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/classes/getEndClassesToCalendar`, { params })
  }
}
