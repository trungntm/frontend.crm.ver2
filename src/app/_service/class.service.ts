import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment'
import { HttpClient } from '@angular/common/http'
import { Observable, throwError } from 'rxjs'
import { TextConstant, PaginatorConstant, Status } from '../_constant';
import { map, catchError } from 'rxjs/operators';
import { GridDataResult } from '@progress/kendo-angular-grid';
import {IFilterRequest} from '../_entity';

@Injectable({
  providedIn: 'root'
})
export class ClassService {

  context
  constructor(private http: HttpClient) {
    this.context = environment.base_url
   }
  
  retrieveClassesByParamsPaging(keyword = TextConstant.EMPTY, status = Status.ACTIVE, page = PaginatorConstant.page, size = PaginatorConstant.size): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/classes/list?keyword=${keyword}&status=${status}&page=${page}&size=${size}`)
  }

  getClassesByPage(params: IFilterRequest): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/classes/list`, { params })
    .pipe(
      map(
        response =>
          <GridDataResult>{
            data: response["result"],
            total: parseInt(response["totalItems"], 10)
          }
      )
    )
  }

  getClassById(classId): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/classes/${classId}`)
  }

  createClass(classDto): Observable<any> {
    return this.http.post(`${this.context}/api/v1.0/classes`, classDto, { observe: `response` })
  }

  updateClass(classDto): Observable<any> {
    console.log(classDto)
    return this.http.put(`${this.context}/api/v1.0/classes`, classDto, { observe: `response` })
  }

  deleteClass(id): Observable<any> {
    return this.http.put(`${this.context}/api/v1.0/classes/delete/${id}`, { observe: `response` })
  }

  insertStudentsInToClass(classId, studentDtos): Observable<any> {
    return this.http.post(`${this.context}/api/v1.0/classes/students/${classId}`, studentDtos, { observe: 'response' })
  }

  importExcel(classId, formData): Observable<any> {
    return this.http.post(`${this.context}/api/v1.0/classes/students/excel/${classId}`, formData, { observe: 'response' })
  }

  setLectureForClass(classId, lectureId): Observable<any> {
    return this.http.post(`${this.context}/api/v1.0/class-lectures/${classId}/${lectureId}`, { observe: 'response' })
  }

  getClassLectureByClasssId(classId): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/class-lectures/${classId}`)
  }

  getClassesByCourse(courseId): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/classes/getByCourse/${courseId}`)
  }

  deleteStudentFromClass(classId, studentId): Observable<any> {
    return this.http.delete(`${this.context}/api/v1.0/classes/deleteStudent/${classId}/${studentId}`, { observe: 'response' })
  }
}
