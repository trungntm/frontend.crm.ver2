import { Injectable, ErrorHandler } from '@angular/core';
import { environment } from '../../environments/environment'
import { HttpClient } from '@angular/common/http'
import { Observable, BehaviorSubject, throwError } from 'rxjs'
import { GridDataResult } from '@progress/kendo-angular-grid';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RoleService extends BehaviorSubject<GridDataResult>{

  context
  constructor(private http: HttpClient) {
    super(null)
    this.context = environment.base_url
   }
  
  retrieveAllRolePaging(name, page, size): Observable<any> {
    if (!name) {
      name = '';
    }
    return this.http.get(`${this.context}/api/v1.0/roles/list?name=${name}&page=${page}&size=${size}`)
  }

  retrieveRoleById(id): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/roles/${id}`)
  }

  updatePrivilegeForRole(roleId, privilegeId): Observable<any> {
    return this.http.put(`${this.context}/api/v1.0/roles/${roleId}/${privilegeId}`, {observe: `response`})
  }

  insertRole(newRole): Observable<any> {
    return this.http.post(`${this.context}/api/v1.0/roles`, newRole, { observe: `response` });
  }

  updateRole(updateRole): Observable<any> {
    return this.http.put(this.context + `/api/v1.0/roles`, updateRole, { observe: `response` });
  }

  deleteRole(roleId): Observable<any> {
    return this.http.put(`${this.context}/api/v1.0/roles/${roleId}`, { observe: `response` })
  }

  getRolesByPage(keyword, page, size): Observable<any> {
    return this.http
      .get(
        `${
          this.context
        }/api/v1.0/roles/list?name=${keyword}&page=${page}&size=${size}`
      )
      .pipe(
        map(
          response =>
            <GridDataResult>{
              data: response["result"],
              total: parseInt(response["totalItems"], 10)
            }
        )
      )
      .pipe(
        catchError(err => throwError(err))
      );
  }
}
