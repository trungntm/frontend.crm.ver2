import { Injectable } from '@angular/core'
import { environment } from '../../environments/environment'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { TextConstant } from '../_constant';
import { map } from 'rxjs/operators';
import { GridDataResult } from '@progress/kendo-angular-grid';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  context;
  constructor(private http: HttpClient) {
    this.context = environment.base_url;
  }

  retrieveAllUsersPaging(keyword, status, page, size): Observable<any> {
    if (!keyword) {
      keyword = TextConstant.EMPTY
    }
    return this.http.get(`${this.context}/api/v1.0/users/list?keyword=${keyword}&status=${status}&page=${page}&size=${size}`)
  }

  retrieveUserById(id): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/users/${id}`)
  }

  onDeleteUser(id): Observable<any> {
    return this.http.put(`${this.context}/api/v1.0/users/${id}`, { observe: `response` })
  }
  
  createUser(userDto): Observable<any> {
    return this.http.post(`${this.context}/api/v1.0/users`, userDto, { observe: `response` })
  }

  updateUser(userDto): Observable<any> {
    return this.http.put(`${this.context}/api/v1.0/users`, userDto, { observe: 'response' })
  }

  updateRoleForUser(uid, rid): Observable<any> {
    return this.http.put(`${this.context}/api/v1.0/users/${uid}/${rid}`, {observe: `response`})
  }

  reactiveUser(id): Observable<any> {
    return this.http.put(`${this.context}/api/v1.0/users/reactive/${id}`, { observe: `response` })
  }

  getUsersByPage(keyword, status, page, size): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/users/list?keyword=${keyword}&status=${status}&page=${page}&size=${size}`)
      .pipe(
        map(
          response =>
            <GridDataResult>{
              data: response["result"],
              total: parseInt(response["totalItems"], 10)
            }
        )
      )
  }

  retrieveUsersByRole(id: number, keyword: string, page: number, size: number): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/roles/users?id=${id}&keyword=${keyword}&page=${page}&size=${size}`);
  }

}
