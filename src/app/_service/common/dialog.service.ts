import { Injectable } from '@angular/core';
import { MatConfirmDialogComponent } from 'src/app/components-common/mat-confirm-dialog/mat-confirm-dialog.component';
import { MatDialog } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class MatDialogService {

  constructor(private dialog: MatDialog) { }

  openConfirmDialog(data:any) {
    return this.dialog.open(MatConfirmDialogComponent, {
      width: '456px',
      height: '150px',
      panelClass: 'custom-modalbox',
      data:{
        title: data.title,
        message: data.message,
        disableClose:true
      }
    })
  }
}
