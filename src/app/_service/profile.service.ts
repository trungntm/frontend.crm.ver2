import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {IFilterRequest, User} from '../_entity';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  private context: string;

  constructor(private http: HttpClient) {
    this.context = environment.base_url;
  }

  public retrieveCurrent(): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/users/current`)
  }

  public updatePassword(params: IFilterRequest, password: string): Observable<any> {
    return this.http.put(`${this.context}/api/v1.0/users/updatePassword`, password, {params: params, observe: 'body'});
  }

  public updateProfile(userDto: User): Observable<any> {
    return this.http.put(`${this.context}/api/v1.0/users/updateProfile`, userDto, { observe: 'body' });
  }
}
