import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class StatisticService {

  context
  constructor(private http: HttpClient) {
    this.context = environment.base_url
   }
  
   getStatisticUsersCreated(year): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/statistics/users/created?year=${year}`)
  }

  getStatisticUsersDeleted(year): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/statistics/users/deleted?year=${year}`)
  }

  countStudentsByCourse(courseId): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/statistics/students/CountByCourseCategory?courseCategoryId=${courseId}`)
  }

  countStudentsByTenYear(status): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/statistics/students/countStudentsByYearAndStatus?status=${status}`)
  }

}
