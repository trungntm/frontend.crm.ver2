import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { TextConstant } from '../_constant';
import { map } from 'rxjs/operators';
import { GridDataResult } from '@progress/kendo-angular-grid';

@Injectable({
  providedIn: 'root'
})
export class CourseService {

  context
  constructor(private http: HttpClient) {
    this.context = environment.base_url
   }

  createCourse(courseDto): Observable<any> {
    return this.http.post(`${this.context}/api/v1.0/courses`, courseDto, { observe: `response` })
  }

  updateCourse(id, courseDto): Observable<any> {
    return this.http.put(`${this.context}/api/v1.0/courses/${id}`, courseDto, { observe: `response` })
  }

  deleteCourse(courseId): Observable<any> {
    return this.http.put(`${this.context}/api/v1.0/courses/delete/${courseId}`, { observe: 'response' })
  }

  retrieveCourseById(id): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/courses/${id}`)
  }

  retrieveCourses(keyword, status, page, size): Observable<any> {
    if (!keyword) {
      keyword = TextConstant.EMPTY
    }

    return this.http.get(`${this.context}/api/v1.0/courses/list?keyword=${keyword}&status=${status}&page=${page}&size=${size}`)
  }

  retrieveCoursesByIsLongtermCourse(keyword, status, longtermCourse, page, size): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/courses/list?keyword=${keyword}&status=${status}&longTermCourse=${longtermCourse}&page=${page}&size=${size}`)
    
  }

  getCoursesByPage(keyword, status, page, size): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/courses/list?keyword=${keyword}&status=${status}&page=${page}&size=${size}`)
      .pipe(
        map(
          response =>
            <GridDataResult>{
              data: response["result"],
              total: parseInt(response["totalItems"], 10)
            }
        )
      )
  }

  retrieveCoursesByCourseCategory(courseCategoryId): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/courses/types/${courseCategoryId}`)
  }

  changeImage(id, formData): Observable<any> {
    return this.http.put(`${this.context}/api/v1.0/courses/${id}/images`, formData, { observe: `response` })
  }

  countCourses(): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/courses/count`)
  }
}
