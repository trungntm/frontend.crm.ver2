import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { TextConstant } from '../_constant';
import { map } from 'rxjs/operators';
import { GridDataResult } from '@progress/kendo-angular-grid';
import {BaseResponse, Employee, IFilterRequest} from '../_entity';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  context
  constructor(private http: HttpClient) {
    this.context = environment.base_url
  }

  retrieveAllEmployeesPaging(keyword, status, page, size): Observable<any> {
    if (!keyword) {
      keyword = TextConstant.EMPTY
    }
    
    return this.http.get(`${this.context}/api/v1.0/employees/list?keyword=${keyword}&status=${status}&page=${page}&size=${size}`)
  }

  retrieveAll(): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/employees/getAll`);
  }

  getEmployeesByPage(params: IFilterRequest): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/employees/list`, { params });
  }

  retrieveEmployeeById(id): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/employees/${id}`)
  }

  createEmployee(employeeDto): Observable<any> {
    return this.http.post(`${this.context}/api/v1.0/employees`, employeeDto, { observe: `response` })
  }

  updateEmployee(id, employeeDto): Observable<any> {
    return this.http.put(`${this.context}/api/v1.0/employees/${id}`, employeeDto, { observe: `response` })
  }

  changeImage(id, formData): Observable<any> {
    return this.http.put(`${this.context}/api/v1.0/employees/${id}/images`, formData, { observe: `response` })
  }

  deleteEmployee(id): Observable<any> {
    return this.http.delete(`${this.context}/api/v1.0/employees/delete/${id}`, { observe: 'body' })
  }

  retrieveEmployeeByTypeAndStatus(empTypeId, status): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/employees/type/${empTypeId}/${status}`)
  }
  
  countEmployeesByType(id): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/employees/count/type/${id}`)
  }
 }
