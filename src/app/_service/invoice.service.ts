import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { TextConstant } from '../_constant';

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {

  context
  constructor(private http: HttpClient) {
    this.context = environment.base_url
   }

   retrieveInvoiceByStudentAndClass(studentId, classId): Observable<any> {
     return this.http.get(`${this.context}/api/v1.0/invoices/getInvoices/${studentId}/${classId}`)
   }

  createInvoice(invoiceDto): Observable<any> {
    return this.http.post(`${this.context}/api/v1.0/invoices/createInvoice`, invoiceDto, { observe: 'response' })
  }

  printInvoice(): Observable<any> {
    return this.http.post(`${this.context}/api/v1.0/invoices/printInvoice`, { observe: 'response' })
  }
}
