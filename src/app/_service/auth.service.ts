import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { User } from '../_entity/user';
import { Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { TokenConstant } from '../_constant';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  context = environment.base_url
  jwtHelper: JwtHelperService
  private headers = new HttpHeaders({ 'Content-Type': 'application/json' })
  constructor(private http: HttpClient) {
    this.jwtHelper = new JwtHelperService
   }

  login(username, password): Observable<any> {
    return this.http.post(`${this.context}/login?username=${username}&password=${password}`, { headers: this.headers }, {observe:`response`})
  }

  IsAuthenticated() {
    const token = localStorage.getItem(TokenConstant.X_ACCESS_TOKEN);
    if (!token) {
      return false;
    }
    return !this.jwtHelper.isTokenExpired(token);
  }

  Logout() {
    localStorage.clear();
  }

  retrieveLoggedUser(): Observable<any> | any {
    return this.http.get(`${this.context}/api/v1.0/users/current`)
  }
}
