import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment'
import { HttpClient } from '@angular/common/http'
import { Observable, BehaviorSubject, throwError } from 'rxjs'
import { GridDataResult } from '@progress/kendo-angular-grid';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  context
  constructor(private http: HttpClient) {
    this.context = environment.base_url
   }

   getById(studentId): Observable<any> {
     return this.http.get(`${this.context}/api/v1.0/students/${studentId}`)
   }

   createStudent(studentDto): Observable<any> {
     return this.http.post(`${this.context}/api/v1.0/students`, studentDto, { observe: `response` })
   }

   updateStudent(studentDto): Observable<any> {
     return this.http.put(`${this.context}/api/v1.0/students`, studentDto, { observe: 'response' })
   }

   deleteStudent(studentId): Observable<any> {
     return this.http.put(`${this.context}/api/v1.0/students/delete/${studentId}`, { observe: 'response' })
   }

   getCoursesOfStudent(studentId, keyword, status): Observable<any> {
     return this.http.get(`${this.context}/api/v1.0/students/getCourses?studentId=${studentId}&keyword=${keyword}&status=${status}`)
   }

   getClassesOfStudent(studentId, keyword, status): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/students/getClasses?studentId=${studentId}&keyword=${keyword}&status=${status}`)
    }

   getStudentsByPage(keyword, status, page, size): Observable<any> {
    return this.http
      .get(
        `${
          this.context
        }/api/v1.0/students/list?keyword=${keyword}&status=${status}&page=${page}&size=${size}`
      )
      .pipe(
        map(
          response =>
            <GridDataResult>{
              data: response["result"],
              total: parseInt(response["totalItems"], 10)
            }
        )
      );
  }

}
