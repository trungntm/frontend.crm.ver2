import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  private context;
  constructor(private http: HttpClient) {
    this.context = environment.base_url;
   }

  public countStudents() {
    return this.http.get(`${this.context}/api/v1.0/students/countStudents`);
  }

  public countLectures() {
    return this.http.get(`${this.context}/api/v1.0/employees/count/type/2`);
  }

  public countCourses() {
    return this.http.get(`${this.context}/api/v1.0/courses/count`);
  }

  public countStudentsByTenYear(status): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/statistics/students/countStudentsByYearAndStatus?status=${status}`)
  }

  public countStudentsByCourseCategory(): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/statistics/students/countByCourseCategory`);
  }
}
