import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { GridDataResult } from '@progress/kendo-angular-grid';

@Injectable({
  providedIn: 'root'
})
export class EmployeeTypeService {

  context
  constructor(private http: HttpClient) {
    this.context = environment.base_url
   }

  getAllEmployeeTypes(): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/employee-types`);
  }

  retrieveEmployeeTypesPaging(keyword, status, page, size): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/employee-types/list?keyword=${keyword}&status=${status}&page=${page}&size=${size}`)
  }

  getEmployeeTypesByPage(keyword, status, page, size): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/employee-types/list?keyword=${keyword}&status=${status}&page=${page}&size=${size}`)
      .pipe(
        map(
          response =>
            <GridDataResult>{
              data: response["result"],
              total: parseInt(response["totalItems"], 10)
            }
        )
      )
  }

  deleteEmployeeType(empTypeId): Observable<any> {
    return this.http.put(`${this.context}/api/v1.0/employee-types/delete/${empTypeId}`, { observe: `response` })
  }

  createEmployeeType(employeeTypeDto): Observable<any> {
    return this.http.post(`${this.context}/api/v1.0/employee-types`, employeeTypeDto, { observe: `response` })
  }

  updateEmployeeType(id, employeeTypeDto): Observable<any> {
    return this.http.put(`${this.context}/api/v1.0/employee-types/${id}`, employeeTypeDto, { observe: `response` })
  }

  retrieveEmployeeTypeById(id): Observable<any> {
    return this.http.get(`${this.context}/api/v1.0/employee-types/${id}`)
  }
}
