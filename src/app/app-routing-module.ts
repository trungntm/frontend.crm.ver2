import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import {
  DashboardComponent,
  UsersListComponent,
  CoursesListComponent,
  RolesListComponent,
  EmployeeTypesComponent,
  EmployeesListComponent,
  EmployeeDetailComponent,
  UsersTrashComponent,
  RoleRightComponent,
  UserRightComponent,
  CourseCategoriesListComponent,
  TemplateListComponent,
  TemplateDetailComponent,
  ClassListComponent,
  StudentsComponent,
  StudentDetailComponent,
  ClassStudentComponent,
  CoursesOfStudentComponent,
  ClassesOfStudentComponent,
  EmployeeContractComponent,
  ProfileComponent,
  ContractClauseComponent, ContractTypesComponent
} from './components-admin';
import { LoginComponent } from './login/login.component'
import {
  AccessDeniedComponent,
  ResourceNotFoundComponent
} from './components-error'
import {
  HomeComponent,
  EducationProgramsComponent, 
  InfrastructureComponent,
  AchievementsAcquiredComponent,
  LecturesTeamComponent,
  AdvisoryStaffComponent,
  LecturesTeamDetailComponent,
  CourseCategoryDetailClientComponent,
  CourseDetailClientComponent,
  ClassDetailClientComponent,
  StudentsRegisterCourseComponent
} from './components-client'
import {EmployeeContractDetailComponent} from './components-admin/employee-contract/employee-contract-detail/employee-contract-detail.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'quan-ly/cau-hinh-he-thong/permission', component: RoleRightComponent },
  { path: 'quan-ly/nguoi-dung', component: UsersListComponent },
  { path: 'quan-ly/nguoi-dung/trash', component: UsersTrashComponent },
  { path: 'quan-ly/quyen-nguoi-dung', component: RolesListComponent },
  { path: 'quan-ly/quyen-nguoi-dung/phan-quyen/:id', component: UserRightComponent },
  { path: 'quan-ly/khoa-hoc', component: CoursesListComponent },
  { path: 'quan-ly/lop-hoc', component: ClassListComponent },
  { path: 'quan-ly/lop-hoc/chi-tiet/:id', component: ClassStudentComponent },
  { path: 'quan-ly/loai-nhan-vien', component: EmployeeTypesComponent },
  { path: 'quan-ly/nhan-vien', component: EmployeesListComponent },
  { path: 'quan-ly/nhan-vien/:id', component: EmployeeDetailComponent },
  { path: 'quan-ly/loai-khoa-hoc', component: CourseCategoriesListComponent },
  { path: 'quan-ly/template', component: TemplateListComponent },
  { path: 'quan-ly/hoc-vien', component: StudentsComponent },
  { path: 'quan-ly/hoc-vien/them-moi', component: StudentDetailComponent },
  { path: 'quan-ly/hoc-vien/:id/khoa-hoc', component: CoursesOfStudentComponent },
  { path: 'quan-ly/hoc-vien/:id/lop-hoc', component: ClassesOfStudentComponent },
  { path: 'quan-ly/hoc-vien/chi-tiet/:id', component: StudentDetailComponent },
  { path: 'quan-ly/templates', component: TemplateListComponent },
  { path: 'quan-ly/templates/tao-moi', component: TemplateDetailComponent },
  { path: 'quan-ly/templates/:id', component: TemplateDetailComponent },
  { path: 'quan-ly/hop-dong/dieu-khoan', component: ContractClauseComponent },
  { path: 'quan-ly/hop-dong/loai-hop-dong', component: ContractTypesComponent },
  { path: 'quan-ly/hop-dong/hop-dong-nhan-vien', component: EmployeeContractComponent },
  { path: 'quan-ly/hop-dong/hop-dong-nhan-vien/tao-moi', component: EmployeeContractDetailComponent },
  { path: 'quan-ly/hop-dong/hop-dong-nhan-vien/:id', component: EmployeeContractDetailComponent },
  { path: 'login', component: LoginComponent },
  { path: 'trang-chu', component: HomeComponent },
  { path: 'gioi-thieu/chuong-trinh-dao-tao', component: EducationProgramsComponent },
  { path: 'gioi-thieu/co-so-ha-tang', component: InfrastructureComponent },
  { path: 'gioi-thieu/thanh-tuu-dat-duoc', component: AchievementsAcquiredComponent },
  { path: 'gioi-thieu/doi-ngu-giang-vien', component: LecturesTeamComponent },
  { path: 'gioi-thieu/doi-ngu-tu-van', component: AdvisoryStaffComponent },
  { path: 'gioi-thieu/doi-ngu-giang-vien/:id', component: LecturesTeamDetailComponent },
  { path: 'loai-khoa-hoc/:id', component: CourseCategoryDetailClientComponent },
  { path: 'khoa-hoc/:id', component: CourseDetailClientComponent },
  { path: 'khoa-hoc/lop-hoc/:id', component: ClassDetailClientComponent },
  { path: 'lich-khai-giang/:id', component: StudentsRegisterCourseComponent },
  { path: '403', component: AccessDeniedComponent },
  { path: '**', redirectTo: '/404' },
  { path: '404', component: ResourceNotFoundComponent }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
