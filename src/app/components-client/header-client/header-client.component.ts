import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../../app.component';
import { User, CourseCategory } from 'src/app/_entity';
import { AuthService, CourseCategoryService } from 'src/app/_service';
import { first } from 'rxjs/operators';
import { TextConstant, Status } from 'src/app/_constant';

@Component({
  selector: 'app-header-client',
  templateUrl: './header-client.component.html',
  styleUrls: [
    '../../../assets/frontend/layout/css/style.css',
    '../../../assets/frontend/layout/css/style-responsive.css',
    '../../../assets/frontend/layout/css/themes/red.css',
    './header-client.component.css']
})
export class HeaderClientComponent implements OnInit {

  principal: User
  courseCategories: CourseCategory[]

  constructor(private authService: AuthService,
    private courseCategoryService: CourseCategoryService) { 
    this.principal = new User()

  }

  async ngOnInit() {
    await this.retrieveLogged()
    await this.getAllCourseCategories()
  }

  async retrieveLogged() {
    await this.authService.retrieveLoggedUser()
      .pipe(first())
      .subscribe(res => {
        if(res.errors && res.errors.length > 0) {
        } else {
          this.principal = res.result
        }
      }, err => {

      })
  }

  async getAllCourseCategories(keyword = TextConstant.EMPTY, status = Status.ACTIVE, page = 0, size = 1000000) {
    return this.courseCategoryService.retrieveCourseCategories(keyword, status, page, size)
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {

        } else {
          this.courseCategories = res.result
        }
      })
  }

}
