import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-achievements-acquired',
  templateUrl: './achievements-acquired.component.html',
  styleUrls: [
    '../../../assets/frontend/layout/css/style.css',
    '../../../assets/frontend/layout/css/style-responsive.css',
    '../../../assets/frontend/layout/css/themes/red.css',
    '../../../assets/frontend/layout/css/custom.css',
    './achievements-acquired.component.css']
})
export class AchievementsAcquiredComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    this.initAccordion()
  }

  initAccordion() {
    const acc = $('.accordion')

    for (let i = 0; i < acc.length; i++) {
      acc[i].addEventListener('click', function() {
        this.classList.toggle('active-acc')
        let panel = this.nextElementSibling
        if (panel.style.maxHeight){
          panel.style.maxHeight = null
        } else {
          panel.style.maxHeight = `${panel.scrollHeight}px`
        } 
      })
    }

    const firstAcc = acc[0]
    const firstElementSibling = firstAcc.nextElementSibling

    $(firstAcc).addClass('active-acc')
    $(firstElementSibling).css('maxHeight', `${firstElementSibling.scrollHeight}px`)
  }
}
