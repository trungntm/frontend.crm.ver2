import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery'
import { EmployeeService, CourseService } from '../../_service';
import { first } from 'rxjs/operators';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: [
    '../../../assets/frontend/layout/css/style.css',
    '../../../assets/frontend/layout/css/style-responsive.css',
    '../../../assets/frontend/layout/css/themes/red.css',
    '../../../assets/frontend/layout/css/custom.css',
    './home.component.css']
})
export class HomeComponent implements OnInit {

  totalLectures: number
  totalCourses: number
  constructor(private employeeService: EmployeeService, private courseService: CourseService) {
    this.totalLectures = 0
    this.totalCourses = 0
   }

  async ngOnInit() {
    this.initAccordion()
    await this.countLectures()
    await this.countCourses()
  }

  initAccordion() {
    const acc = $('.accordion')

    for (let i = 0; i < acc.length; i++) {
      acc[i].addEventListener('click', function() {
        this.classList.toggle('active-acc')
        let panel = this.nextElementSibling
        if (panel.style.maxHeight){
          panel.style.maxHeight = null
        } else {
          panel.style.maxHeight = `${panel.scrollHeight}px`
        } 
      })
    }

    const firstAcc = acc[0]
    const firstElementSibling = firstAcc.nextElementSibling

    $(firstAcc).addClass('active-acc')
    $(firstElementSibling).css('maxHeight', `${firstElementSibling.scrollHeight}px`)
  }

  async countLectures() {
    return await this.employeeService.countEmployeesByType(2)
      .pipe(first())
      .subscribe(res => {
        this.totalLectures = res
      })
  }

  async countCourses() {
    return await this.courseService.countCourses()
      .pipe(first())
      .subscribe(res => this.totalCourses = res)
  }
}
