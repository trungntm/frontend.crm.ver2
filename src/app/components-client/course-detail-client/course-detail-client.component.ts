import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CourseService, ClassService } from '../../_service';
import { Course, Class } from '../../_entity';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { TextConstant } from '../../_constant';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-course-detail-client',
  templateUrl: './course-detail-client.component.html',
  styleUrls: [
    '../../../assets/frontend/layout/css/style.css',
    '../../../assets/frontend/layout/css/style-responsive.css',
    '../../../assets/frontend/layout/css/custom.css',
    './course-detail-client.component.css']
})
export class CourseDetailClientComponent implements OnInit {

  @BlockUI() blockUI: NgBlockUI
  courseId
  course: Course
  classes: Class[]
  constructor(private activatedRoute: ActivatedRoute, 
    private courseService: CourseService,
    private classService: ClassService) {
      this.course = new Course()
     }

  async ngOnInit() {
    this.activatedRoute.params.subscribe(async params => {
      if (params.id) {
        this.courseId = params.id
        await this.retrieveCourseById(this.courseId)
      }
    })
  }

  async retrieveCourseById(courseId) {
    this.blockUI.start(TextConstant.LOADING)
    await this.courseService.retrieveCourseById(courseId)
      .pipe(first())
      .subscribe(async res => {
        if (res.errors && res.errors.length > 0) {
          this.course = new Course()
        } else {
          this.course = res.result
          await this.getClassesByCourse(this.courseId)
        }
        this.blockUI.stop()
      }, err => {
        this.blockUI.start()
      })
  }

  async getClassesByCourse(courseId) {
    await this.classService.getClassesByCourse(courseId)
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {
          this.classes = []
        } else {
          this.classes = res.result
        }
      })
  }
}
