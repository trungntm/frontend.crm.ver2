import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Class, Employee } from 'src/app/_entity';
import { ClassService } from 'src/app/_service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-class-detail-client',
  templateUrl: './class-detail-client.component.html',
  styleUrls: [
    '../../../assets/frontend/layout/css/style.css',
    '../../../assets/frontend/layout/css/style-responsive.css',
    '../../../assets/frontend/layout/css/custom.css',
    './class-detail-client.component.css']
})
export class ClassDetailClientComponent implements OnInit {

  classId
  class: Class
  lecture: Employee
  constructor(private activatedRoute: ActivatedRoute, 
    private classService: ClassService
    ) {
    this.class = new Class()
    this.lecture = new Employee()
   }

  ngOnInit() {
    this.activatedRoute.params.subscribe(async params => {
      if (params.id) {
        this.classId = params.id
        await this.retrieveClassById(this.classId)
      }
    })
  }

  async retrieveClassById(classId) {
    await this.classService.getClassById(classId)
      .pipe(first())
      .subscribe(async res => {
        if (res.errors && res.errors.length > 0) {

        } else {
          this.class = res.result
          await this.retrieveLectureInClass(this.classId)
        }
      })
  }

  async retrieveLectureInClass(classId) {
    await this.classService.getClassLectureByClasssId(classId)
      .subscribe(res => {
        console.log(res)
        if (res.errros && res.errors.length > 0) {

        } else {
          this.lecture = res.result.employee
        }
      })
  }

}
