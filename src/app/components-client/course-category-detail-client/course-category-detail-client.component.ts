import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CourseService } from 'src/app/_service';
import { Course } from 'src/app/_entity';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { TextConstant } from 'src/app/_constant';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-course-category-detail-client',
  templateUrl: './course-category-detail-client.component.html',
  styleUrls: [
    '../../../assets/frontend/layout/css/style.css',
    '../../../assets/frontend/layout/css/style-responsive.css',
    '../../../assets/frontend/layout/css/custom.css',
    './course-category-detail-client.component.css']
})
export class CourseCategoryDetailClientComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI
  courseCategoryId
  courses: Course[]
  constructor(private activatedRoute: ActivatedRoute,
    private courseService: CourseService,
    private toastrService: ToastrService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      if (params.id) {
        this.courseCategoryId = params.id
        this.getCourseByCourseCategory(this.courseCategoryId)
      }
    })
  }

  async getCourseByCourseCategory(courseCategoryId) {
    this.blockUI.start(TextConstant.LOADING)
    await this.courseService.retrieveCoursesByCourseCategory(courseCategoryId)
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {
          this.courses = []
        } else {
          this.courses = res.result
        }
        this.blockUI.stop()
      }, err => {
        this.blockUI.stop()
      })
  }

}
