import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../../_service';
import { Status } from '../../_constant';
import { Employee } from '../../_entity';

@Component({
  selector: 'app-lectures-team',
  templateUrl: './lectures-team.component.html',
  styleUrls: [
    '../../../assets/frontend/layout/css/style.css',
    '../../../assets/frontend/layout/css/style-responsive.css',
    '../../../assets/frontend/layout/css/themes/red.css',
    './lectures-team.component.css'
  ]
})
export class LecturesTeamComponent implements OnInit {

  lectures: Employee[]

  constructor(private employeeService: EmployeeService) { }

  async ngOnInit() {
    await this.getActiveLectures(2)
  }

  async getActiveLectures(empTypeId, status = Status.ACTIVE) {
    return await this.employeeService.retrieveEmployeeByTypeAndStatus(empTypeId, status)
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {
        } else {
          this.lectures = res.result
        }
      })
  }

}
