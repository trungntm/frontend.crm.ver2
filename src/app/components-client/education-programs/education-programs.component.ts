import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-education-programs',
  templateUrl: './education-programs.component.html',
  styleUrls: [
    '../../../assets/frontend/layout/css/style.css',
    '../../../assets/frontend/layout/css/style-responsive.css',
    '../../../assets/frontend/layout/css/themes/red.css',
    '../../../assets/frontend/layout/css/custom.css',
    './education-programs.component.css']
})
export class EducationProgramsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
