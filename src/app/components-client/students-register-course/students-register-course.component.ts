import { Component, OnInit } from '@angular/core';
import { CourseService, ClassService } from 'src/app/_service';
import { Course, Class } from 'src/app/_entity';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-students-register-course',
  templateUrl: './students-register-course.component.html',
  styleUrls: ['./students-register-course.component.css']
})
export class StudentsRegisterCourseComponent implements OnInit {

  courses = [];
  courseId: number = 0;
  classes: Class[] = [];
  errors: any[] = [];
  constructor(private activatedRoute: ActivatedRoute, private courseService: CourseService, private classService: ClassService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      if (params.id) {
        this.retrieveCoursesByCourseCategory(params.id)
      }
    })
  }

  retrieveCoursesByCourseCategory(id) {
    this.courseService.retrieveCoursesByCourseCategory(id)
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {

        } else {
          res.result.map(item => {
            this.courses.push({
              label: item.name,
              value: item.id
            })
          })
        }
      })
  }

  retrieveClassesByCourse(courseId) {
    this.classService.getClassesByCourse(courseId)
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {
          this.errors = [...res.errors]
        } else {
          this.classes = res.result
        }
      })
  }

  selectCourse() {
    this.retrieveClassesByCourse(this.courseId)
  }
}
