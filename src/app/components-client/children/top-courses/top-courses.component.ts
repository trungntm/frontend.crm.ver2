import { Component, OnInit } from '@angular/core';
import { CourseService } from 'src/app/_service';
import { Course } from 'src/app/_entity';
import { TextConstant, Status } from 'src/app/_constant';

@Component({
  selector: 'app-top-courses',
  templateUrl: './top-courses.component.html',
  styleUrls: ['./top-courses.component.css']
})
export class TopCoursesComponent implements OnInit {

  topCourses: Course[]
  constructor(private courseService: CourseService) { }

  ngOnInit() {
    this.getTopCourses()
  }

  async getCourses(index) {
    if (index === 1) {
      await this.getTopCourses()
    }
    if (index === 2) {
      await this.getlongTermCourses(TextConstant.EMPTY, Status.ACTIVE, false, 0, 15)
    }
    if (index === 3) {
      await this.getlongTermCourses()
    }
  }

  async getTopCourses(keyword = TextConstant.EMPTY, status = Status.ACTIVE, page = 0, size = 15) {
    await this.courseService.retrieveCourses(keyword, status, page, size)
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {
          this.topCourses = []
        } else {
          this.topCourses = res.result
        }
      })
  }

  async getlongTermCourses(keyword = TextConstant.EMPTY, status = Status.ACTIVE, longTermCourse = true, page = 0, size = 15) {
    await this.courseService.retrieveCoursesByIsLongtermCourse(keyword, status, longTermCourse, page, size)
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {
          this.topCourses = []
        } else {
          this.topCourses = res.result
        }
      })
  }
}
