import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EmployeeService } from '../../_service';
import { Employee } from '../../_entity';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-lectures-team-detail',
  templateUrl: './lectures-team-detail.component.html',
  styleUrls: [
    '../../../assets/frontend/layout/css/style.css',
    '../../../assets/frontend/layout/css/style-responsive.css',
    '../../../assets/frontend/layout/css/themes/red.css',
    './lectures-team-detail.component.css']
})
export class LecturesTeamDetailComponent implements OnInit {
  id
  lecture: Employee
  constructor(private route: ActivatedRoute, private employeeService: EmployeeService) {
    this.lecture = new Employee()
   }

  async ngOnInit() {
    this.route.params
      .subscribe( params => this.id = params.id )
    
    if (this.id) {
      await this.retrieveLectureById(this.id)
    }
  }

  async retrieveLectureById(empId) {
    return await this.employeeService.retrieveEmployeeById(empId)
      .pipe(first())
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {

        } else {
          this.lecture = res.result
        }
      })
  }

}
