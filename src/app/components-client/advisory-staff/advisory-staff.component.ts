import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../../_service';
import { Employee } from '../../_entity';
import { Status } from '../../_constant';

@Component({
  selector: 'app-advisory-staff',
  templateUrl: './advisory-staff.component.html',
  styleUrls: [
    '../../../assets/frontend/layout/css/style.css',
    '../../../assets/frontend/layout/css/style-responsive.css',
    '../../../assets/frontend/layout/css/themes/red.css',
    '../../../assets/frontend/layout/css/custom.css',
    './advisory-staff.component.css']
})
export class AdvisoryStaffComponent implements OnInit {

  staffs: Employee[]
  constructor(private employeeService: EmployeeService) { }

  async ngOnInit() {
    await this.getActivateAdvisoryStaffs(1)
  }

  async getActivateAdvisoryStaffs(empTypeId, status = Status.ACTIVE) {
    return await this.employeeService.retrieveEmployeeByTypeAndStatus(empTypeId, status)
      .subscribe(res => {
        if (res.errors && res.errors.length > 0) {
        } else {
          this.staffs = res.result
        }
      })
  }
}
