import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer-client',
  templateUrl: './footer-client.component.html',
  styleUrls: [
    '../../../assets/global/plugins/font-awesome/css/font-awesome.min.css',
    '../../../assets/global/plugins/bootstrap/css/bootstrap.min.css',
    '../../../assets/global/plugins/uniform/css/uniform.default.css',
    '../../../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
    '../../../assets/global/css/components.css',
    '../../../assets/global/css/plugins.css',
    '../../../assets/frontend/layout/css/style.css',
    '../../../assets/frontend/pages/css/style-shop.css',
    '../../../assets/frontend/pages/css/style-layer-slider.css',
    '../../../assets/frontend/layout/css/style-responsive.css',
    '../../../assets/frontend/layout/css/themes/red.css',
    '../../../assets/frontend/layout/css/custom.css',
    './footer-client.component.css']
})
export class FooterClientComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
