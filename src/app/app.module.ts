import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { AppComponent } from './app.component'
import { AppRoutingModule } from './app-routing-module'
import { LoginComponent } from './login/login.component'
import { AuthService } from './_service'
import { CommonModule } from '@angular/common'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import * as angularJwt from '@auth0/angular-jwt'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { ErrorInterceptor, HttpAuthInterceptor  } from './_interceptor'
import { AdminGuard } from './_guard'
import { MaterialModule } from './material-module'
import { MatDialogService } from './_service/common'
import { BlockUIModule } from 'ng-block-ui'
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { PrimeNGModule } from './prime-ng-module';
import { ToastrModule } from 'ngx-toastr';
import { DialogService } from 'primeng/api';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { CountUpModule } from 'countup.js-angular2';

import { 
  DashboardComponent,
  HeaderComponent,
  FooterComponent,
  MenuComponent,
  StudentsComponent,
  UsersListComponent,
  UserDetailComponent,
  UsersTrashComponent,
  RolesListComponent,
  RoleDetailComponent,
  CoursesListComponent,
  CourseDetailComponent,
  EmployeeTypesComponent,
  EmployeeTypeDetailComponent,
  EmployeesListComponent,
  EmployeeDetailComponent,
  UserRightComponent,
  RoleRightComponent,
  CourseCategoryDetailComponent,
  CourseCategoriesListComponent,
  TemplateDetailComponent,
  TemplateListComponent,
  ClassListComponent,
  ClassDetailComponent,
  ClassStudentComponent,
  StudentDetailComponent,
  ClassStudentDetailComponent,
  RoleUserConfigComponent,
  MarkClassStudentComponent,
  StudentsListInClassComponent,
  CoursesOfStudentComponent,
  CoursePricesComponent,
  PaymentTuitionFeesComponent,
  ClassesOfStudentComponent,
  InvoicesStudentClassComponent,
  ProfileComponent,
  EmployeeContractComponent,
  ContractClauseComponent,
  ContractTypesComponent
} from './components-admin'

import { 
  HeaderClientComponent,
  FooterClientComponent,
  HomeComponent,
  AboutComponent,
  EducationProgramsComponent,
  InfrastructureComponent,
  AchievementsAcquiredComponent,
  LecturesTeamComponent,
  AdvisoryStaffComponent,
  LecturesTeamDetailComponent,
  CourseCategoryDetailClientComponent,
  CourseDetailClientComponent,
  ClassDetailClientComponent,
  StudentsRegisterCourseComponent,
} from './components-client'
import { 
  ResourceNotFoundComponent,
  AccessDeniedComponent,
} from './components-error';
import {
  MatConfirmDialogComponent,
  PopupSocialComponent,
  PopupChatComponent,
  ScrollToTopComponent,
  PaginationComponent,
  TableToolbarComponent,
  AlertErrorComponent,
  AlertSuccessComponent,
  PageBreadcrumbComponent,
  LineChartComponent,
  BarChartComponent
} from './components-common';

import {
  UsersComponent 
} from './components-admin/statistics';

import {
  MessageRightComponent,
  MessageLeftComponent
} from './components-common/message';
import { KendoModule } from './kendo-module';
import { TopCoursesComponent } from './components-client/children';
import { PrinterComponent } from './components-common/printer/printer.component';
import { StudentsLineChartComponent } from './components-admin/statistics';
import { StudentsRegisterCourseFormComponent } from './components-client/students-register-course/students-register-course-form/students-register-course-form.component';
import { MwlCalendarModule } from './mwl-calendar-module';
import { ContractClauseDetailComponent } from './components-admin/contract-clause/contract-clause-detail/contract-clause-detail.component';
import { ContractTypeDetailComponent } from './components-admin/contract-types/contract-type-detail/contract-type-detail.component';
import { EmployeeContractDetailComponent } from './components-admin/employee-contract/employee-contract-detail/employee-contract-detail.component';
import {OrderByPipe} from './_pipe/orderBy.pipe';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    HeaderComponent,
    MenuComponent,
    FooterComponent,
    LoginComponent,
    MatConfirmDialogComponent,
    HomeComponent,
    ResourceNotFoundComponent,
    AccessDeniedComponent,
    UsersListComponent,
    UserDetailComponent,
    AboutComponent,
    EducationProgramsComponent,
    CoursesListComponent,
    HeaderClientComponent,
    FooterClientComponent,
    RolesListComponent,
    RoleDetailComponent,
    CourseDetailComponent,
    EmployeeTypesComponent,
    EmployeeTypeDetailComponent,
    EmployeesListComponent,
    EmployeeDetailComponent,
    UsersTrashComponent,
    UserRightComponent,
    RoleRightComponent,
    InfrastructureComponent,
    PopupChatComponent,
    PopupSocialComponent,
    AchievementsAcquiredComponent,
    CourseCategoryDetailComponent,
    CourseCategoriesListComponent,
    LecturesTeamComponent,
    AdvisoryStaffComponent,
    ScrollToTopComponent,
    LecturesTeamDetailComponent,
    TemplateListComponent,
    TemplateDetailComponent,
    PaginationComponent,
    UsersComponent,
    TableToolbarComponent,
    ClassListComponent,
    ClassDetailComponent,
    AlertErrorComponent,
    AlertSuccessComponent,
    MessageRightComponent,
    MessageLeftComponent,
    PageBreadcrumbComponent,
    StudentsComponent,
    StudentDetailComponent,
    ClassStudentComponent,
    ClassStudentDetailComponent,
    RoleUserConfigComponent,
    TopCoursesComponent,
    CourseCategoryDetailClientComponent,
    CourseDetailClientComponent,
    ClassDetailClientComponent,
    MarkClassStudentComponent,
    StudentsListInClassComponent,
    CoursesOfStudentComponent,
    CoursePricesComponent,
    PaymentTuitionFeesComponent,
    ClassesOfStudentComponent,
    InvoicesStudentClassComponent,
    PrinterComponent,
    StudentsLineChartComponent,
    LineChartComponent,
    BarChartComponent,
    StudentsRegisterCourseComponent,
    StudentsRegisterCourseFormComponent,
    EmployeeContractComponent,
    ProfileComponent,
    ContractClauseComponent,
    ContractClauseDetailComponent,
    ContractTypesComponent,
    ContractTypeDetailComponent,
    EmployeeContractDetailComponent,
    OrderByPipe,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    angularJwt.JwtModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    CKEditorModule,
    NgbDropdownModule,
    ToastrModule.forRoot(),
    BlockUIModule.forRoot(),
    PrimeNGModule,
    KendoModule,
    CountUpModule,
    NgxChartsModule,
    MwlCalendarModule
  ],
  entryComponents: [
    MatConfirmDialogComponent,
    RoleDetailComponent,
    UserDetailComponent,
    CourseDetailComponent,
    CourseCategoryDetailComponent,
    EmployeeTypeDetailComponent,
    ClassDetailComponent,
    ClassStudentDetailComponent,
    RoleUserConfigComponent,
    CoursePricesComponent,
    PaymentTuitionFeesComponent,
    ContractClauseDetailComponent,
    ContractTypeDetailComponent
  ],
  providers: [AdminGuard, AuthService, MatDialogService,
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: HttpAuthInterceptor, multi: true },
    DialogService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
