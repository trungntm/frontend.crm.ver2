import { Component, OnInit } from '@angular/core'
import { User } from '../_entity/user'
import { AuthService } from '../_service/auth.service'
import { JwtHelperService } from '@auth0/angular-jwt'
import { first } from 'rxjs/operators'
import { Route, Router } from '@angular/router'
import { AuthenticationConstant, TokenConstant, TextConstant } from '../_constant'
import { BlockUI, NgBlockUI } from 'ng-block-ui';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css',
              '../../assets/custom/css/Login-Form-Dark.css',
            ]
})
export class LoginComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI

  user: User
  jwtHelper: JwtHelperService
  error: string
  isLoginSuccess: boolean
  constructor(private router: Router, private authSerice: AuthService) {
    this.user = new User
    this.jwtHelper = new JwtHelperService
   }

  ngOnInit() {
    this.onLogout()
  }

  onLogin() {
    this.blockUI.start(TextConstant.LOADING)
    this.authSerice.login(this.user.username, this.user.password)
      .pipe(first())
      .subscribe(res => {
        this.isLoginSuccess = true
        let author: String = res.headers.get(AuthenticationConstant.Authorization)
        const tokenIndex = author.lastIndexOf(' ') + 1
        let token = author.substr(tokenIndex)
        if (token) {
          localStorage.setItem(TokenConstant.X_ACCESS_TOKEN, token)
          // this.tokenHelper.SetToken(token)
          const decodedToken = this.jwtHelper.decodeToken(token)
          localStorage.setItem(TokenConstant.X_SUB_TOKEN, decodedToken.sub)
          for (let prop of decodedToken.authorities) {
            localStorage.setItem(prop.authority, prop.authority)
          }

          this.router.navigate(['/trang-chu'])
        }

        this.blockUI.stop()
      }, err => {
        console.log("login fail : " + err)
        this.error = err
        this.isLoginSuccess = false
        this.blockUI.stop()
      })
  }

  onLogout() {
    localStorage.clear()
  }
}
