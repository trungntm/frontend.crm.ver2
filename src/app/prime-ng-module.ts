import { NgModule } from "@angular/core";
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { CalendarModule } from 'primeng/calendar';
import { TabViewModule } from 'primeng/tabview';
import { PickListModule } from 'primeng/picklist';
import { VirtualScrollerModule } from 'primeng/virtualscroller';
import { ChartModule } from 'primeng/chart';
import {SlideMenuModule} from 'primeng/slidemenu';
import {DeferModule} from 'primeng/defer';
import {KeyFilterModule} from 'primeng/keyfilter';
import {TooltipModule} from 'primeng/tooltip';

@NgModule({
  exports: [
    DropdownModule,
    InputTextModule,
    DynamicDialogModule,
    InputTextareaModule,
    ScrollPanelModule,
    CalendarModule,
    TabViewModule,
    PickListModule,
    VirtualScrollerModule,
    ChartModule,
    SlideMenuModule,
    DeferModule,
    KeyFilterModule,
    TooltipModule
  ]
})

export class PrimeNGModule {}
