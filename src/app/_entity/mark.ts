import { BaseEntity } from './base-entity'
import { Class } from './class';
import { Student } from './student';

export class Mark extends BaseEntity {
  midMark: number
  finalMark: number
  total: number
  scoreWords: string
  content: string
  aclass: Class
  student: Student

  constructor() {
    super()
    this.aclass = new Class()
    this.student = new Student()
  }
}