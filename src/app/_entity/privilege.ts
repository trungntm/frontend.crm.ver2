import { BaseEntity } from "./base-entity";

export class Privilege extends BaseEntity {
  privilegeName: String
  description: String
  isOfRole: boolean = false
}
