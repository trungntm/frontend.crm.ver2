import { BaseEntity } from "./base-entity";

export class Template extends BaseEntity {
  name: string
  title: string
  description: string
  content: string
  type: number
}