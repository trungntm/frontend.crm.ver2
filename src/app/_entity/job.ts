import { BaseEntity } from './base-entity'

export class Job extends BaseEntity {
  name : string;
  description : string;
}