import {BaseEntity} from './base-entity';
import {ContractType} from './contract-type';
import {Employee} from './employee';
import {ContractEmployeeRule} from './contract-employee-rule';

export class ContractEmployee extends BaseEntity{
  contractCode: string;
  totalMonth: number;
  salary: number;
  addressWork: string;
  nameSideA: string;
  companySideA: string;
  positionSideA: string;
  phoneSideA: string;
  addressSideA: string;
  workDescription: string;
  startDate: Date = new Date();
  endDate: Date = new Date();
  contractType: ContractType;
  employee: Employee;
  contractEmployeeRules: ContractEmployeeRule[] = [];

  constructor() {
    super();
    this.contractType = new ContractType();
    this.employee = new Employee();
  }
}
