import {BaseEntity} from './base-entity';

export class TakeBreak extends BaseEntity {
  reason: string;
  fromTime: Date = new Date();
  toTime: Date = new Date();
}
