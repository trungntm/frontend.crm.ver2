import { BaseEntity } from './base-entity';
import { Class } from './class';
import { Contact } from './contact';
import { Job } from './job';

export class Student extends BaseEntity {
  name: string
  gender: boolean
  birthDay: Date
  potentialStudent: boolean
  contact: Contact
  job: Job
  
  constructor() {
    super()
    this.contact = new Contact()
    this.job = new Job()
  }
}