import { BaseEntity } from "./base-entity";
import { Employee } from "./employee";
import { Role } from "./role";

export class User extends BaseEntity {
	username: string;
	password: string;
	confirmPassword: string;
	lastLoginDate: Date = new Date();
	lastActivtyDate: Date= new Date();
	employee: Employee = new Employee();
	roles: Role[] = [];
}
