import { BaseEntity } from './base-entity'

export class Price extends BaseEntity {
  cost: number
  total: number
  expiryDate: Date
  content: string
}