import { BaseEntity } from './base-entity'
import { Student } from './student';
import { Class } from './class';
import { Employee } from './employee';

export class Invoice extends BaseEntity {
  code: string
  discountValue: number
  note: string
  content: string 
  paymentValue: number
  paymentDate: Date
  student: Student
  aclass: Class
  employee: Employee

  constructor() {
    super() 
    this.student = new Student()
    this.aclass = new Class()
    this.employee = new Employee()
  }
}