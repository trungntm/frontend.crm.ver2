import { BaseEntity } from "./base-entity";
import { Image } from './image'
import { EmployeeType } from "./employee-type";
import {TakeBreak} from './take-break';

export class Employee extends BaseEntity {
  code: string;
  name: string;
  passPort: string;
  birthDay: Date = new Date();
  indentify: string;
  phoneNumber: string;
  folk: string;
  addressHometown: string;
  addressNow: string;
  email: string;
  bankNo: string;
  diploma: string;
  major: string;
  isLecture: boolean;
  description: string;

  employeeType: EmployeeType = new EmployeeType();
  image: Image = new Image();
  takeBreaks: TakeBreak[] = [];
}
