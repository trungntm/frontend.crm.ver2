import { BaseEntity } from "./base-entity";
import { Privilege } from "./privilege";

export class Role extends BaseEntity {
  roleName: string
  description: string
  isOfUser: boolean

  privileges: Privilege[]
}
