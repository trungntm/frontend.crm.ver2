import {BaseEntity} from './base-entity';

export class ContractEmployeeRule extends BaseEntity {
  title: string;
  content: string;
  description: string;
}
