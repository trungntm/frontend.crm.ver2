import { BaseEntity } from "./base-entity";

export class EmployeeType extends BaseEntity {
  typeName: string
  description: string
}
