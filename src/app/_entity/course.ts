import { BaseEntity } from "./base-entity";
import { CourseCategory } from "./course-category";
import { Price } from './price';

export class Course extends BaseEntity {
  name: string
  description: string
  content: string
  longTermCourse: boolean
  imgPath: string
  costValue: number
  courseCategory: CourseCategory = new CourseCategory()
  prices: Price[]
}