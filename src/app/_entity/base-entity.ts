export class BaseEntity {
  id: number;
  dateCreated: Date;
  userCreated: string;
  dateUpdated:  Date;
  userUpdated: string;
  status: string;
  state: number;
}
