import { BaseEntity } from "./base-entity";
import { Course } from "./course";
import { Student } from './student';

export class Class extends BaseEntity {
  name: string
  noStudent: number
  currentNoStudent: number
  openDate: Date
  endDate: Date
  description: string
  room: string
  tuitionValue: number
  course: Course = new Course()
  students: Student[]
}
