import { BaseEntity } from "./base-entity";

export class Image extends BaseEntity {
  fileName: string
  imgPath: string
  filePath: string
}