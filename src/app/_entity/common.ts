export interface IFilterRequest {
  [key: string]: any
}

export interface IDataConfirmDelete {
  title: string;
  message: string;
}

export class BaseResponse<T> {
  status: number;
  errors: Error[] = [];
  result: T;
}

export class PageResponse<T> extends BaseResponse<T>{
  errors: Error[] = [];
  result: T;
  status: number;
  pageNumber: number;
  totalItems: number;
}

export class Error {
  status: number;
  message: string;
}

export interface IPDropdown {
  label: string;
  value: any;
}
