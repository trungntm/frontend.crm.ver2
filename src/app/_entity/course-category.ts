import { BaseEntity } from "./base-entity"
import { Course } from "./course";

export class CourseCategory extends BaseEntity {
  name: string
  description: string
  courses: Course[]
}