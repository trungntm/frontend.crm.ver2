import { BaseEntity } from './base-entity'

export class Contact extends BaseEntity {
  contactName: string
  mobilePhone: string
  workPhone: string
  email: string
  facebook: string
  fax: string
  website: string
  backAccount: string
  address1: string
  address2: string
  address3: string
}