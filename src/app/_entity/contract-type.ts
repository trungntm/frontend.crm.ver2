import {BaseEntity} from './base-entity';

export class ContractType extends BaseEntity{
  name: string;
  sign: string;
  title: string;

  constructor() {
    super();
  }
}
