import {BaseEntity} from './base-entity';
import {ContractType} from './contract-type';

export class ContractClause extends BaseEntity {
  title: string;
  content: string;
  description: string;
  contractType: ContractType;

  constructor() {
    super();
    this.contractType = new ContractType();
  }
}
