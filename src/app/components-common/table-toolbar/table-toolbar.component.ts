import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatDialogService } from '../../_service/common';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { TextConstant } from '../../_constant';
import { DialogService } from 'primeng/api';
import { Router } from '@angular/router';
import { RoleDetailComponent, UserDetailComponent, CourseDetailComponent, CourseCategoryDetailComponent, EmployeeTypeDetailComponent, ClassDetailComponent, ClassStudentDetailComponent } from '../../components-admin';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-table-toolbar',
  templateUrl: './table-toolbar.component.html',
  styleUrls: [
    './table-toolbar.component.css']
})

export class TableToolbarComponent implements OnInit {

  @BlockUI() blockUI: NgBlockUI
  selectedExcel
  @Input() locationCreatedPage
  @Input() addEditRole
  @Input() addEditUser
  @Input() addEditCourse
  @Input() addEditCourseCat
  @Input() addEditEmpType
  @Input() addEditClass
  @Input() classStudentInput
  @Output() emitAfterCreateRecord
  @Output() emitImportExcel
  // input for css
  @Input() buttonAdd = true
  @Input() buttonSave = true
  @Input() buttonUpload = true
  @Input() buttonDowload = true
  @Input() buttonCancel = true

  constructor(private dialogService: MatDialogService,  private router: Router, private primeDialogService: DialogService) {
    this.emitAfterCreateRecord = new EventEmitter<any>()
    this.emitImportExcel = new EventEmitter<any>()
   }

  ngOnInit() {
  }

  onShowCreatedRecord() {
    if (!this.locationCreatedPage) {
      if (this.addEditRole) {
        const ref = this.primeDialogService.open(RoleDetailComponent, {
          header: 'Thông tin quyền người dùng',
          width: '50%'
        })
        ref.onClose.subscribe(res => {
          this.emitAfterCreateRecord.emit(res)
        })

      } 
      if (this.addEditUser) {
        const ref = this.primeDialogService.open(UserDetailComponent, {
          header: 'Thông tin người dùng',
          width: '70%'
        })

        ref.onClose.subscribe(res => {
          this.emitAfterCreateRecord.emit(res)
        })
      }
      if (this.addEditCourse) {
        const ref = this.primeDialogService.open(CourseDetailComponent, {
          header: 'Thông tin khóa học',
          width: '70%',
        })

        ref.onClose.subscribe(res => {
          this.emitAfterCreateRecord.emit(res)
        })
      }
      if (this.addEditCourseCat) {
        const ref = this.primeDialogService.open(CourseCategoryDetailComponent, {
          header: 'Thông tin loại khóa học',
          width: '50%'
        })

        ref.onClose.subscribe(res => {
          this.emitAfterCreateRecord.emit(res)
        })
      }
      if (this.addEditEmpType) {
        const ref = this.primeDialogService.open(EmployeeTypeDetailComponent, {
          header: 'Thông tin loại nhân viên',
          width: '50%'
        })

        ref.onClose.subscribe(res => {
          this.emitAfterCreateRecord.emit(res)
        })
      }

      if (this.addEditClass) {
        const ref = this.primeDialogService.open(ClassDetailComponent, {
          header: 'Thông tin lớp học',
          width: '70%'
        })

        ref.onClose.subscribe(res => {
          this.emitAfterCreateRecord.emit(res)
        })
      }
      if (this.classStudentInput) {
        const ref = this.primeDialogService.open(ClassStudentDetailComponent, {
          data: {
            classId: this.classStudentInput
          },
          header: 'Thêm học viên vào lớp',
          width: '70%'
        })

        ref.onClose.subscribe(res => {
          isNullOrUndefined(res)? undefined: this.emitAfterCreateRecord.emit(res)
        })
      }
    } else {
      this.router.navigate([`/${this.locationCreatedPage}`])
      console.log(`redirect to ${this.locationCreatedPage}`)
    }
  }

  async onChangeExcel(e) {
    if (e.target.files && e.target.files[0]) {
      this.selectedExcel = e.target.files[0];
      await this.onImportExcel();
    }
  }

  async onImportExcel() {
    
    const formData = new FormData();
    formData.append('excel', this.selectedExcel, this.selectedExcel.name);
    this.emitImportExcel.emit(formData)
    // this.userService.importExcel(formData)
    //   .subscribe(res => {
    //     console.log(res);
    //   }, err => {
    //     console.log(err);
    //   });
  }

}
