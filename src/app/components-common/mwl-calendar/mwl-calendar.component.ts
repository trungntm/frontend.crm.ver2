import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { CalendarEventTimesChangedEvent, CalendarView, CalendarEvent } from 'angular-calendar';
import { Subject } from 'rxjs';
import { isSameDay, isSameMonth } from 'date-fns';


@Component({
  selector: 'app-mwl-calendar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './mwl-calendar.component.html',
  styleUrls: ['./mwl-calendar.component.css']
})
export class MwlCalendarComponent implements OnInit {

  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;

  viewDate: Date = new Date();

  refresh: Subject<any> = new Subject();

  @Input() events: CalendarEvent[] = [];

  @Output() emitClickEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() emitClickNextPrevious: EventEmitter<any> = new EventEmitter<any>();

  activeDayIsOpen: boolean = true;
  @Input() showExplain = true;
  constructor() { }

  ngOnInit() {
    if (this.events.length === 0) {
      this.activeDayIsOpen = false;
    }
  }

  clickNextPrevious(viewDate, value, viewType) {
    this.emitClickNextPrevious.emit({viewDate, value, viewType});
  }


  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      this.viewDate = date;
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
    }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map(iEvent => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd
        };
      }
      return iEvent;
    });
    this.handleEvent('Dropped or resized', event);
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.emitClickEvent.emit(event);
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

}
