import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-page-breadcrumb',
  templateUrl: './page-breadcrumb.component.html',
  styleUrls: ['./page-breadcrumb.component.css']
})
export class PageBreadcrumbComponent implements OnInit {

  @Input() activePage
  @Input() subLinkTitle
  @Input() subLinkHref

  constructor() {
    
   }

  ngOnInit() {
    this.convertSubLink()
  }

  convertSubLink() {
    if (this.subLinkTitle ) {
      this.subLinkTitle = this.subLinkTitle.split(',')
    }

    if (this.subLinkHref) {
      this.subLinkHref = this.subLinkHref.split(',')
    }
  }
}
