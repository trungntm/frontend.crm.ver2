import { Component, OnInit, Input } from '@angular/core';
import { IChartData } from './chart-data';
import * as shape from 'd3-shape';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css']
})
export class LineChartComponent implements OnInit {

  @Input() protected readonly number : number = 1;
  @Input() protected readonly xAxisLabel: string = 'X Axis';
  @Input() protected readonly yAxisLabel: string = 'Y Axis';
  @Input() protected readonly legendTitle: string = 'Ghi chú';
  @Input() protected readonly colorCodes: string[] = ['#5AA454', '#A10A28', '#C7B42C', '#000055', '#009966', '#333300'];
  @Input() protected readonly legendPosition: 'bottom' | 'top' | 'left' | 'right' | 'below' | 'above' = 'below';
  @Input('data') set data(val: IChartData[]) {
    if (val && val.length) {
      this.chartData = [...val];
    }
  }
  protected lineCurve: any = shape.curveCatmullRom;
  protected colorSchema: any = {domain: []};
  protected chartData: IChartData[] = [];
  protected chartWidth: number;

  constructor() {
  }

  ngOnInit() {
    this.chartWidth = (window.innerWidth/this.number) - 330;
    this.colorSchema = {
      domain: [...this.colorCodes],
    };
  }
}
