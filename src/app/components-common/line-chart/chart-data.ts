export interface IChartData {
  name: string;
  series: {
    name: string;
    value: any;
  }[];
}