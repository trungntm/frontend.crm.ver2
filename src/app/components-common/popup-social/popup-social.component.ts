import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-popup-social',
  templateUrl: './popup-social.component.html',
  styleUrls: [
    '../../../assets/global/plugins/font-awesome/css/font-awesome.min.css',
    '../../../assets/global/plugins/bootstrap/css/bootstrap.min.css',
    './popup-social.component.css']
})
export class PopupSocialComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
