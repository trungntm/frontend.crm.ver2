import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-alert-error',
  templateUrl: './alert-error.component.html',
  styleUrls: [
    '../../../assets/global/plugins/font-awesome/css/font-awesome.min.css',
    '../../../assets/global/plugins/bootstrap/css/bootstrap.min.css',
    './alert-error.component.css']
})
export class AlertErrorComponent implements OnInit {

  @Input() statusCode
  @Input() strongMsg
  @Input() normalMsg
  constructor() { }

  ngOnInit() {
  }

}
