import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import * as Stomp from 'stompjs';
import * as toastr from '../../../assets/global/plugins/bootstrap-toastr/toastr.min.js'
import { environment } from 'src/environments/environment.js';
import { Sender } from './sender.js';

@Component({
  selector: 'app-popup-chat',
  templateUrl: './popup-chat.component.html',
  styleUrls: [
    './popup-chat.component.css']
})
export class PopupChatComponent implements OnInit {

  private serverUrl = 'ws://localhost:8080/gretting'
  private title = 'WebSockets chat';
  private stompClient;

  greetings: string[] = [];
  showConversation: boolean = false;
  ws: any;
  name: string;
  disabled: boolean;
  isSend: boolean 
  sender: Sender;

  constructor() {
    this.isSend = false
    this.sender = new Sender();
   }

  async ngOnInit() {
    await this.connect()
  }

  async connect() {
    //connect to stomp where stomp endpoint is exposed
    //let ws = new SockJS(http://localhost:8080/greeting);
    let socket = new WebSocket("ws://localhost:8080/messaging");
    
    this.ws = Stomp.over(socket);
    let that = this;
    await this.ws.connect({}, async function(frame) {
      that.ws.subscribe("/errors", function(message) {
        toastr.error(message.body)
      });
      await that.ws.subscribe("/topic/reply", async function(message) {
        console.log("this", this)
        console.log("that", that)
        if (that.isSend !== true) {
          that.isSend = false
        }
        console.log(that.isSend )
        await that.showGreeting(message.body);
      });
      that.disabled = true;
    }, function(error) {
      toastr.error(error)
    });
  }

  disconnect() {
    if (this.ws != null) {
      this.ws.ws.close();
    }
    this.setConnected(false);
    console.log("Disconnected");
  }

  async onSendMessage() {
    let data = JSON.stringify({
      'message' : this.sender.phone
    })
    this.isSend = true
    console.log(this.isSend)
    await this.ws.send("/app/message", {}, data);
  }

  async showGreeting(message) {
    this.showConversation = true;
    await this.greetings.push(message)
  }

  setConnected(connected) {
    this.disabled = connected;
    this.showConversation = connected;
    this.greetings = [];
  }

  openChatBox() {
    $('#myForm').css('display','block')
  }

  closeChatBox() {
    $('#myForm').css('display','none')
  }
  
}
