export class Sender {
  name: string;
  email: string;
  phone: string;
}