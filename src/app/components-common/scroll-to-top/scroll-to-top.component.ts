import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';


@Component({
  selector: 'app-scroll-to-top',
  templateUrl: './scroll-to-top.component.html',
  styleUrls: [
    './scroll-to-top.component.css']
})
export class ScrollToTopComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $(window).scroll(() => {
      this.scrollFunction()
    })
  }

  scrollFunction() {
    if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
      document.getElementById("btn-scroll-top").style.display = "block";
    } else {
      document.getElementById("btn-scroll-top").style.display = "none";
    }
  }
  
  // When the user clicks on the button, scroll to the top of the document
  backToTop() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    
  }

}
