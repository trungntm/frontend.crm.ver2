import {IPDropdown} from '../_entity';
import {STATE, STATE_EMPLOYEE} from '../_common/common';

export function getStates(): IPDropdown[] {
  let states: IPDropdown[] = [];
  states.push({
    label: 'Tất cả',
    value: STATE.ALL
  });
  states.push({
    label: 'Mới tạo',
    value: STATE.NEW
  });
  states.push({
    label: 'Đang diễn ra',
    value: STATE.IN_PROGRESS
  });
  states.push({
    label: 'Tạm dừng',
    value: STATE.PENDING
  });
  states.push({
    label: 'Nghỉ tết',
    value: STATE.TET_HOLIDAY
  });
  states.push({
    label: 'Kết thúc',
    value: STATE.FINISH
  });

  return states;
}

export function getStatus() {
  let status: IPDropdown[] = [];
  status.push({
    label: 'Hoạt động',
    value: true
  });
  status.push({
    label: 'Ngưng hoạt động',
    value: false
  });

  return status;
}

export function getStateEmployee() {
  let states: IPDropdown[] = [];

  states.push({
    label: 'Hoạt động',
    value: STATE_EMPLOYEE.ACTIVATE
  });
  states.push({
    label: 'Kết thúc',
    value: STATE_EMPLOYEE.DEACTIVATE
  });
  states.push({
    label: 'Nghỉ ốm',
    value: STATE_EMPLOYEE.SICK_LEAVE
  });
  states.push({
    label: 'Nghỉ thai sản',
    value: STATE_EMPLOYEE.MATERNITY_LEAVE
  });
  states.push({
    label: 'Nghỉ phép',
    value: STATE_EMPLOYEE.PERMISSION_LEAVE
  });

  return states;
}
