import { STATUS } from '../_constant/color-constant';

export function getColorByState(state: number) {
  switch(state) {
    case 0:
      return STATUS.COLORS.green;
    case 1:
      return STATUS.COLORS.orange;
    case 2:
      return STATUS.COLORS.black;
  }
}